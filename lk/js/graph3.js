<link rel="stylesheet" href="/js/amcharts/style.css" type="text/css">
<script src="/js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/js/amcharts/serial.js" type="text/javascript"></script>
<script src="/js/amcharts/light.js" type="text/javascript"></script>
<script src="/js/amcharts/amstock.js" type="text/javascript"></script>
<script>
var type_bonds={if $GET.p=='stock' &&$GET.id>0}{$GET.id}{else}6{/if};
var day = 0;
var date_create=0;
$(function(){



var chartData = generateChartData();
var chart = AmCharts.makeChart("ecommerce_chart1", {
    "type": "serial",
    "theme": "dark",
    "marginRight": 80,
    "dataProvider": chartData,
    "valueAxes": [{
        "position": "left",
        "title": "Цена акции"
    }],
        "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "fillAlphas": 0.4,
        "valueField": "visits",
      // "connect": false,
         "balloonText": "<div style='margin:5px; font-size:19px;'>Цена:<b>[[value]]</b></div>"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "scrollbarHeight": 80,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount": true,
        "color": "#AAAAAA"
    },
    "chartCursor": {
        "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
        "cursorPosition": "mouse"
    },
    "categoryField": "date",
    "categoryAxis": {
        "minPeriod": "mm",
        "parseDates": true
    },
      "periodSelector": {
      
    "periods": [ {
      "period": "mm",
      "count": 5,
      "label": "5 мин"
    }, {
      "period": "DD",
      "count": 1,
      "label": "1 день"},{
      "period": "DD",
      "count": 10,
      "label": "10 days"
    }, {
      "period": "MM",
      "count": 1,
      "label": "1 month"
    }, {
      "period": "YYYY",
      "count": 1,
      "label": "1 year"
    }, {
      "period": "YTD",
      "label": "YTD"
    }, {
      "period": "MAX",
      "label": "MAX"
    } ]
  },
    "export": {
        "enabled": true,
         "dateFormat": "YYYY-MM-DD HH:NN:SS"
    }
});

var params= new Array({  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   });
setInterval(function () {
    $.ajax({
        url: '/ajax.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        data: { object: 'Bond', action: 'Price', param: params},
        
        success: function(data)
        {  
          //  chart.dataProvider.push({  date: data[],visits: });
            //data.Reverse();
           //console.log(newDate);
              if (date_create!=data['date_create']) {
                    chart.dataProvider.push({
                    date:  data['DateTimeNow'],
                    visits: data['price']
                    });                   
                   date_create=data['date_create'];        
                   chart.validateData();
                   chart.addListener("dataUpdated", zoomChart);
                   // таблица открытых
                    $(".table_real_price_buy").each(function(){
                        var count=$(this).data('count'), 
                        price=$(this).data('price'), 
                        new_price=data['price'],
                        total=$(this).data('total'), 
                        arrow='';
                        var difference=new_price-price;
                        var differencePercent=((difference*100)/new_price).toFixed(2);
                        
                       // $(this).data('price');
                        if (total<new_price*count) { arrow='fa fa-arrow-up text-success'; } else { arrow='fa fa-arrow-down text-danger';}
                        
                        $(this).html(new_price+" <i class='"+arrow+"'></i>").next().html(differencePercent+"% <i class='"+arrow+"'></i>").next().html(((new_price*count)-total).toFixed(2)+" <i class='"+arrow+"'></i>");
                       });
               }

            

        },
    });   
}, 30000);



chart.addListener("dataUpdated", zoomChart);
zoomChart();
function zoomChart() {
    chart.zoomToIndexes(chartData.length-10, chartData.length );
}

function generateChartData() {

var chartData = [];
//var type_bonds={if $GET.p=='stock' &&$GET.id>0}{$GET.id}{else}1{/if};
var params= new Array({  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   });
        
        $.ajax({
        url: '/ajax.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: 'Bond', action: 'PriceAll', param: params},
        
        success: function(data)
        {  
            
            //data.Reverse();
            data['row'].forEach(function(row2, i, arr) {        //console.log(newDate);  
                chartData.unshift({
                    date:  row2['date_create'],
                    visits: row2['price']
                    });                   
           });     
        },
    });         
         
   return chartData; 
}
});
	</script>