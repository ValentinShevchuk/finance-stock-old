$(function(){

function getUrlVar(){
    var urlVar = window.location.search; // получаем параметры из урла
    var arrayVar = []; // массив для хранения переменных
    var valueAndKey = []; // массив для временного хранения значения и имени переменной
    var resultArray = []; // массив для хранения переменных
    arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
    if(arrayVar[0]=="") return false; // если нет переменных в урле
    for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
        valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
        resultArray[valueAndKey[0]] = valueAndKey[1]==undefined?true:valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
    }
    return resultArray; // возвращаем результат
}
var GET = getUrlVar();

    $("[name=to_market_in_percent]").on('keyup',function(){
        // калькулятор для поля процентов, высчитывает сколько в штуках акций будет введеный процент
        var percent=parseFloat($(this).val()); // получаем введный процент
        var bond=parseFloat($("[name=capital_in_stock]").val()); // получаем сколько всего акций есть
        if (percent>100) { 
            alert_custom({ "text":"Нельзя продать больше 100 процентов своих акций"}); 
            $("[data-command='create']").hide();
        }
        else{         
            $("[data-command='create']").show();
        }
        $("[name=to_market_in_amount]").val(Math.round((percent*bond)/100));
    });

    $("[name=to_market_in_amount]").on('keyup',function(){
        // функция аналогичная выше.
        var bond=parseFloat($(this).val()); 
        var all=parseFloat($("[name=capital_in_stock]").val());
        
        //console.log(bond+' '+$("[name=capital_in_stock]").val());
        if (bond>all) { 
            alert_custom({ "text":"Нельзя продать больше своих акций"}); 
            $("[data-command='create']").hide();
        }
        else{
            $("[data-command='create']").show();
        }
        $("[name=to_market_in_percent]").val(((bond/all)*100).toFixed(2));
           
            
    
    });    
{if !isset($GET.beneficiary)}
    $(".dividend").toggle();
    $("[name=is_dividend]").change(function(){
         $(".dividend").toggle();
    });
{/if}

//var is_registration={if isset($GET.new)}1{/if};

if (GET.new){
    $("[name=id_project]").change(function(){
        // при выборе проекта подгружается бенефициары и акции которые зарегины под проектом
       // alert('Надо довить тут проверку для клиента что он не являцется бенефициаром, и если является то в список который скрыл, нужно поменять на поле и вставлять туда ID');
        var id_project= $("option:selected",this).attr('value');
        if (id_project>0){
            
            var BondNameList=_AJAX('Bond','GetBondNameList',{ 'json':'1',  'id_project': id_project,  'vis':1});
            
            if (BondNameList['error']==0){ 
                // очищаем список
                $("[name=id_bond_name]").find('option').remove();
                 // выставляем значение чтобы менеджер выбрал пункт
                $("[name=id_bond_name]").append('<option value="0">Выберите акции</option>');
                BondNameList['row'].forEach(function(row, i, arr) {                   
                        $("[name=id_bond_name]").append('<option value="'+row['id']+'">'+row['title']+'</option>')
                    });
                
            } else  alert_custom({ 'type':'danger','text':BondNameList['error_msg']});      
                  
            var BeneficiaryId= _AJAX('Project', 'GetProjectBeneficiaryId',{  'json':'1',  'id_project': id_project,  'vis':1});
            /* console.log(BeneficiaryList[0]['name']);*/
            if (BeneficiaryId['error']==0){
                // очищаем список
  
                // выставляем значение чтобы менеджер выбрал пункт
                $("[name=id_beneficiary]").val(BeneficiaryId['id_beneficiary']);$(".order_click").show();
            }
            else  { alert_custom({ 'type':'danger','text':BeneficiaryId['error_msg']}); $(".order_click").hide();}

                
       } else  alert_custom({ 'type':'warning','text':'Выберите проект'});
       
    }); 
    $("[name=id_bond_name]").change(function(){
            // при выборе бенефициара подгружается инфа по акциях, которая вводится при регистрации бенека
            var id_bond_name= $("[name=id_bond_name] option:selected").attr('value');
            var id_client= "{$smarty.session.site_user}";//$("[name=id_beneficiary] option:selected").attr('value');
            if (id_bond_name>0){ 

            if (GET.registration){
                
                var BondInfo=_AJAX('Bond', 'GetBondInfo',{  json:'1', id_bond_name: id_bond_name , 'vis':1});
                if (BondInfo.error==0){
                    $("[name=capital_in_percent]").val(BondInfo['row'][0]['capital_in_percent']);
                    $("[name=capital_in_stock]").val(BondInfo['row'][0]['count_in_market']);
                    $("[name=value_of_stock]").val(BondInfo['row'][0]['par_now_sell']);
                    $("[name=to_market_in_percent]").val('');
                    $("[name=to_market_in_amount]").val('');
                }else alert_custom({ 'type':'danger','text':BondInfo['error_msg']});

//------- {* оформление акций в собственность *}

                var OpenList=_AJAX('Bond','GetOpenPositionList',{ 'json':'1', 'is_demo':0, 'id_bond_name': id_bond_name, 'vis':1});
                if (OpenList.error==0){ 
                    // очищаем список
                    $("[name=id_open_position]").find('option').remove();
                    // выставляем значение чтобы менеджер выбрал пункт
                    if (OpenList.count){
                        $("[name=id_open_position]").append('<option value="0">Выберите открытые позиции</option>');
                        OpenList['row'].forEach(function(row, i, arr) {
                            $("[name=id_open_position]").append('<option value="'+row.id+'" data-value='+row.price+' data-count='+row.bonds_count+'>№'+row.id+' Кол-во: '+row.bonds_count+'</option>')
                        });
                    } else $("[name=id_open_position]").append('<option value="0">Нет открытых позиций</option>');
                } else alert_custom({ 'type':'danger','text':OpenList['error_msg']});
//-------
            }
            else{
                var BeneficiaryInfo=_AJAX('Project', 'GetBeneficiaryInfo',{  json:'1',  id_client:  id_client, id_bond_name: id_bond_name , 'vis':1});
                if (BeneficiaryInfo['error']==0){
                    $("[name=capital_in_percent]").val(BeneficiaryInfo['row']['percent']);
                    $("[name=capital_in_stock]").val(BeneficiaryInfo['row']['capital_in_stock']);
                    $("[name=value_of_stock]").val(BeneficiaryInfo['row']['value_of_stock']);
                    $("[name=to_market_in_percent]").val('');
                    $("[name=to_market_in_amount]").val('');
                }else alert_custom({ 'type':'danger','text':BeneficiaryInfo['error_msg']});
            }

            }
            else { 
                alert_custom({ 'type':'danger', 'text':'Акция и бенефициар должны быть выбраны'}); 
                $("[name=id_beneficiary]").val(0);}
    });  
    $("[name=id_open_position]").change(function(){
        $("[name=capital_in_stock]").val($("[name=id_open_position] option:selected").attr('data-count'));
        $("[name=value_of_stock]").val($("[name=id_open_position] option:selected").attr('data-value'));
    });
}  
});