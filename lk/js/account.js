$(function(){
    $(".click-get-account-list").change(function(){

        var id_user= $("option:selected", this).attr('value');

        if (id_user>0){ 

            var result=_AJAX('Account', 'GetAccountList',{  'json':'1',  'id_user':  id_user,  'vis':1});
            if (result['error']==0){
                
                $("[name='id_account']").html('');
                $("[name='id_account']").append('<option value=0>Выберите счет</option>');    
                
                result['row'].forEach(function(row){
                    $("[name='id_account']").append('<option value='+row['id']+' >'+row['full_account']+'</option>');                
                });                                                
            }else alert_custom({ 'type':'danger','text':result['error_msg']});
        }
        else { alert_custom({ 'type':'danger', 'text':'Выберите клиента'}); }         
    });
    {if isset($GET.edit)}
    $("[name='type_transfer']").val({$order_info.type_transfer});
    $("[name='method_transfer']").val({$order_info.method_transfer});
    $("[name='purpose']").val({$order_info.purpose});
    {/if}    
    
});