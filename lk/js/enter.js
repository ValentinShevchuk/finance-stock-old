$(function(){
    /*
    $("[name='go'], [name='repeat']").on('click',function(){

        var 
                login=$("[name='login']").val(), 
                password=$("[name='password']").val(),
                remember=$("[name='remember']").val(),
                sms=$("[name='sms']").val();
                
        var params= new Array({  'json':'1', 'login':login, 'password':password, 'remember':remember, 'sms':sms });     
        var result=_AJAX('Enter', 'enter',params);  
        if (result['error']=='1'){ 
            new PNotify({
                title: 'Ошибка',
                text: result['error_msg'],
                type: 'danger'// all contextuals available(info,system,warning,etc)
            });
      
            }
        if (result['step']=='1'){ 
        $("[name='go']").hide();    
        $(".panelSMS0").addClass('hidden');
        $(".panelSMS1").removeClass('hidden');
        } 
        if (result['step']=='2'){ window.location.href='/'; }
    });
    $("[name='repeat']").on('click',function(){
        
        var params= new Array({  'json':'1' });     
        var result=_AJAX('Enter', 'orderGenerateSMSCode',params);  
        if (result['error']=='1'){ 
            new PNotify({
                title: 'Ошибка',
                text: result['error_msg'],
                type: 'danger'// all contextuals available(info,system,warning,etc)
            });
      
            }        
    });
    $("[name='sms']").on('keypress', function(){
        var code=$(this).val(); if (code.length>=3) { $("[name='go']").show();}
        
    });
*/   

    {if $smarty.session.enter_step=='1'} 
        $(".step1").addClass('hidden');
        $(".step2").removeClass('hidden');
   
    {/if}
    
$(".click-step1").on('click',function(){
      $(this).html("...");   
    var 
        login=$("[name='login']").val(), 
        password=$("[name='password']").val(),
        remember=$("[name='remember']").val(),
        params= new Array({  'json':'1', 'login':login, 'password':password, 'remember':remember, 'step':1 });     
        
    $('.loading').removeClass("hidden");    
    var result=_AJAX('Enter', 'enter_new', params);  
    if (result['error']=='1'){ 
        new PNotify({
            title: 'Ошибка',
            text: result['error_msg'],
            type: 'danger'// all contextuals available(info,system,warning,etc)
        });  
        $(this).html("<span class='fa fa-sign-in'></span> Войти");   
    } 
    else {
        $('.loading').addClass("hidden");    
        $(".step1").addClass('hidden');
        $(".step2").removeClass('hidden');
        var sec120=120;

        var intervalID=setInterval(function () {
            sec120--;
            $(".click-repeat-sms").html(sec120);
            if (sec120==0)  { clearInterval(intervalID);$(".click-repeat-sms").html("<span class='fa fa-mobile'></span> Запросить SMS-код"); }
            }, 1000);
        if (result['error_sms']=='1'){ 
            new PNotify({
                title: 'Ошибка',
                text: result['error_sms_msg'],
                type: 'danger'// all contextuals available(info,system,warning,etc)
            });  
        }
    }


});

$(".click-step2").on('click',function(){

    var 
        sms=$("[name='sms']").val();
        params= new Array({  'json':'1', 'sms':sms, 'step':2 });     
        
    $(this).html("<span class='fa fa-spinner fa-spin'></span>");    
    var result=_AJAX('Enter', 'enter_new', params);  
    if (result['error']>0){ 
        new PNotify({
            title: 'Ошибка',
            text: result['error_msg'],
            type: 'danger'// all contextuals available(info,system,warning,etc)
        });  
        $(this).html("Продолжить <span class='fa fa-arrow-right'></span>");   
    } 
    else {
        window.location.href='/';
    }


});

$(".click-repeat-sms").on('click',function(){

    var result=_AJAX('Enter', 'generateSmsCode',{ 'json':1});  
        if (result['error']=='1'){ 
            new PNotify({
                title: 'Ошибка',
                text: result['error_msg'],
                type: 'danger'// all contextuals available(info,system,warning,etc)
            });
      
    }
        else{
            new PNotify({
                title: 'Информация',
                text: 'SMS-код успешно отправлен',
                type: 'info'// all contextuals available(info,system,warning,etc)
            });
            var sec120=120;    
            var intervalID=setInterval(function () {
                sec120--;
                $(".click-repeat-sms").html(sec120);
                if (sec120==0)  { clearInterval(intervalID);$(".click-repeat-sms").html("<span class='fa fa-mobile'></span> Запросить SMS-код"); }
            }, 1000);
    }        

});
});