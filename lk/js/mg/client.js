$(function(){
	$(".button_is_legal").click(function(){
		var dataValue=$(this).attr('data-value');
		if (dataValue==0){
			$(".is_legal").hide();
			$(".is_no_legal").show();	
			$('.button_is_legal:eq(1)').removeClass('dark');
			$('.button_is_legal:eq(1)').addClass('light');

			$('.button_is_legal:eq(0)').removeClass('light');
			$('.button_is_legal:eq(0)').addClass('dark');
			$("[name=is_legal]").val(0);
		}else{	
			$(".is_legal").show();
			$(".is_no_legal").hide();	

			$('.button_is_legal:eq(1)').removeClass('light');
			$('.button_is_legal:eq(1)').addClass('dark');

			$('.button_is_legal:eq(0)').removeClass('dark');
			$('.button_is_legal:eq(0)').addClass('light');
			$("[name=is_legal]").val(1);
		}
		$("[name=is_legal]").attr('data-edit',1);
	});
$(".is_legal").hide();
{if isset($order_info.is_legal)}
		$(".is_no_legal").hide();
	if ({$order_info.is_legal}==0){
		$(".is_no_legal").show();
	} else {
		$(".is_legal").show();
	}
{/if}
});