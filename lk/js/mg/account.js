$(function(){
    $(".click-get-account-list").change(function(){

        var id_user= $("option:selected", this).attr('value');
        var tag=$(this).attr('name');
        //console.log(tag);
        var prefix='';
        if (tag=='id_user_to') prefix='_to';

        if (id_user>-1){ 

            var result=_AJAX('Account', 'GetAccountList',{  'json':'1',  'id_user':  id_user,  'vis':1});
            if (result['error']==0){
                
                $("[name='id_account"+prefix+"']").html('');
                $("[name='id_account"+prefix+"']").append('<option value=0>Выберите счет</option>');    
                
                result['row'].forEach(function(row){
                    $("[name='id_account"+prefix+"']").append('<option value='+row['id']+' >****'+row['full_account'].substring(row['full_account'].length-6)+' '+' Баланс:'+row['value']+'р. '+row['title']+'</option>');                
                });                                                
            }else alert_custom({ 'type':'danger','text':result['error_msg']});
        }
        else { alert_custom({ 'type':'danger', 'text':'Выберите клиента'}); }         
    });
    {if isset($GET.edit)}
 //   $("[name='type_transfer']").val({$order_info.type_transfer});
    $("[name='method_transfer']").val({$order_info.method_transfer});
    $("[name='purpose']").val({$order_info.purpose});
    {/if}    
   {if $order_info.type_transfer!=3} $(".between").toggle(); {/if}   
   $("[name='type_transfer']").change(function(){
        var type_transfer= $("option:selected", this).attr('value');
        if (type_transfer==3)  $(".between").toggle();  
   });
    
});