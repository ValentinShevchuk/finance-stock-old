<script>
var type_bonds={if $GET.p=='stock' &&$GET.id>0}{$GET.id}{else}6{/if};
var day = 0;
var date_create=0;
var chartData = [];
generateChartData();


function generateChartData() {

//var type_bonds={if $GET.p=='stock' &&$GET.id>0}{$GET.id}{else}1{/if};
var params= new Array({  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   });
        
        $.ajax({
        url: '/ajax.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
                    data: { object: 'Bond', action: 'PriceAll', param: params},
        
        success: function(data)
        {  
            
            //data.Reverse();
            data['row'].forEach(function(row2, i, arr) {        //console.log(newDate);  
                chartData.unshift({
                    date:  row2['date_create'],
                    value: row2['price'] ,
                    volume:    row2['price']                });                   
           });     
        },
    });         
         
//   return chartData; 
}
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "stock",
  "theme": "light",
    "categoryAxesSettings": {
    "minPeriod": "mm"
  },
  
  "dataSets": [ {
    "color": "#b0de09",
    "fieldMappings": [ {
      "fromField": "value",
      "toField": "value"
    }, {
      "fromField": "volume",
      "toField": "volume"
    } ],
    "dataProvider": chartData,
    "categoryField": "date"
    // EVENTS

  } ],


  "panels": [ {
    "title": "Value",
    "stockGraphs": [ {
      "id": "g1",
      "valueField": "value"
    } ],
    "stockLegend": {
      "valueTextRegular": " ",
      "markerType": "none"
    }
  } ],

 

  "chartCursorSettings": {
    "valueBalloonsEnabled": true,
    "graphBulletSize": 1,
    "valueLineBalloonEnabled": true,
    "valueLineEnabled": true,
    "valueLineAlpha": 0.5
  },

  "periodSelector": {
      
    "periods": [ {
      "period": "mm",
      "count": 5,
      "label": "5 мин"
    }, {
      "period": "DD",
      "count": 1,
      "label": "1 день"},{
      "period": "DD",
      "count": 10,
      "label": "10 days"
    }, {
      "period": "MM",
      "count": 1,
      "label": "1 month"
    }, {
      "period": "YYYY",
      "count": 1,
      "label": "1 year"
    }, {
      "period": "YTD",
      "label": "YTD"
    }, {
      "period": "MAX",
      "label": "MAX"
    } ]
  },

  "export": {
    "enabled": true
  }
});
</script>