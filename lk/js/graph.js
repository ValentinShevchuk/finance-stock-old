<!--script src="https://code.highcharts.com/stock/highstock.js"></script-->
<script src="js/highstock.js"></script>

<script>
var type_bonds={if $GET.p=='stock' && $GET.id>0}{$GET.id}{else if $GET.bond>0}{$GET.bond}{else}1{/if};
var day = 0;
var date_create=0;
 'use strict';
/* global document */
// Load the fonts
/*
Highcharts.createElement('link', {
   href: 'https://fonts.googleapis.com/css?family=Unica+One',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);
*/
Highcharts.theme = {
   colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]
      },
      style: {
         fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
   	
$(function () {
function generateChartData() {

var chartData = [];
//var type_bonds={if $GET.p=='stock' &&$GET.id>0}{$GET.id}{else}1{/if};
//var params= new Array({  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   });
        
        $.ajax({
        url: '/ajax.php',
        async:false,
        type: 'POST',
        dataType: 'jsonp',
        jsonp: 'callback',
        jsonpCallback: "jsonpCallback",
        //data: dataSend,
        data: { object: 'Bond', action: 'PriceAll', param: {  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   }},       
        success: function(data)
        {  
       
            //data.Reverse();
            data['row'].forEach(function(row2, i, arr) {        //console.log(newDate);  + (3*3600000)
                
           var   time= new Date(row2['date_create_new_format']).getTime()+ (3*3600000);
//console.log(time+' '+parseFloat(row2['price']));
                chartData.unshift([ time, parseFloat(row2['price'])]);                   
           });     
        },
    });         
         
   return chartData; 
}
function newPriceChartData(series){
   $.ajax({
                        url: '/ajax.php',
                        async:false,
                        type: 'POST',
                        dataType: 'jsonp',
                        jsonp: 'callback',
                        jsonpCallback: "jsonpCallback",
                        data: { object: 'Bond', action: 'Price', param: {  'type_bonds':type_bonds,   'is_demo':0 , 'date_create':date_create,   'json':'1'   }},
                        
                        success: function(data)
                        {  
                            //  chart.dataProvider.push({  date: data[],visits: });
                            //data.Reverse();
                            //console.log(newDate);
                            if (date_create!=data['date_create_new_format']) {                             
                                var   time = new Date(data['date_create_new_format']).getTime()+ (3*3600000);
                                series.addPoint([time, parseFloat(data['price'])], true, true);                                                           
                                date_create=data['date_create_new_format'];        
                               // таблица открытых позициий
                                $(".table_real_price_buy").each(function(){
                                    var count=$(this).data('count'), 
                                    price=$(this).data('price'), 
                                    new_price=data['price'],
                                    total=$(this).data('total'), 
                                    arrow='';
                                    var difference=new_price-price;
                                    var differencePercent=((difference*100)/new_price).toFixed(2);
                                    
                                   // $(this).data('price');
                                    if (total<new_price*count) { arrow='fa fa-arrow-up text-success'; } else { arrow='fa fa-arrow-down text-danger';}
                                    
                                    $(this).html(new_price+" <i class='"+arrow+"'></i>").next().html(differencePercent+"% <i class='"+arrow+"'></i>").next().html(((new_price*count)-total).toFixed(2)+" <i class='"+arrow+"'></i>");
                                   });
                                  //-- выгрузка игфы 
                                
                               
                                $("#Different").removeClass('text-success').removeClass('text-danger');
                                $("#Different").html(data['difference']+" ("+data['differencePercent'].toFixed(2)+"%)");
                                if (data['difference']>=0)  { $("#Different").addClass('text-success'); var arrow="<span class='fa fa-arrow-up text-success'></span>";} else { $("#Different").addClass('text-danger'); var arrow="<span class='fa fa-arrow-down text-danger'></span>";}
                                $("#PriceLast").html(arrow+''+data['PriceLast']);
                                $(".order-sell-open").html('Купить<br>'+data['price_sell']+' <span class="fa fa-rub">');
                                $(".order-buy-open").html('Продать<br>'+data['price_buy']+' <span class="fa fa-rub">');
                                $("#price_max").html(data['price_max']);
                                $("#price_min").html(data['price_min']);                                
                                $("#tourover_all_deal").html(data['tourover_all_deal']);                                
                           }                                           
                        },
                    });   
                     
}
    //$.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=large-dataset.json&callback=?', function (data) {    });generateChartData()

//var params= new Array({  'type_bonds':type_bonds,   'is_demo':0, 'date_create':date_create,   'json':'1'   });    


    
$('#chart').highcharts('StockChart', {
    chart: {
       events: {
            load: function () {
                var series = this.series[0];
                setInterval(function () {
                    newPriceChartData(series);  
                    }, 10000);    
            }
        }
    },

    rangeSelector: {
        buttons: [{
      
            count: 5,
            type: 'minute',
            text: '5 м.'
        }, 
      {
            count: 10,
            type: 'minute',
            text: '10 м.'
        }, 
      {
            count: 1,
            type: 'hour',
            text: '1 ч'
        },
        {
            count: 1,
            type: 'day',
            text: '1 d'
        }, 

        {
            type: 'all',
            text: 'All'
        }],
        inputEnabled: false,
        selected: 3
    },
     xAxis: {
                type: 'datetime',
            //  tickPixelInterval: 1
            },
    title: {
        text: '{$info.title}'
    },

    series: [{
        name: '{$info.short_name}',
        data:generateChartData(),
type:'area',
        threshold:null,// 1168.29,
     //   pointInterval: 60*60,
     //   pointStart: new Date('2016/10/16 10:00:00').getTime()+ (3*3600000),
     connectNulls: true,
      pointInterval: 3600 ,
      
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, Highcharts.getOptions().colors[0]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
            ]
            },
        tooltip: {
            valueDecimals: 2
        }
    }]
});
/*
    $('#chart').highcharts('StockChart', {
    chart: {
       events: {
            load: function () {
                var series = this.series[0];
                setInterval(function () {
                    newPriceChartData(series);  
                    }, 10000);    
            }
        }
    },

    rangeSelector: {
        buttons: [{
      
            count: 5,
            type: 'minute',
            text: '5 м.'
        }, 
      {
            count: 10,
            type: 'minute',
            text: '10 м.'
        }, 
      {
            count: 1,
            type: 'hour',
            text: '1 ч'
        },
        {
            count: 1,
            type: 'day',
            text: '1 d'
        }, 

        {
            type: 'all',
            text: 'All'
        }],
      
        selected: 3
    },
     xAxis: {
                type: 'datetime',
            },
    title: {
        text: '{$info.title}'
    },

    series: [{
        name: '{$info.short_name}',
        data:generateChartData(),
        type: 'area',
        threshold:null,// 1168.29,
     //   pointInterval: 60*60,
     //   pointStart: new Date('2016/10/16 10:00:00').getTime()+ (3*3600000),
      
        fillColor: {
            linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
            },
            stops: [
                [0, Highcharts.getOptions().colors[0]],
                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
            ]
            },
        tooltip: {
            valueDecimals: 2
        }
    }]
});
*/
});
	</script>