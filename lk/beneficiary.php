<?   
$client=New Client();
$bondsOrder=New BondsOrder();
$project=New Project();

$breadcrumb[]=array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link');
$bn='';
$tt='';

if (isset($_GET['table'])){
	$TEMPLATE='bond_order_table.html';
	
 #ffffff
	$regBond=isset($_GET['registration'])?1:0;
	
	if ($regBond){
		$bn='Список заявок';      
		$tt='Список  заявок оформления в собственность'; 
	} else{
		$bn='Список заявок';
		$tt='Список  заявок выпускающихся на биржу';     
	} 

   // $smarty->assign('BondsOrderList',$bondsOrder->GetBondOrderList()); 
	
	//echo $regBond; exit;
		$param=array(
				(id_user)=>$_SESSION['site_user'],
				(is_registration)=>$regBond
			);
		if (isset($_GET['vis'])) $param['vis']=$_GET['vis'];

		$list=$bondsOrder->GetBeneficiaryBondOrderList($param);
		$smarty->assign('BeneficiaryBondOrderList',$list['row']);

	//print_r($a); exit;

	
	
	
}
if (isset($_GET['new'])){
   

		$TEMPLATE='bond_order_beneficiary_edit.html';  
		if (isset($_GET['registration'])) $breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=beneficiary&registration&table','type'=>'link');
		else 	$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=beneficiary&table','type'=>'link');
		$bn='Акции на биржу';   
		$tt='Заявка выпуска акций на биржу';
		
		$smarty->assign('Project',$project->GetProjectList(array('vis'=>1)));
		
		if (isset($_POST['command']))
		{ 
			unset($_POST['command']);  
			//unset($_POST['to_market_in_amount']);  
			unset($_POST['capital_in_percent']);
			unset($_POST['capital_in_stock']);
			//unset($_POST['value_of_stock']);
			unset($_POST['to_market_in_percent']);
			
			//print_r($_POST); exit;
			$bondsOrder->CreateBeneficiaryBondOrder($_POST); 
		}        

//print_r($a); exit;
}

if (isset($_GET['edit']) && !isset($_GET['beneficiary'])){
   
	$TEMPLATE='mg/bond_order_edit.html';     
	$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table','type'=>'link');
		
	$bn='Заявка на выпуск акции';   
	$tt='Форма заявки на выпуск акции'; 
	
	if (isset($_POST['command'])){
		if ($_POST['command']=='save'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->EditBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));}
		if ($_POST['command']=='cancel'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->CancelBondOrder(array( 'id'=>$_GET['id']));}
		if ($_POST['command']=='approve'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->ApproveBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));}
		  //  echo $ref; exit; 
			header("location:/?p={$_GET['p']}&table&alert=Запись №{$_GET['id']} успешно изменена");
	}    
	$smarty->assign('Project',$project->GetProjectList());

	$info= $bondsOrder->GetBondOrderInfo(array('id'=>$_GET['id']));           
   
	$smarty->assign('order_info',$info['row']); 
}  
	
if (isset($_GET['edit']) && isset($_GET['beneficiary'])){
		$TEMPLATE='mg/bond_order_beneficiary_edit.html';     
		$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table','type'=>'link');
			
		$bn='Заявка на собственность';   
		$tt='Форма заявки на оформление акций в собственность'; 
		
		if (isset($_POST['command'])){
			if ($_POST['command']=='save'){ 
				unset($_POST['command']);             
				unset($_POST['capital_in_percent']);
				unset($_POST['capital_in_stock']);
				unset($_POST['value_of_stock']);
				unset($_POST['to_market_in_percent']);
				
				$bondsOrder->EditBeneficiaryBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));
				header("location:/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено");
				}
			if ($_POST['command']=='cancel'){ 
				$bondsOrder->CancelBondOrder(array( 'id'=>$_GET['id']));}
			if ($_POST['command']=='approve'){ 
				$bondsOrder->ApproveBeneficiaryBondOrder(array('id'=>$_GET['id']));}
			  //  echo $ref; exit; 
				header("location:/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено");
		}    

		$info=$bondsOrder->GetBeneficiaryBondOrderInfo(array('id'=>$_GET['id']));
		$smarty->assign('order_info',$info['row']); 
 
}  
//print_r($breadcrumb); exit;
$smarty->assign('breadcrumb', $breadcrumb);
$smarty->assign('breadcrumb_now', $bn);   // Заголовок хлебных крошек
$smarty->assign('TableTitle', $tt); // заголовок таблицы
	
?>