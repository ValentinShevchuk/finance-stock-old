<?
// Исполнение на всех страницах, постоянная инфа которая должна быть в лк на протяжении всего сеанса

$smarty->assign('GET',$_GET); // передаем строку запроса в smarty чтобы обращаться к GET без $smarty.get
$smarty->assign('POST',$_POST); // передаем строку запроса в smarty чтобы обращаться к POST без $smarty.post
if (isset($_GET['p']) && file_exists('/js/'.$_GET['p'].'.js')) { $smarty->assign('file_js','../js/'.$_GET['p'].'.js');}

$smarty->assign('index_menu',$_SESSION['mg']."index_menu.html");    // подгружаем левое меню, в зависимости от MG или LK
if ($_SESSION['mg']=='')
{
    // клиент биржи
    $Account=New Account();
    $Bond=New Bond();
    
    $row=$Account->getAccountList(array('session_user'=>true));
    
    $smarty->assign('AccountList',$row);
    
    $row=$Bond->GetOpenPositionList(array('id_user'=>$_SESSION['site_user']));
    //print_r($row); exit;
    $smarty->assign('widget_open_position',$row);   
}

$enter=New Enter();
$enter->UpdateIOClient();

?>