<?
$Client=New Client();
$Bond=New Bond();
if (isset($_GET['table'])){
    $TEMPLATE='position_table.html';   
    $smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link')));
    $bn='Счета клиентов';   
    $tt='Список счетов'; 
    
    if (isset($_GET['close'])){
        
        $bn='Закрытые позиции';   
        $tt='Списки закрытых позиций'; 
        
        $list=$Bond->GetClosePositionList(array( 'id_user'=>$_SESSION['site_user']));
        //print_r($row); exit;
        $row_temp=$list['row'];
        foreach ($row_temp as $key=>$item)
        {
            $row_temp[$key]['diff']=round(($row_temp[$key]['price_sell']*$row_temp[$key]['bonds_count'])-($row_temp[$key]['price_buy']*$row_temp[$key]['bonds_count']),2);
            $row_temp[$key]['percent']=round(($row_temp[$key]['diff']*100)/($row_temp[$key]['price_sell']*$row_temp[$key]['bonds_count']),2);        
            if ($row_temp[$key]['diff']>=0) $row_temp[$key]['polar']='up'; else $row_temp[$key]['polar']='down';
        }    
        $list['row']=$row_temp;
        $smarty->assign('close_position',$list['row']);          
    }
    else{
        
        $bn='Открытые позиции';   
        $tt='Списки открытых позиций'; 
        
        //  $row=$Bond->GetOpenPositionList(array('id_user'=>$_SESSION['site_user'], 'id_bond_name'=>(int)$_GET['id']));
        $list=$Bond->GetOpenPositionList(array( 'id_user'=>$_SESSION['site_user']));
        //print_r($row); exit;
        $smarty->assign('open_position',$list['row']);   
   }
}

 $smarty->assign('breadcrumb_now',$bn);   // Заголовок хлебных крошек
 $smarty->assign('TableTitle',$tt); // заголовок таблицы
?>