<?php
header("Content-Type: text/html; charset=UTF-8");
SESSION_START();

$path=str_replace("/", "\\", $_SERVER['DOCUMENT_ROOT']);
define('PATH_SERVER',$path);
define('server',PATH_SERVER);

$_SESSION['SMARTY_DIR']='..\functions\smarty\\';
$_SESSION['GAME_DIR']=PATH_SERVER.'\\';

require_once('../functions/smarty/Smarty.class.php');
$GLOBALS['smarty'] = new Smarty();
$GLOBALS['smarty']->template_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates/';
$GLOBALS['smarty']->compile_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates_c/';

require_once('../../bonds.database.php');  // база нужна для генерации смс кода

/*require_once('../functions/bonds.php'); // старые функции Счета клиентов и работа с акциями
require_once('../functions/bonds.direction.php');*/
require_once('../functions/enter.php');

require_once('../functions/stock.clients.php'); // работа с инфой о клиентах
require_once('../functions/stock.projects.php'); // проекты
require_once('../functions/stock.bonds.order.php'); // заявки на акции
require_once('../functions/stock.account.php'); // счета клиентов Новые функции
require_once('../functions/stock.stats.php'); 
require_once('../functions/stock.bond.php');
require('../functions/class.phpmailer.php');
require('../functions/class.smtp.php');
require_once('../functions/stock.files.php');

require_once('../functions/stock.email.php');
require_once('../functions/stock.search.php');
?>