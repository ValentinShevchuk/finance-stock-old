<?   
$client=New Client();
$bondsOrder=New BondsOrder();
$project=New Project();

$breadcrumb[]=array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link');
$bn='';$tt='';

if (isset($_GET['table'])){

	$TEMPLATE='mg/bond_order_table.html';
	
	$bn='Список заявок';
	
	$tt='Список заявок акций на биржу';
	
   if (!isset($_GET['beneficiary'])){ 
	   
		if (isset($_GET['vis'])) { 
						   
			if ($_GET['vis']==2) { 
				$bn='Новые заявки';
				$tt='Список новых заявок требующих оформления'; 
			}       
			
			$smarty->assign('BondsOrderList',$bondsOrder->GetBondOrderList(array('vis'=>$_GET['vis'])));
		}
		else { 
			$bn='Список заявок';    
			$tt='Список  заявок выпускающихся на биржу';      
			$smarty->assign('BondsOrderList',$bondsOrder->GetBondOrderList()); 
		}
	}

	
	if (isset($_GET['beneficiary'])){
		
		if (!isset($_GET['registration'])){ 
			
			$bn='Список заявок';     
			 
			$tt='Список  заявок выпускающихся на биржу';   
			if (isset($_GET['vis'])) $param['vis']=$_GET['vis'];
			$list=$bondsOrder->GetBeneficiaryBondOrderList($param);
			
			$smarty->assign('BeneficiaryBondOrderList',$list['row']);
		}
		
		if (isset($_GET['registration'])){
			$bn='Список заявок';     
			$tt='Список  заявок выпускающихся на биржу';   
			$param=array('is_registration'=>1);
			if (isset($_GET['vis'])) $param['vis']=$_GET['vis'];
			$list=$bondsOrder->GetBeneficiaryBondOrderList($param);
			$smarty->assign('BeneficiaryBondOrderList',$list['row']);
		}
	}
	
}
if (isset($_GET['new'])){
   
	if (!isset($_GET['beneficiary'])) {
		 
		$TEMPLATE='mg/bond_order_edit.html';  
		$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table','type'=>'link');
		$bn='Акции на биржу';   
		$tt='Заявка выпуска акций на биржу';
		$smarty->assign('Project',$project->GetProjectList(array('vis'=>1)));
		if (isset($_POST['command']))
		{ 
			unset($_POST['command']);  unset($_POST['to_market_in_amount']);  
			$bondsOrder->CreateBondOrder($_POST); 
			header("location:/?p={$_GET['p']}&table&alert=Успешно выполнено");
		}    

	
	}
	if (isset($_GET['beneficiary'])) {
		
		$TEMPLATE='mg/bond_order_beneficiary_edit.html';  
		$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table&beneficiary','type'=>'link');
		//print_r($breadcrumb); exit;
		if (isset($_GET['registration']))  $breadcrumb[1]['link']='/?p=bond_order&table&beneficiary&registration';
		$bn='Акции на биржу';   
		$tt='Заявка выпуска акций на биржу';
		
		$smarty->assign('Project',$project->GetProjectList(array('vis'=>1)));
		
		if (isset($_POST['command']))
		{ 
			unset($_POST['command']);  
			//unset($_POST['to_market_in_amount']);  
			unset($_POST['capital_in_percent']);
			unset($_POST['capital_in_stock']);
			//unset($_POST['value_of_stock']);
			unset($_POST['to_market_in_percent']);
			//print_r($_POST);
			$bondsOrder->CreateBeneficiaryBondOrder($_POST); 
			$url="/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено";
			if (isset($_GET['registration'])) $url.="&registration";
			header("location:$url");
		}
	}
//print_r($a); exit;
}

if (isset($_GET['edit']) && !isset($_GET['beneficiary'])){
   
	$TEMPLATE='mg/bond_order_edit.html';     
	$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table','type'=>'link');
		
	$bn='Заявка на выпуск акции';   
	$tt='Форма заявки на выпуск акции'; 
	
	if (isset($_POST['command'])){
		if ($_POST['command']=='save'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->EditBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));}
		if ($_POST['command']=='cancel'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->CancelBondOrder(array( 'id'=>$_GET['id']));}
		if ($_POST['command']=='approve'){ 
			unset($_POST['command']); unset($_POST['to_market_in_amount']);  
			$bondsOrder->ApproveBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));}
		  //  echo $ref; exit; 
			header("location:/?p={$_GET['p']}&table&alert=Запись №{$_GET['id']} успешно изменена");
	}    
	$smarty->assign('Project',$project->GetProjectList());

	$info= $bondsOrder->GetBondOrderInfo(array('id'=>$_GET['id']));           
   
	$smarty->assign('order_info',$info['row']); 
}  
	
if (isset($_GET['edit']) && isset($_GET['beneficiary'])){
	$TEMPLATE='mg/bond_order_beneficiary_edit.html';     
	$breadcrumb[]=array('title'=>'Список заявок', 'link'=>'/?p=bond_order&table','type'=>'link');
		
	$bn='Заявка на выпуск акции';   
	$tt='Форма заявки на выпуск акции'; 
	
	if (isset($_POST['command'])){
		if ($_POST['command']=='save'){ 
			unset($_POST['command']);             
			unset($_POST['capital_in_percent']);
			unset($_POST['capital_in_stock']);
			unset($_POST['value_of_stock']);
			unset($_POST['to_market_in_percent']);
			
			$bondsOrder->EditBeneficiaryBondOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));
			//header("location:/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено");
		}
		if ($_POST['command']=='cancel'){ 
			$param=array( 'id'=>$_GET['id']);
			if (isset($_POST['cancel_reason'])) {
				$param['POST']['cancel_reason']=$_POST['cancel_reason'];
				$param['POST']['is_cancel']=$_POST['is_cancel'];
			}
			//rint_r($param); exit;
			$bondsOrder->CancelBeneficiaryBondOrder($param);
		}
		if ($_POST['command']=='approve'){ 
			$bondsOrder->ApproveBeneficiaryBondOrder(array('id'=>$_GET['id'], 'is_registration'=>$_POST['is_registration']));
		}
			//  echo $ref; exit; 
		$url="/?p={$_GET['p']}&table&beneficiary&alert=Запись №{$_GET['id']} успешно изменена";
		if (isset($_GET['registration'])) $url.="&registration";
		header("location:$url");
	}    
		
		
		
		$info=$bondsOrder->GetBeneficiaryBondOrderInfo(array('id'=>$_GET['id']));
		//print_r($info); exit;
		$smarty->assign('order_info',$info['row']); 
		
}  
//print_r($breadcrumb); exit;
$smarty->assign('breadcrumb', $breadcrumb);
$smarty->assign('breadcrumb_now', $bn);   // Заголовок хлебных крошек
$smarty->assign('TableTitle', $tt); // заголовок таблицы
	
?>