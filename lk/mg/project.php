<?
	
	/// ALTER TABLE `project`  ADD `descr` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'описание для каталога'  AFTER `title`,  ADD `body` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'визитная карточка'  AFTER `descr`;
$project=New Project();
$client = New Client();
$files=New Files();
if (isset($_GET['table'])){

	if (isset($_GET['beneficiary'])){
		$TEMPLATE='mg/project_table.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список проектов', 'link'=>'/?p=project&table','type'=>'link')));
		$smarty->assign('breadcrumb_now','Список бенефициаров');
		$smarty->assign('TableTitle','Список бенефициаров');
		//if (isset($_GET['vis'])) $smarty->assign('Projects',$projects->GetProjectsList(array(array('vis'=>$_GET['vis'])))); else
		if (isset($_GET['vis'])) $arr['vis']=$_GET['vis'];
		if (isset($_GET['id_project'])) $arr['id_project']=$_GET['id_project'];

		 $smarty->assign('Beneficiary',$project->GetProjectBeneficiaryList(($arr)));

	}
	else
	if (isset($_GET['news'])){
		$TEMPLATE='mg/project_table.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список проектов', 'link'=>'/?p=project&table','type'=>'link')));
		$smarty->assign('breadcrumb_now','Список новостей');
		$smarty->assign('TableTitle','Список экономических новостей');

		//if (isset($_GET['vis'])) $smarty->assign('Projects',$projects->GetProjectsList(array(array('vis'=>$_GET['vis'])))); else
		if (isset($_GET['vis'])) $arr['vis']=$_GET['vis'];
		if (isset($_GET['id_project'])) $arr['id_project']=$_GET['id_project'];

		$smarty->assign('List',$row=$project->GetProjectNewsList(($arr)));
		//print_r($row); exit;

	}
	else
	{
		$TEMPLATE='mg/project_table.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link')));
		$smarty->assign('breadcrumb_now','Список проектов');
		$smarty->assign('TableTitle','Список проектов');
		$arr['get_bond_price']=1;
		if (isset($_GET['vis'])) $arr['vis']=$_GET['vis'];

		$smarty->assign('Project',$a=$project->GetProjectList($arr));
	}
}
if (isset($_GET['new'])){
	if (isset($_GET['beneficiary'])){
		$TEMPLATE='mg/project_beneficiary.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список бенефициаров', 'link'=>'/?p=project&table&beneficiary','type'=>'link')));
		$smarty->assign('breadcrumb_now','Новый бенефициар');
		$smarty->assign('TableTitle','Форма создания нового бенефициара');

		$list=$client->GetClientList(array('is_beneficiary'=>1));
		$smarty->assign('ClientList',$list);

		$list=$project->GetProjectList(array('is_beneficiary'=>1, 'vis'=>1));
		$smarty->assign('CompanyList',$list);

		$row=$project->GetProjectList(array('is_stock'=>true)); // убрал условие ТОЛЬКО с акциями так как акции надо продать бенефициарам, фиг знает - оставил пока так
		//print_r($row);
		// exit;
		$smarty->assign('Project',$row);
		
	   // $smarty->assign('Project',$project->GetProjectList(array('is_stock'=>true)));
		
		
		if (isset($_POST['command'])){
			//print_r($_POST);
			unset($_POST['command']);
			unset($_POST['value_of_stock']);
			$project->CreateProjectBeneficiary($_POST);     
			header("location:/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено");
		}
	}
	elseif (isset($_GET['news'])){
		$TEMPLATE='mg/project_news.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список проектов', 'link'=>'/?p=project&table','type'=>'link'),array('title'=>'Список новостей', 'link'=>'/?p=project&table&news','type'=>'link')));
		$smarty->assign('breadcrumb_now','Новая новость');
		$smarty->assign('TableTitle','Форма создания новости');
		$list=$project->GetProjectList();
		//print_r($list); exit;
		$smarty->assign('ProjectList',$list);

		if (isset($_POST['command'])){ unset($_POST['command']);  $project->CreateProjectNews($_POST); header("location:/?p=project&table&news"); }
	}
	else{
		$TEMPLATE='mg/project_edit.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список проектов', 'link'=>'/?p=project&table','type'=>'link')));
		$smarty->assign('breadcrumb_now','Новый проект');
		$smarty->assign('TableTitle','Форма создания нового проекта');
		if (isset($_POST['command']))
		{ 
			unset($_POST['command']); 
			$project->CreateProjectOrder(array('POST'=>$_POST)); 
			header("location:/?p={$_GET['p']}&table&alert=Успешно выполнено");
		}
	}
}
if (isset($_GET['edit'])){

	if (isset($_GET['beneficiary'])){

		$TEMPLATE='mg/project_beneficiary.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список бенефициаров', 'link'=>'/?p=project&table&beneficiary','type'=>'link')));
		$smarty->assign('breadcrumb_now','Редактировать бенефициара');
		$smarty->assign('TableTitle','Форма редактирования бенефициара');

		if (isset($_POST['command'])){
			if ($_POST['command']=='save'){
				   // print_r($_POST); exit;
				unset($_POST['command']);
				if (!empty($_POST)) $project->EditProjectBeneficiary(array('POST'=>$_POST, 'id'=>$_GET['id']));
				
				}
			if ($_POST['command']=='cancel'){
				   // print_r($_POST); exit;
				unset($_POST['command']);
				$project->CancelProjectBeneficiary(array( 'id'=>$_GET['id']));
				}
			if ($_POST['command']=='approve'){
				   // print_r($_POST); exit;
				unset($_POST['command']);
				$project->ApproveProjectBeneficiary(array( 'id'=>$_GET['id']));
				}
			header("location:/?p={$_GET['p']}&table&beneficiary&alert=Успешно выполнено");
		}


		$list=$client->GetClientList(array('is_beneficiary'=>1));
		$smarty->assign('ClientList',$list);

		$list=$project->GetProjectList(array('is_beneficiary'=>1, 'vis'=>1));
		$smarty->assign('CompanyList',$list);

		$beneficiaryInfo=$project->GetBeneficiaryInfo(array('id'=>$_GET['id']));
	   // print_r($beneficiaryInfo); exit;
		if ($beneficiaryInfo['error']=='0') $smarty->assign('order_info',$beneficiaryInfo['row']);
		else  header("location:/?p={$_GET['p']}&table&alert=".$beneficiaryInfo['error_msg']);

		$row=$project->GetProjectList();
		//print_r($row); exit;
		$smarty->assign('Project',$row);



	}
	elseif (isset($_GET['news'])){

		$TEMPLATE='mg/project_news.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список бенефициаров', 'link'=>'/?p=project&table&beneficiary','type'=>'link')));
		$smarty->assign('breadcrumb_now','Редактировать бенефициара');
		$smarty->assign('TableTitle','Форма редактирования бенефициара');

		if (isset($_POST['command'])){
			$comm=$_POST['command'];unset($_POST['command']);

			if ($comm=='save'){
				   // print_r($_POST); exit;

				if (!empty($_POST)) $project->EditProjectNews(array('POST'=>$_POST, 'id'=>$_GET['id']));
				}
			if ($comm=='cancel'){
				   // print_r($_POST); exit;

				$project->CancelProjectNews(array( 'id'=>$_GET['id']));
				}
			if ($comm=='approve'){
				   // print_r($_POST); exit;

				$project->ApproveProjectNews(array( 'id'=>$_GET['id']));
				}
			header("location:/?p={$_GET['p']}&table&news&alert=Успешно выполнено");
		}


		$list=$project->GetProjectList();
		$smarty->assign('ProjectList',$list);

		$order_Info=$project->GetProjectNewsInfo(array('id'=>$_GET['id']));
		$smarty->assign('order_info',$order_Info['row']);



	}
	else{
		$TEMPLATE='mg/project_edit.html';
		$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список проектов', 'link'=>'/?p=project&table','type'=>'link')));
		$smarty->assign('breadcrumb_now','Новый проект');
		$smarty->assign('TableTitle','Форма создания нового проекта');

		if (isset($_POST['command'])){
			if ($_POST['command']=='save'){
				//print_r($_POST); exit;
				unset($_POST['command']);
				//print_r($_POST); exit;
				if (!empty($_POST)) $project->EditProjectOrder(array('POST'=>$_POST, 'id'=>$_GET['id']));
			}

			if ($_POST['command']=='cancel'){
				unset($_POST['command']);
				$project->CancelProjectOrder((array('id'=>$_GET['id'])));}

			if ($_POST['command']=='approve'){
				unset($_POST['command']);
				$project->ApproveProjectOrder((array('id'=>$_GET['id'])));}
			header("location:/?p={$_GET['p']}&table&alert=Успешно выполнено");
		}
		$info=$project->GetProjectInfo(array('id'=>$_GET['id']));
		$smarty->assign('Project',$info['row']);

		$info=$project->GetProjectBeneficiaryList(array('id_project'=>$_GET['id']));
		$smarty->assign('Beneficiary',$info['row']);

		$info=$files->getFileList(array(
			(own_id)=>$_GET['id'], 
			(tableNameInt)=>1)
		);
		//print_r($info); exit;
		$smarty->assign('ProjectFile',$info['row']);
	}
   // print_r($a); exit;
}
?>