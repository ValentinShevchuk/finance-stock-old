<?
$Account=New Account();
$Client=New Client();
$Bond=New Bond();
if (isset($_GET['table'])){
    $TEMPLATE='mg/position_table.html';   
    $smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link')));
    $bn='Счета клиентов';   
    $tt='Список счетов'; 
    
    if (isset($_GET['close'])){
        $bn='Закрытые позиции';   
        $tt='Списки закрытых позиций'; 
        

        $list=$Bond->GetClosePositionList(array('is_demo'=>0));
        //print_r($row); exit;
        $row_temp=$list['row'];
        foreach ($row_temp as $key=>$item)
        {
            $row_temp[$key]['diff']=round(($row_temp[$key]['price_sell']*$row_temp[$key]['bonds_count'])-($row_temp[$key]['price_buy']*$row_temp[$key]['bonds_count']),2);
            $row_temp[$key]['percent']=round(($row_temp[$key]['diff']*100)/($row_temp[$key]['price_sell']*$row_temp[$key]['bonds_count']),2);        
            if ($row_temp[$key]['diff']>=0) $row_temp[$key]['polar']='up'; else $row_temp[$key]['polar']='down';
        }    
        $list['row']=$row_temp;
        $smarty->assign('close_position',$list['row']);          
    }
    else{
        $bn='Открытые позиции';   
        $tt='Списки открытых позиций'; 
        
        //  $row=$Bond->GetOpenPositionList(array('id_user'=>$_SESSION['site_user'], 'id_bond_name'=>(int)$_GET['id']));
        $list=$Bond->GetOpenPositionList(array('is_demo'=>0));
        //print_r($list); exit;
        $smarty->assign('open_position',$list['row']);   
    }
}
if (isset($_GET['edit'])){ 
  
    $TEMPLATE='mg/account_edit.html';     
    $smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список счетов', 'link'=>'/?p=position&table','type'=>'link')));
    $bn='Просмотр профиля';   
    $tt='Форма редактирования счета'; 
    
    if (isset($_POST['command'])){
        $comm=$_POST['command'];   unset($_POST['command']);
   //     if ( $comm=='save'){   //print_r($_POST); exit;    $client->EditClientOrder((array('POST'=>$_POST, 'id'=>$_GET['id'])));} Нельзя редактировать
            
        if ( $comm=='approve'){ 
    
            $res=$Account>ApproveAccount(array('id'=>$_GET['id']));
           //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
        }    
        if ( $comm=='cancel'){ 
   
            $res=$Account->CancelAccount(array('id'=>$_GET['id']));
           //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
        }    
}    


    $info=$Account->GetAccountInfo(array('id'=>$_GET['id']));
    //print_r($info); exit;
    $smarty->assign('order_info',$info['row']);
  //  $smarty->assign('order_info_stats',$info['stats']);
    
    
 //   $smarty->assign('AccountList',$row=$account->GetAccountList(array('id_user'=>$_GET['id'])));
    //print_r($row);
    
   //print_r($row); exit;
}   

$smarty->assign('breadcrumb_now',$bn);   // Заголовок хлебных крошек
$smarty->assign('TableTitle',$tt); // заголовок таблицы
?>