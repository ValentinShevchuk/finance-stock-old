<?
$label=array(
	'type_transfer'=>array(     
		array('id'=>1,'title'=>'Зачисление'),
		array('id'=>2,'title'=>'Списание'),
		array('id'=>3,'title'=>'Между счетами')       
	),
	'method_transfer'=>array(  
		array('id'=>1,'title'=>'QIWI'),
		array('id'=>2,'title'=>'Реальный счет'),
		array('id'=>3,'title'=>'Наличка')   
	),
	'purpose'=>array(  
		array('id'=>1,'title'=>'Пополнение счета'),
		array('id'=>2,'title'=>'Выплата бонусов клиенту'),
		array('id'=>3,'title'=>'Оплата за привлечение'),
		array('id'=>4,'title'=>'Вывод денежных средств')         
	),
);
$smarty->assign('label',$label);

$Account=New Account();
$Client=New Client();
$Bond=new Bond();
$Beneficiary=new Project();

if (isset($_GET['table'])  && !isset($_GET['transaction'])){
	$TEMPLATE='mg/account_table.html';   
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список клиентов', 'link'=>'/?p=client&table','type'=>'link')));
	$bn='Счета клиентов';   
	$tt='Список счетов'; 
	$row=$Account->GetAccountList();
	$smarty->assign('List',$row['row']);
	//print_r($row); exit;
}
if (isset($_GET['table']) && isset($_GET['transaction'])){
	$TEMPLATE='mg/account_transaction_table.html';   
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список транзакций', 'link'=>'/?p='.$_GET['p'].'&table&transaction','type'=>'link')));
	$bn='Список транзакций';   
	$tt='Список транзакций'; 
	if (isset($_GET['bookkeeping'])) $row=$Account->GetTransactionList(array('id_account'=>1, 'id_account_to'=>1));
	else $row=$Account->GetTransactionList();
	$smarty->assign('List',$row['row']);
	//print_r($row); exit;
}

if (isset($_GET['history'])){
	$TEMPLATE='mg/account_history.html';   
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список счетов', 'link'=>'/?p=account&table','type'=>'link')));
	$bn='История операций';   
	$tt='История по счёту'; 
	
	$row=$Account->GetAccountInfo(array('id'=>$_GET['id']));
	$tt.=" / {$row['row']['full_account']} Баланс:{$row['row']['value']}";
	$row=$Account->GetAccountHistory(array('id_account'=>$_GET['id']));
	$smarty->assign('List',$row['row']);
	//print_r($row); exit;
}
else if (isset($_GET['new']) && !isset($_GET['transaction'])){   

	$TEMPLATE='mg/account_edit.html'; 
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список счетов', 'link'=>'/?p=account&table','type'=>'link')));
	$bn='Просмотр профиля';   
	$tt='Форма редактирования счета'; 


		if (isset($_POST['command'])){ unset($_POST['command']);  $Account->CreateAccount($_POST); }

   
	   
	$list=$Client->GetClientList(array('vis'=>1));
	$smarty->assign('ClientList',$list);
}

else if (isset($_GET['new']) && isset($_GET['transaction'])){   

	$TEMPLATE='mg/account_transaction_edit.html'; 
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список транзакций', 'link'=>'/?p=account&transaction&table','type'=>'link')));
	$bn='Создание транзакции';   
	$tt='Форма создания транзакции'; 
	if (isset($_POST['command'])){ 
		$comm=$_POST['command'];   unset($_POST['command']);
		//print_r($_POST); exit;
		$Account->CreateTransaction(array('POST'=>$_POST, 'id'=>$_GET['id']));
	   header("location:/?p={$_GET['p']}&table&transaction&alert=Успешно выполнено");
	}   
	   
	$list=$Client->GetClientList(array('vis'=>1, 'is_beneficiary'=>1)); // 
	$smarty->assign('ClientList',$list);
	
}


else if (isset($_GET['edit']) && !isset($_GET['transaction'])){ 
  
	$TEMPLATE='mg/account_edit.html';     
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список счетов', 'link'=>'/?p=account&table','type'=>'link')));
	$bn='Просмотр профиля';   
	$tt='Форма редактирования счета'; 
	
	if (isset($_POST['command'])){
		$comm=$_POST['command'];   unset($_POST['command']);
   //     if ( $comm=='save'){   //print_r($_POST); exit;    $client->EditClientOrder((array('POST'=>$_POST, 'id'=>$_GET['id'])));} Нельзя редактировать
			
		if ( $comm=='approve'){ 
	
			$res=$Account>ApproveAccount(array('id'=>$_GET['id']));
		   //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
		}    
		if ( $comm=='cancel'){ 
   
			$res=$Account->CancelAccount(array('id'=>$_GET['id']));
		   //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
		}    
}    


	$info=$Account->GetAccountInfo(array('id'=>$_GET['id']));
	$smarty->assign('order_info',$info['row']);

	$q="SELECT full_name FROM bond t1, project_beneficiary t2 WHERE t1.id_project_beneficiary=t2.id AND t2.id_client='{$info['row']['id_user']}'";
	$row=$Bond->db->getAll($q);
	$smarty->assign('bond_list',$row);

}  
else if (isset($_GET['edit']) && isset($_GET['transaction'])){ 
  
	$TEMPLATE='mg/account_transaction_edit.html';     
	$smarty->assign('breadcrumb', array( array('title'=>'Рабочий стол', 'link'=>'/','type'=>'link'),array('title'=>'Список транзакций', 'link'=>'/?p=account&transaction&table','type'=>'link')));
	
	$bn='Просмотр профиля';   
	$tt='Форма редактирования счета'; 
	
	if (isset($_POST['command'])){
		$comm=$_POST['command'];   unset($_POST['command']);
   //     if ( $comm=='save'){   //print_r($_POST); exit;    $client->EditClientOrder((array('POST'=>$_POST, 'id'=>$_GET['id'])));} Нельзя редактировать
			
		if ( $comm=='approve'){    
			$res=$Account->ApproveTransaction(array('id'=>$_GET['id'], 'POST'=>$_POST));
		   //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
		}    
		if ( $comm=='cancel'){ 
   
			$res=$Account->CancelTransaction(array('id'=>$_GET['id']));
		   //  if ($res['error']=='0') echo "Все ок"; else echo $res['error_msg'];print_r($res); exit;
		}    
		header("location:/?p={$_GET['p']}&table&transaction&alert=Успешно выполнено");
	}    

	$list=$Client->GetClientList(array('vis'=>1, 'is_beneficiary'=>1));
	$smarty->assign('ClientList',$list);
	

}   
 $smarty->assign('breadcrumb_now',$bn);   // Заголовок хлебных крошек
 $smarty->assign('TableTitle',$tt); // заголовок таблицы
?>