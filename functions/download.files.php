<?php

function filesize2bytes($str) {
    $bytes = 0;

    $bytes_array = array(
        'B' => 1,
        'KB' => 1024,
        'MB' => 1024 * 1024,
        'GB' => 1024 * 1024 * 1024,
        'TB' => 1024 * 1024 * 1024 * 1024,
        'PB' => 1024 * 1024 * 1024 * 1024 * 1024,
    );

    $bytes = floatval($str);

    if (preg_match('#([KMGTP]?B)$#si', $str, $matches) && !empty($bytes_array[$matches[1]])) {
        $bytes *= $bytes_array[$matches[1]];
    }

    $bytes = intval(round($bytes, 2));

    return $bytes;
}

function downloadPhoto( $width,$height,$pole1,$pole2,$pole3,$files)
{
//длинна, ширина, каталог, имя, описание, файл
	global $con;
	global $_POST;
	global $_FILES;
	
	$type= pathinfo($files["name"]);
	 
	if (!isset($pole2) or $pole2=='') $pole2=$type['basename'];
	if (!isset($pole3) or $pole3=='') $pole3=$type['basename'];

	$q = "INSERT INTO week_photo VALUES( null,'".mysql_real_escape_string($pole2)."','".mysql_real_escape_string($pole3)."', '".$type['extension']."',1)";

	$r = mysql_query($q, $con);           
	$id=mysql_insert_id();
	 
	 rtime(array('create',$_SESSION['admins'],'9',$id));
	
	
	echo "<div style='display:none'>";
	if (move_uploaded_file($files["tmp_name"] , server.'/photo/'.$id.'.'.$type['extension']))
		{
			$exet=$type['extension'];
			$image =imagecreateFromJpeg( server."/photo/".$id.".".$exet);
			
			
			$srcWidth  = imagesx($image);
			$srcHeight = imagesy($image);
			
			if ($height!==0) if ($height<imagesy($image)){$perc=(($height*100)/$srcHeight)/100;} else $perc=1;
			if ($width!==0) if ($width<imagesy($image)){$perc=(($width*100)/$srcWidth)/100;} else $perc=1;
			
			$dstWidth  =imagesx($image)*$perc;
			$dstHeight = imagesy($image)*$perc;
			$type=getimagesize(server."/photo/".$id.".".$exet);
			
			$dst = imagecreatetruecolor($dstWidth, $dstHeight);
			imagecopyresampled($dst, $image, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
			
			
			imagejpeg($dst, server."/photo/".$id."_.".$exet);
			imagedestroy($image);
			imagedestroy($dst);
			
			$str='Фотография успешно добавлена';
		}
	echo "</div>";
		$q="INSERT INTO week_photo_list VALUES(null,'".$pole1."','".$id."',1)";
		$r = mysql_query($q, $con);
		$id=mysql_insert_id();
		
		 rtime(array('create',$_SESSION['admins'],'32',$id));
		
		return $id;
}


function download_photo($massiv)
{
$width=$massiv[0];
$height=$massiv[1];
$pole1=$massiv[2];
$pole2=$massiv[3];
$pole3=$massiv[4];
$files=$massiv[5];
$type=$massiv[6];
//длинна, ширина, каталог, имя, описание, файл
	global $con;
	global $_POST;
	global $_FILES;
	
	//$type= pathinfo($files["name"]);
	 
	if (!isset($pole2) or $pole2=='') $pole2=$type['basename'];
	if (!isset($pole3) or $pole3=='') $pole3=$type['basename'];
	
	$q = "INSERT INTO week_photo VALUES( null,'".mysql_real_escape_string($pole2)."','".mysql_real_escape_string($pole3)."', '".$type['extension']."',1)";
//echo $q;
	$r = mysql_query($q, $con);
	$id=mysql_insert_id();
	 
	 rtime(array('create',$_SESSION['admins'],'9',$id));
	
	echo "<div style='display:none'>";
	if (move_uploaded_file($files, server.'/photo/'.$id.'.'.$type['extension'])) 
		{
			$exet=$type['extension'];
			$image =imagecreateFromJpeg( server."/photo/".$id.".".$exet);
			
			
			$srcWidth  = imagesx($image);
			$srcHeight = imagesy($image);
			
			if ($height!==0) if ($height<imagesy($image)){$perc=(($height*100)/$srcHeight)/100;} else $perc=1;
			if ($width!==0) if ($width<imagesy($image)){$perc=(($width*100)/$srcWidth)/100;} else $perc=1;
			
			$dstWidth  =imagesx($image)*$perc;
			$dstHeight = imagesy($image)*$perc;
			$type=getimagesize(server."/photo/".$id.".".$exet);
			
			$dst = imagecreatetruecolor($dstWidth, $dstHeight);
			imagecopyresampled($dst, $image, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
			
			
			imagejpeg($dst, server."/photo/".$id."_.".$exet);
			imagedestroy($image);
			imagedestroy($dst);
			
			$str='Фотография успешно добавлена';
		}
	echo "</div>";
		$q="INSERT INTO week_photo_list VALUES(null,'".$pole1."','".$id."',1)";
		$r = mysql_query($q, $con);
		$id=mysql_insert_id();
		
	 rtime(array('create',$_SESSION['admins'],'32',$id));
		
		return $id;
}




function downloadFiles( $pole1,$pole2,$pole3,$files)
{
//каталог, имя, описание, файл

	global $con, $_POST, $_FILES, $_SESSION;
	$type= pathinfo($files["name"]);
	 
	//if (!isset($pole2) or $pole2=='') $pole2=$type['basename'];
	//if (!isset($pole3) or $pole3=='') $pole3=$type['basename'];
	
	$q = "INSERT INTO week_files VALUES( null,'".$pole2."','".$pole3."','".date("Y-m-d H:i:s")."','".$_SESSION['login']."', 1,'".$type['extension']."')";
	$r = mysql_query($q, $con);
	$id=mysql_insert_id();

	 rtime(array('create',$_SESSION['admins'],'10',$id));

	echo "<div style='display:none'>";
	if (move_uploaded_file($files["tmp_name"] , server."/$files_category/$id.{$type['extension']}"))
		{
		}
	echo "</div>";

	$q="INSERT INTO week_folder_list VALUES(null,'".$pole1."','".$id."',1)";
	$r = mysql_query($q, $con);
	$id=mysql_insert_id();
	
	 rtime(array('create',$_SESSION['admins'],'33',$id));
		
		return $id;
}

function download_files($massiv)
{
	global $files_category;
	global $con, $_POST, $_FILES, $_SESSION, $site_, $admins_,$child_, $access_, $db;

	$pole1 = $massiv[0];
	$pole2 = $massiv[1];
	$pole3 = $massiv[2];
	$files = $massiv[3];
	$type  = $massiv[4];
	$path  = $massiv[5];

    if (!isset($admins_)){ $admins_ = $_SESSION['site_admin'];	$child_ = $_SESSION['site_admin'];	}
	if (!isset($pole2) or $pole2=='') $pole2=$type['basename'];

	//if (!isset($pole3) or $pole3=='') $pole3=$type['basename'];
    $file_type=1;
	$mark_file = array('png','jpg','jpeg','bmp','gif');
    $type['extension']=strtolower($type['extension']);
	
	//mark as an image file
    if (in_array($type['extension'],$mark_file))  { $file_type=2; }

	$db->query("INSERT INTO week_files VALUES( null,'".$pole2."','".$pole3."', $file_type,'".$type['extension']."','','".time()."',$admins_,$child_)"); 
	$id=$db->getOne("SELECT LAST_INSERT_ID();");
    $catalog=(int)($id/100);

    if (!is_dir("../$path$files_category/".$catalog)){
      mkdir("../$path$files_category/".$catalog,0777);
    }
	if (move_uploaded_file($files, "../$path$files_category/$catalog/$id.$type[extension]")){
		$size= filesize2bytes(filesize("../$path$files_category/$catalog/$id.$type[extension]"));
		$db->query("UPDATE week_files SET size='$size' WHERE id='$id'");
        if ($pole1!==''){
            $q="INSERT INTO week_folder_list VALUES(null,'".$pole1."','".$id."',1)";
		    $db->query($q);
		    $id2=$db->getOne("SELECT LAST_INSERT_ID();");
            }
        else $id2=0;
	}
		return array(1=>$id,2=>$id2);
}

function download_files_lite($massiv)
{
	global $files_category;
	global $con, $_POST, $_FILES, $_SESSION, $site_, $admins_,$child_, $access_, $db;

	$pole1 = $massiv[0];
	$pole2 = $massiv[1];
	$pole3 = $massiv[2];
	$files = $massiv[3];
	$type  = $massiv[4];

    if (!isset($admins_)){ $admins_ = $_SESSION['site_admin'];	$child_ = $_SESSION['site_admin'];	}
	if (!isset($pole2) or $pole2=='') $pole2=$type['basename'];
	//if (!isset($pole3) or $pole3=='') $pole3=$type['basename'];
    $file_type=1;
	$mark_file = array('png','jpg','jpeg','bmp','gif');
    $type['extension']=strtolower($type['extension']);

	//mark as an image file
    if (in_array($type['extension'],$mark_file))  { $file_type=2; }

	$db->query("INSERT INTO week_files VALUES( null,'".$pole2."','".$pole3."', $file_type,'".$type['extension']."','','".time()."',$admins_,$child_)");
	$id=$db->getOne("SELECT LAST_INSERT_ID();");
    $catalog=(int)($id/100);

    if (!is_dir("../$files_category/".$catalog)){
      mkdir("../$files_category/".$catalog,0777);
    }
	if (move_uploaded_file($files, "../$files_category/$catalog/$id.$type[extension]")){
		$size= filesize2bytes(filesize("../$files_category/$catalog/$id.$type[extension]"));
		$db->query("UPDATE week_files SET size='$size' WHERE id='$id'");

    	$q="INSERT INTO week_folder_list VALUES(null,'".$pole1."','".$id."',1)";
		$db->query($q);
		$id2=$db->getOne("SELECT LAST_INSERT_ID();");
	}
		return array(1=>$id,2=>$id2);
}

?>
