<?
class Client extends database {    
	
	public $account;
	public $stats;
	public $enter;
	public $email;
	function __construct()
	{        
		parent::__construct();
		$this->account=New Account();
		$this->stats=New Stats();
		$this->enter=New Enter();
		$this->email=New Email();        

		
	}
	
	public function GetClientNumbers($param=array())     // подсчёт заявок в базе, новых, проверенных и отмененных
	{
		if ($param[0]['json']==1) $param=$param[0];
		$q="
		SELECT  
			COUNT(IF ((vis =2 OR vis=4), vis, NULL )) AS new, 
			
			COUNT(IF ((vis =2 AND type_registration='reg-startup'), vis, NULL )) AS new_startup, 
			COUNT(IF ((vis =2 AND type_registration='reg-invest'), vis, NULL )) AS new_invest, 
			COUNT(IF ((vis =2 AND type_registration='reg-stock'), vis, NULL )) AS new_stock, 
					   
			COUNT(IF (vis =1, vis, NULL )) AS current, 
			COUNT(IF (vis =0, vis, NULL )) AS del, 
			COUNT(IF (vis =3, vis, NULL )) AS respond
		FROM client_profile"; 
		$row=$this->getRow($q);       
	
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}
	public function GetClientList($param=array())     // подсчёт заявок в базе, новых, проверенных и отмененных
	{
		$param['json']=(int)$param['json'];
		
		if (isset($param['vis'])) { 
			$param['vis']=(int)$param['vis'];
			if ($param['vis']==1) $where=" AND ('{$param['vis']}'=vis )"; //OR is_beneficiary=1 не понятно зачем тут оно?
			else $where=" AND '{$param['vis']}'=vis"; 
		}       
	
		if (isset($param['is_beneficiary'])) { $param['is_beneficiary']=(int) $param['is_beneficiary']; $where.=" AND '{$param['is_beneficiary']}'=is_beneficiary";  }        

		if (isset($param['is_beneficiary']) && isset($param['vis']))  {
			$param['is_beneficiary']=(int) $param['is_beneficiary'];
			$param['vis']=(int)$param['vis'];
			$where=" AND ('{$param['vis']}'=vis OR '{$param['is_beneficiary']}'=is_beneficiary)"; 
			
		}       
		if (isset($param['type_registration'])) {$param['type_registration']=$param['type_registration']; $where.=" AND '{$param['type_registration']}'=type_registration"; }
		$q="
		SELECT  id, vis, surname, name, email, date_create, is_verify_email, is_beneficiary
		FROM client_profile t1 WHERE id>0 $where";   
	   // print_r($q);      exit;
		$row=$this->getAll($q);    
		 //  print_r($row); exit;
		foreach ($row as $item=>$key)
		{
			$row[$item]['label']='<ul>';
			if ($row[$item]['surname']=='') $row[$item]['fio']=$row[$item]['email'];
			else $row[$item]['fio']=$row[$item]['surname']." ".$row[$item]['name']." ".$row[$item]['middle_name'];
			
			if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
			if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Утверждено';
			if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';
			
			if ($row[$item]['id_files_contract']==0) $row[$item]['contract']='-'; else $row[$item]['contract']='+';
			
			if ($row[$item]['is_verify_email']==1)   $row[$item]['label'].='<li>Email подтвержден</li>';  else     $row[$item]['label'].='<li>Email НЕ подтвержден</li>';       
			if ($row[$item]['is_beneficiary']==1)   $row[$item]['label'].='<li>Бенефициар</li>';
			$row[$item]['label'].='</ul>';
		}
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}
	public function GetClientInfo($param=array())     // информация о клиенте
	{
		if ($param[0]['json']==1) $param=$param[0];
	   
		if (isset($param['id'])){ $where=" AND ?=id"; $p[]=$param['id'];}
		if (isset($param['id_user'])){ $where.=" AND ?=id_user"; $p[]=$param['id_user'];}  //id_files_second_doc, id_files_labor_book, id_files_income
		$q="
			SELECT  t1.*, 
			(SELECT t2.title FROM files t2 WHERE id=t1.id_files_passport) as id_files_passport_title,
			(SELECT t2.title FROM files t2 WHERE id=t1.id_files_contract) as 	id_files_contract_title,
			(SELECT t2.title FROM files t2 WHERE id=t1.id_files_second_doc) as 	id_files_second_doc_title,
			(SELECT t2.title FROM files t2 WHERE id=t1.id_files_labor_book) as 	 id_files_labor_book_title,
			(SELECT t2.title FROM files t2 WHERE id=t1.id_files_income) as 	id_files_income_title
			FROM client_profile t1 WHERE id>0 $where"; 
		//echo $q; exit;
		$row=$this->getRow($q,$placeholder=$p);      
		
		if (count($row)>0)  {
			$res['stats']=$this->stats->GetUserEventList(array('id_record'=>$param['id'],'p'=>'client','limit'=>10));         
			//print_r()
			$this->stats->CreateUserEvent(array('event'=>'4','event_text'=>'Просмотр'));  // записываем в лог действие Умышлено записываем действие позже выгрузки чтобы не флудить
			
			if ($row['vis']=='2') $row['vis_text']='Новый профиль';
			if ($row['vis']=='1') $row['vis_text']='Активный профиль';
			if ($row['vis']=='0') $row['vis_text']='В архиве';   
			
			if ($row['type_registration']=='reg-stock') $row['type_registration_text']='Биржа';           
			if ($row['type_registration']=='reg-invest') $row['type_registration_text']='Долевое инвестирование';   
			if ($row['type_registration']=='reg-startup') $row['type_registration_text']='Стартап';   
			if ($row['type_registration']=='') $row['type_registration_text']='Нет информации'; 
			$res['row']=$row;
			$res['error']=0;
		}
		else $res=array('error'=>1,'error_msg'=>'');
									   
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	

	
	public function CreateClientOrder($p=array())     
	{
		/*
			// Создание заявки нового клиента
			Заявка со статусом vis=2 уже может входить на биржу и совершать действия
			Заявка тут же проходит и создается Демо счёт
		*/
		
		
		$p['json']=(int)$p['json'];        
		$post=$p['post'];
		$demo=isset($p['demo'])?$p['demo']:0; // нужно создать демо счет 1. Не нужно 0 (по умолчанию)
		//print_r($post); exit;
		if (isset($post['is_beneficiary']))$post['is_beneficiary']=1; else $post['is_beneficiary']=0;
		if (!isset($post['password']))$post['password']='12345';
		$post['password']=md5($post['password']);
		$post['mobile_phone']=preg_replace('/[^0-9]/', '', $post['mobile_phone']);
		
		if (isset($post['date_of_birth'])) $post['date_of_birth']=date('Y-m-d',strtotime($post['date_of_birth']));
		if (isset($post['date_of_issue']))$post['date_of_issue']=date('Y-m-d',strtotime($post['date_of_issue']));
		if (isset($post['registered_date']))$post['registered_date']=date('Y-m-d',strtotime($post['registered_date']));
		
		//print_r($p); 
		unset($post['file']);
		$id_user=$this->insertdata('client_profile',$post); 
		if ($id_user>0){
		//если клиент успешно создан и передано в demo=1
		if ($demo>0) $this->account->CreateAccount(array('id_user'=>$id_user,'deposit'=>'500000'));
		$this->email->Send(array('template'=>'email_welcome.html','email'=>$p['post']['email']));
		$res=array('error'=>0); 
		}
		else $res=array('error'=>1, 'error_msg'=>'Ошибка создания клиента'); 
	//exit;
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	
	public function ApproveClientOrder($param=array())
	{
		$param['json']=(int)$param['json'];       
		$id_user=(int)$param['id'];
		
		$arr=array('vis'=>'1');
		$this->update('client_profile',$arr," WHERE id=?", array($id_user));
		
		$res=$this->account->CreateAccount(array('id_user'=>$id_user,'type_account'=>2));   //  биржевой счет 
		$res=$this->account->CreateAccount(array('id_user'=>$id_user,'type_account'=>4)); 
 /*       
		$q="SELECT is_beneficiary FROM client_profile WHERE id='{$id_user}'";
	   // echo $q; exit;
		$is_beneficiary=(int)$this->getOne($q);
		if ($is_beneficiary>0) $res=$this->account->CreateAccount(array('id_user'=>$id_user,'type_account'=>4));   //  лицевой счет
 */       
		
		
		if ( $res['error']=='0') {
			$row['error']=0; 
				
				$this->stats->CreateUserEvent(array('event'=>'5','event_text'=>'Смена статуса')); 
		
			}
		else {
			$row['error']=1; 
			$row['error_msg']=$res['error_msg'];
			}
		
		if ($param['json']==1)  {            
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;        
	}
	public function CancelClientOrder($param=array())
	{
		if ($param[0]['json']==1) $param=$param[0];
		$id_user=isset($param['id'])?$param['id']:'';
		// проверяем является ли клиент беенфициаром действующим! со статусом больше 0
		$q="SELECT vis FROM project_beneficiary WHERE id_client='{$id_user}'";
		$is_beneficiary=(int)$this->db->getOne($q);
		
		if ($is_beneficiary==0){
			$arr=array('vis'=>'0');
			$this->update('client_profile',$arr," WHERE id=?", array($id_user));
			$res['error']=0;    
		} else {$res['error']=1;  $row['error_msg'].='Нельзя удалить действующего бенефициара';}
		
		if ( $res['error']=='0') {
			$row['error']=0;             
			$this->stats->CreateUserEvent(array('event'=>'1','event_text'=>'Удалено')); 
		
			}
		else {
			$row['error']=1; 
			$row['error_msg'].=$res['error_msg'];
			}
		
		if ($param['json']==1)  {
			
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;        
	}
	public function EditClientOrder($param=array('json','POST','id'))     // редактирование проектов
	{   //print_r($param); exit;
		
		
		if ($param[0]['json']==1) $param=$param[0];
		$post=$param['POST'];
		unset($post['file']);

		if (isset($post['date_of_birth']))    $post['date_of_birth']=date('Y-m-d',strtotime($post['date_of_birth']));
		if (isset($post['date_of_issue']))   $post['date_of_issue']=date('Y-m-d',strtotime($post['date_of_issue']));
		if (isset($post['registered_date']))$post['registered_date']=date('Y-m-d',strtotime($post['registered_date']));    
		if (isset($post['password']))    $post['password']=md5($post['password']);    

		//$post['is_beneficiary']=()$post['is_beneficiary']=0;
   //print_r($post); exit;     
		$this->update('client_profile',$post," WHERE id=?",array($param['id'])); // записываем саму запись
	
		$this->stats->CreateUserEvent( array( 'event'=>'3', 'event_text'=>'Исправление','post'=>$post)); // записываем в лог действие
		
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}
	
	public function ForgotPassword($param=array())
	{
		$param['json']=(int)$param['json'];

		if (!isset($_SESSION['forgot_password_try']))$_SESSION['forgot_password_try']=3;        
		if ($_SESSION['forgot_password_try']>0){      
			 
			$param['mobile']=preg_replace('/[^0-9]/', '', $param['mobile']);
			if (strlen($param['mobile'])>0){

				$q="SELECT id FROM client_profile WHERE mobile_phone=?";
				$row=$this->getOne($q, array($param['mobile']));
				//echo $row;
			
				if ($row==0){
				$res=array('error'=>1, 'error_msg'=>"Запись с таким телефоном не найдена. Попыток осталось:{$_SESSION['forgot_password_try']}");
				$_SESSION['forgot_password_try']--;
			}else{
					
				$res['msg']='SMS отправлено';
				$pass=$this->GeneratePassword();
				$this->EditClientOrder(array('id'=>$row, 'POST'=>array('password'=>md5($pass))));
				$this->enter->send_sms($param['mobile'],'Ваш новый пароль: \n'.$pass);
				}
			}
			else {
				$res=array('error'=>1, 'error_msg'=>"Запись с таким телефоном не найдена. Попыток осталось:{$_SESSION['forgot_password_try']}");
				$_SESSION['forgot_password_try']--;
	
			} 
		} else $res=array('error'=>1, 'error_msg'=>'Попытки закончились, профиль заблокирован');   
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}

	public function CheckEmail($p=array()) // проверка наличия email
	{
		$p['json']=(int)$p['json'];
		$p['email']=addslashes($p['email']);
		
		$q="SELECT id FROM client_profile WHERE email=?";
		$is_email=(int)$this->getOne($q,array($p['email']));
		if ($is_email==0){ $res=array('error'=>0);
		} else $res=array('error'=>1, 'error_msg'=>'Такой email уже существует');   
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}
	
	function CheckTel($p=array()) // проверка наличия email
	{
		$p['json']=(int)$p['json'];
		$p['mobile_phone']=addslashes($p['mobile_phone']);
		
		$q="SELECT id FROM client_profile WHERE mobile_phone=?";
		$is_tel=(int)$this->getOne($q,array($p['mobile_phone']));
		if ($is_tel==0){ $res=array('error'=>0);
		} else $res=array('error'=>1, 'error_msg'=>'Такой телефон уже существует');   
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}

	function CheckData($p=array()) // email, tel
	{
		$p['json']=(int)$p['json'];
		$res['error']=0;
		
		if (isset($p['email'])) $res=$this->CheckEmail(array('email'=>$p['email']));
		if ($res['error']==0) $res=$this->CheckTel(array('mobile_phone'=>$p['mobile_phone']));
		
		
		//if ()
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}     
	function GeneratePassword($number=8)
	{
		$arr = array(
			'a','b','c','d','e','f',
			'g','h','i','j','k','l',
			'm','n','o','p','r','s',
			't','u','v','x','y','z',
			'A','B','C','D','E','F',
			'G','H','I','J','K','L',
			'M','N','O','P','R','S',
			'T','U','V','X','Y','Z',
			'1','2','3','4','5','6',
			'7','8','9','0','.',',',
			'(',')','!','?',':',';',
			'&','@');
		// Генерируем пароль
		$pass = "";
		for($i = 0; $i < $number; $i++)
		{
		  // Вычисляем случайный индекс массива
		  $index = rand(0, count($arr) - 1);
		  $pass .= $arr[$index];
		}
		return $pass;
	}
	function Subscribe($p=array())
	{
		$p['json']=(int)$p['json'];
		$res['error']=0;
		$p['o_name']=htmlspecialchars($p['o_name']);
		$p['o_email']=htmlspecialchars($p['o_email']);
		$p['o_quest']=htmlspecialchars($p['o_quest']);
/*--Отправка формы с заявкой--*/


		$body.="<strong>Имя:</strong> ".($p['o_name'])." <br><br>
		<strong>Email:</strong> ".($p['o_email'])." <br> <br>
		<strong>Проблема или вопрос:</strong> ".($p['o_quest'])."  <br> <br>
		";
		$email = ' info@sayloans.ru';
		$subject = 'Как мы можем Вам помочь?';

		$mess="";
		$this->email = new PHPMailer(); // Класс для отправки почты с прописанными логинами и паролем для отправки
	
		$this->email->From = $email;	
		$this->email->AddAddress('birdvoron@yandex.ru'); 
		$this->email->FromName = "Администрация Сайбель Финанс"; // Отображется в отправителе
		$this->email->IsHTML(true);
		$this->email->Subject = $subject;
		$this->email->Body = $body;


		if (!$this->email->Send()){
			$this->email->ErrorInfo;
			$res['error']=1;
			
		}else{
			$res['error']=0;
			
		}
		//echo $this->email->Body;
		//Header("Location: /?p=contacts");
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res; 

	}
}
?>