<?
class Account extends database{    

	public $stats;
	
	public $label=array(
	'type_transfer'=>array(   
		array('id'=>0,'title'=>'---'),         
		array('id'=>1,'title'=>'Зачисление'),
		array('id'=>2,'title'=>'Списание'),
		array('id'=>3,'title'=>'Между счетами'),
		array('id'=>4,'title'=>'Покупка акций'),   
		array('id'=>5,'title'=>'Продажа акций'),
		array('id'=>6,'title'=>'Комиссия'),
		array('id'=>7,'title'=>'Компенсация')   
	),
	'method_transfer'=>array(  
		array('id'=>0,'title'=>'---'),   
		array('id'=>1,'title'=>'QIWI'),
		array('id'=>2,'title'=>'Реальный счет'),
		array('id'=>3,'title'=>'Наличка')   
	),
	'purpose'=>array(  
		array('id'=>0,'title'=>'---'),   
		array('id'=>1,'title'=>'Пополнение счета'),
		array('id'=>2,'title'=>'Выплата бонусов клиенту'),
		array('id'=>3,'title'=>'Оплата за привлечение'),
	   array('id'=>4,'title'=>'Вывод денежных средств')            
	),
);
	
	function __construct()
	{

		$this->stats=New Stats();
		parent::__construct();
		
	}
/*(    function __destruct() 
	{
	   // echo "Выполнено";
	}
	*/
	public function CreateNumber($param=array())
	{
		//$id_record=number,$type_bonds        
		$param['json']=(int)$param['json'];
		
		$number=(int)$param['number'];
		$type_account=(int)$param['type_account'];
		if(isset($param['id_project'])){ 
			$param['id_project']=(int)$param['id_project'];
			$where="id_project='{$param['id_project']}'";} 
		else { 
			$param['id_bond_name']=(int)$param['id_bond_name']; 
			$where="id='{$param['id_bond_name']}'"; 
		}
	   // echo $type_account."===";    
		$q="SELECT * FROM bond_name WHERE  $where";
		$prefix=$this->db->getRow($q);
	  
			if ($type_account==1) { $prefix['prefix_begin']='BOND'; $prefix['prefix_end']='DEMO'; }
			if ($type_account==2) { $prefix['prefix_begin']='BOND'; $prefix['prefix_end']=''; }
			if ($type_account==4) { $prefix['prefix_begin']='ACC'; $prefix['prefix_end']=''; }

		$zero='';
		$full_account='';
		for ($i=0;$i<(12-strlen($number));$i++) $zero.='0';
		$full_account = $prefix['prefix_begin'].$zero.$number.$prefix['prefix_end'];
				
		if ($param['json']==1)  {
			$result =  json_encode($full_account);
			echo "jsonpCallback(".$result.")";
		} else return  $full_account;   
	}
	public function CreateAccount($param=array()) // создание аккауанта
	{
		/*
			Входящие данные:
			id_user (int) - по умолчанию 0
			type_account (int) -  если не передали, значит создается демо счет
			deposti (float)  по умолчанию 0 - если >0 - пополняем после создания
			bond_name (int) - 0  или id бонда
			bond_count (int)
		*/
		//$id_user,$type_bonds,$id_manager=0,$deposit=0,$id_contract=0
		$param['json']=(int)$param['json'];
		//print_r($param);
		$id_user=(int)$param['id_user'];
		$type_account=isset($param['type_account'])?(int)$param['type_account']:1; 
		$id_manager=$_SESSION['site_user']; // iD менеджера 
		$deposit=(float)$param['deposit']; // пополняем счет сразу по созданию     
		$bond_name=(int)$param['bond_name']; // пополняем счет сразу по созданию          
		$bond_count=(int)$param['bond_count']; // пополняем счет сразу по созданию
		$param['type_bond']=$bond_name;

		   // print_r($param);        
		$q = "SELECT id FROM account WHERE id_user='{$id_user}' AND (type_account='{$type_account}')";
		$id_account = $this->getOne($q);	
			
		if ($id_account==0 || $type_account==3){
			$zero='';
	  		$full_account='';
	
			$inData = array(
	  			'id_user'=>$id_user,
	  			'id_manager'=>$id_manager,
	  			'type_account'=>$type_account,
				'value'=>'0',
				'bond_name'=>$bond_name,
				'bond_count'=>$bond_count,
	  			'vis'=>1);
	  			
	// нужно фиксить все приходы акций для бонд_коунт
	
	//print_r($inData);
	
			$id_record=$this->insertdata('account',$inData);
			if ($deposit>0) $this->IncrementAccountUser(array('type_transfer'=>1, 'value'=>$deposit,'id_account'=>$id_record,'type_events'=>4)); // добавляем стартовый капитал

			$inData = array(
				'full_account'=>$this->CreateNumber(array(
					'number'=>$id_record,
					'id_bond_name'=>$bond_name,
					'type_account'=>$type_account
					)
				)
			);
			$this->update('account',$inData,	$where = " WHERE `id`= ?", array($id_record));
			$row['error']=0;
			$row['id_account']=$id_record;  
		}
		else{ 
			$row['error']=1;
			$row['error_msg']="Счет уже создан";
		}
		
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;   
	}
	public function  CancelAccount($param=array()) // установить активный аккаунт
	{   //print_r($param); exit;
		$param['json']=(int)$param['json'];

		
		$this->update('account',array('vis'=>0)," WHERE id=?", array($param['id']));    
		$this->stats->CreateUserEvent(array('event'=>'1','event_text'=>'Отменена')); 
	
	
	
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	public function  CancelTransaction($p=array()) // установить активный аккаунт
	{   //print_r($param); exit;
		$p['json']=(int)$p['json'];

		
		$this->update('account_transaction',array('vis'=>0), "WHERE id='{$p['id']}'");   
		$this->stats->CreateUserEvent(array('event'=>'1','event_text'=>'Отменена')); 
	
	
	
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	public function  ApproveAccount($param=array()) // установить активный аккаунт
	{   //print_r($param); exit;
		$param['json']=(int)$param['json'];

		
	   // $this->update('account',array('vis'=>1)," WHERE id=?", array($param['id']));    
	   // $this->stats->CreateUserEvent(array('event'=>'5','event_text'=>'Смена статуса')); 
	
	
	
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}

	function IncrementAccountUser($p=array()) //$amount,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
	{
		// 1 - списание на покупку сертов - 2 -списание на вывод 3 - пополнение с продажи 4 - пополнение счета         $id_account=isset($p[0][''])?$p[0]['']:0;
		$p['json']=(int)$p['json'];
		//print_r($p);
		$id_account=(int)$p['id_account'];
		$value=(float)$p['value'];
		$type_events=isset($p['type_events'])?$p['type_events']:3;
		$p['id_manager']=$_SESSION['site_user']; // iD менеджера     
		$trans_number=(int)$p['trans_number']; 
		$p['trans_descr']=$p['trans_descr'].''; 
		$trans_type=(int)$p['trans_type']; 
		$account_user=(int)$p['account_user']; 
		$number_transaction=(int)$p['number_transaction']; 
		
		// type_account
		$type_account=(int)$p['type_account'];

/*        
	if ( $type_account==4 && 1==2) {
		
		$q="UPDATE account SET  bond_count=bond_count+$value WHERE id='".(int)$id_account."'";  
		$this->db->query($q);
		
		$q="SELECT bond_count FROM account WHERE id='".(int)$id_account."'";
		$p['account_value']= $this->db->getOne($q);
	} else
	{          } 
*/            
		$q="UPDATE account SET  value=value+$value WHERE id={$id_account}";  
		$this->db->query($q);
		
		$q="SELECT value FROM account WHERE id={$id_account}";
		$p['account_value']= $this->db->getOne($q);

		$this->AccountsHistory($p);
		
		$res=array('error'=>0);
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}
	
	public function DecrementAccountUser($p=array()) // 1 - списание на покупку сертов - 2 -списание на вывод 3 - пополнение с продажи 4 - пополнение счета
	{
	   // print_r($param); exit;
	   // $amount, $id_account,$type_events=1, $id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
		$p['json']=(int)$p['json'];

		$id_account=(int)$p['id_account'];
		$value=(float)$p['value'];
		$type_events=isset($p['type_events'])?$p['type_events']:1;
		$p['id_manager']=$_SESSION['site_user']; // iD менеджера     
		$trans_number=(int)$p['trans_number']; 
		$p['trans_descr']=$p['trans_descr'].''; 
		$trans_type=(int)$p['trans_type']; 
		$account_user=(int)$p['account_user']; 
		$number_transaction=(int)$p['number_transaction'];     

		$q="UPDATE account SET value=value-{$value} WHERE id='{$id_account}'";
		$this->query($q);

		$q="SELECT value FROM account WHERE id='{$id_account}'";
		$p['account_value']= $this->db->getOne($q);


		$this->AccountsHistory($p);
		//$this->AccountsHistory($id_account, $id_manager, $type_events,$amount, $trans_number, $trans_type, $trans_descr,$account_value,$account_user,$number_transaction);

		$res=array('error'=>0);
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}
	public function AccountsHistory($p=array()) // история платежей
	{   //$id_account=0, $id_manager=0, $type_events=0,$amount=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_value=0,$account_user=0,$number_transaction=0
	   $p['json']=(int)$p['json'];
		$temp=$p;
		unset($temp['json']); // print_r($temp);
		$row=$this->insertdata('account_history',$temp);

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;          
	}
	public function GetAccountHistory($p=array())
	{   
		$p['json']=(int)$p['json'];
		$p['id_account']=(int)$p['id_account'];     
		   
		if (isset($p['id_user'])) {
			$p['id_user']=(int)$p['id_user']; 
			$where=" AND t2.id_user='{$p['id_user']}'"; 
		}
		
		$q = "SELECT t1.*,t2.full_account FROM account_history t1, account t2 WHERE t1.id_account='{$p['id_account']}' AND t1.id_account=t2.id {$where}  ORDER BY t1.date_create DESC";
		$row=$this->db->getAll($q);
		
		foreach($row as $item=>$key)
		{
			if ($row[$item]['type_events']==1)   $row[$item]['vis_text']='Пополнение';
			if ($row[$item]['type_events']==2)   $row[$item]['vis_text']='Пополнение демо-счета';
			if ($row[$item]['type_events']==3)   $row[$item]['vis_text']='Приход';
			if ($row[$item]['type_events']==4)   $row[$item]['vis_text']='Расход';
		}
		
		$res=array('error'=>0,'row'=>$row);
	   
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;      
	}
	 
	public function GetAccountList($param=array()) // получаем список аккаунтов
	{
		$param['json']=(int)$param['json'];
		
		if (isset($param['id_user'])) {$param['id_user']=(int)$param['id_user']; $where=" AND t1.id_user='{$param['id_user']}'"; }
		if (isset($param['session_user'])) { $where.=" AND t1.id_user={$_SESSION['site_user']}"; } // получить все счета авторизованного пользователя 
		if (isset($param['id_account'])) { $where_=" AND t1.id='".(int)$param['id_account']."'"; }
	
		$q = "
			SELECT 
				t1.*, 
				IF (t1.type_account=3, 
					((SELECT par_now FROM bond_name t3 WHERE t3.id=t1.bond_name LIMIT 1)*
					(SELECT capital_in_stock FROM project_beneficiary t4 WHERE t4.id_client=t1.id_user LIMIT 1))
					, t1.value
				) as value, 
				CONCAT(t2.surname,' ',t2.name) as fio
			FROM 
				account t1
				LEFT JOIN client_profile t2 ON  t1.id_user=t2.id AND t2.vis>0
			WHERE 
			t1.id>0
				$where 
			ORDER by t2.id, t1.id";    
		  
		$row=$this->db->getAll($q);
		//echo $q; exit;
		foreach ($row as $item=>$key)
		{
			if ($row[$item]['value']>0)  {} else $row[$item]['value']=0;
			if ($row[$item]['type_account']==1) $row[$item]['title']='Демо счет';
			if ($row[$item]['type_account']==2) $row[$item]['title']='Биржевой счет';
			if ($row[$item]['type_account']==3) $row[$item]['title']='Бенефициарный счет';
			if ($row[$item]['type_account']==4) $row[$item]['title']='Лицевой счет';

			if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
			if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Активный';
			if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Заблокирован';
		}
		$res['error']=0;
		$res['row']=$row;
//         /$res['msg']=$q;
		//print_r($row);// exit;
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return $res;
	}
	public function GetAccountInfo($p=array()) // информация о счете
	{
		/* 
			входяшие 
			$id_user,
			$id = id записи аккауниа
		
		*/
		
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];  
		if (isset($p['id_user'])) { $p['id_user']=(int)$p['id_user']; $where=" AND t1.id_user='{$p['id_user']}'"; }      
		$q = "
			SELECT 
				t1.*,	CONCAT(t2.surname,' ',t2.name) as fio
			FROM 
				account t1 LEFT JOIN client_profile t2 ON t1.id_user=t2.id
			WHERE  t1.id='{$p['id']}' $where
			ORDER by t2.id, t1.id";  
			
		//$q = "SELECT * FROM account WHERE id='{$param['id']}' LIMIT 1";
		$row= $this->getRow($q);
		
		if ($row['type_account']==3){
			$q="SELECT par_now*capital_in_stock as value, capital_in_stock FROM bond_name t2, project_beneficiary t1 WHERE t2.id='{$row['bond_name']}' AND t1.id_client='{$row['id_user']}'";
		//	print_r( $row); exit;
			$row1=$this->getRow($q);
			$row['value']=$row1['value'];
			$row['count']=$row1['capital_in_stock'];
		}
		$res=array('error'=>0, 'row'=>$row);
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return $res;
	}
	public function SetAccountActive($param=array()) // установить активный аккаунт
	{
		if ($param[0]['json']==1) $param=$param[0];
		$account=isset($param['account'])?$param['account']:0;
		//print_r($param);
		// делаем реверс счета если реальный счет существует
		// если параметр не передали для счета, то значит их нужно поменять местами
		if ( $_SESSION['site_user_account']>0){
			if ( $_SESSION['site_user_account_active']== $_SESSION['site_user_account_demo'])  $_SESSION['site_user_account_active']= $_SESSION['site_user_account']; 
			else $_SESSION['site_user_account_active']= $_SESSION['site_user_account_demo'];
		}
	  
		if ($account>0) $_SESSION['site_user_account_active']=$account;
		setcookie("{$_SESSION['site_user']}site_user_account_active",$_SESSION['site_user_account_active'],time()+31556926 ,'/');          
		if ($param['json']==1)  {
			$result =  json_encode(array((error)=>0));
			echo "jsonpCallback(".$result.")";
		} else return $account;
	}
	
	function CreateTransaction( $p=array()) // POST / Создание записи
	{
		//ALTER TABLE `account_transaction` ADD `id_user_to` INT(11) NOT NULL COMMENT 'ID получателя' AFTER `id_account`;
		//$id_record=number,$type_bonds        
		$p['json']=(int)$p['json'];
	   //print_r($p['POST']); exit;
	   
		// если передан vis - то запись можно создавать уже одобренную
		if (isset($p['vis'])) $p['POST']['vis']=(int)$p['vis'];
		$id=$this->insertdata('account_transaction',$p['POST']);
		
		if ($id>0) $res=array('error'=>0, 'id'=>$id);
		else $res=array('error'=>1, 'error_msg'=>'Ошибка создания');
		
		if ( $p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}    

	
	function EditTransaction($p=array()) // POST, id записи / редактирование транзакции
	{
		//$id_record=number,$type_bonds        
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		
		$this->update('account_transaction',$p['POST'], "WHERE id='{$p['id']}'");
		
		if ( $p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}      

	
	function ApproveTransaction($p=array()) //  id записи, POST / подтвержздение
	{
		/* 
			type_transfer>
				1 - incoming - пополнение, 
				2 - outgoing вывод средств, 
				3 - between между счетами 
				4- commission
		*/
	   
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$POST=$p['POST'];
 
		if ($POST['type_transfer']=='1') {

			// пополнение счета $  //$value,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
			$this->IncrementAccountUser(array(
				(type_transfer)=>1,
				(value)=>$POST['value'],
				(id_account)=>$POST['id_account'],
				(type_events)=>3,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>$p['id'],
				(trans_descr)=>$POST['reference']
			));
		}

		
		if ($POST['type_transfer']=='2') {
			// пополнение счета $  //$amount,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
			$this->DecrementAccountUser(array(
				(type_transfer)=>2,
				(value)=>$POST['value'],
				(id_account)=>$POST['id_account'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>$p['id'],
				(trans_descr)=>$POST['reference']
			));
		}

		
		if ($POST['type_transfer']=='3') { // between
			// пополнение счета $  //$amount,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
			$this->DecrementAccountUser(array(
				(type_transfer)=>2,
				(value)=>$POST['value'],
				(id_account)=>$POST['id_account'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>$p['id'],
				(trans_descr)=>$POST['reference']
			));
			
			$this->IncrementAccountUser(array(
				(type_transfer)=>1,
				(value)=>$POST['value'],
				(id_account)=>$POST['id_account_to'],
				(type_events)=>3,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>$p['id'],
				(trans_descr)=>$POST['reference']
			));
		} 
		
		if ($POST['type_transfer']=='4') { // комиссия
			// пополнение счета $  //$amount,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0
			$this->DecrementAccountUser(array(
				(type_transfer)=>2,
				(value)=>$POST['value'],
				(id_account)=>$POST['id_account'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>$p['id'],
				(trans_descr)=>$POST['reference']
			));
		}      
			 
		$this->update('account_transaction',array('vis'=>1), "WHERE id='{$p['id']}'");
		
		if ( $p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}     
	
	function GetTransactionList($p=array()) // vis, purpose, type_transfer, method_trasnfer / список транзакций
	{
		
		//$id_record=number,$type_bonds        
		$p['json']=(int)$p['json'];
		$p['vis']=(int)$p['vis'];
		
		if (isset($p['id_account'])) { $where=" AND t1.id_account='".(int)$p['id_account']."'"; }
		if (isset($p['id_account_to'])) { $where.=" AND t1.id='".(int)$p['id_account']."'"; }
		if (isset($p['id_account']) && isset($p['id_account_to'])) { $where=" AND (t1.id_account='".(int)$p['id_account']."' OR t1.id_account_to='".(int)$p['id_account_to']."')"; }        
		$q="SELECT * FROM account_transaction t1 WHERE t1.id>0 $where";
		
		if (isset($p['id_user'])){
			$p['id_user']=(int)$p['id_user'];
			$where=" AND t2.id_user='{$p['id_user']}'";
			$q="SELECT t1.*, t2.full_account FROM account_transaction t1, account t2 WHERE (t2.id=t1.id_account OR t2.id=t1.id_account_to)  $where GROUP BY t1.id";
		}
		
		$row=$this->getAll($q);
		//print_r($row); exit;
		foreach($row as $item=>$key)
		{
			if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новое';
			if ($row[$item]['vis']=='1')  $row[$item]['vis_text']='Подтверждено';
			if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Отменено';
			
			if ($row[$item]['type_transfer']>0){
				$id=array_search($row[$item]['type_transfer'], array_column($this->label['type_transfer'], 'id'));
				//$row[$item]['type_transfer']=$row[$item]['type_transfer'];
				$row[$item]['type_transfer_title']=$this->label['type_transfer'][$id]['title'];
				 
			}
			else $row[$item]['type_transfer']=0;
			
			
			if ($row[$item]['purpose']>0){
				$id=array_search($row[$item]['purpose'], array_column($this->label['purpose'], 'id'));
				 $row[$item]['purpose']=$this->label['purpose'][$id]['title'];
			}
			else $row[$item]['purpose']='...';

			
			if ($row[$item]['method_transfer']>0){
				$id=array_search($row[$item]['method_transfer'], array_column($this->label['method_transfer'], 'id'));
				$row[$item]['method_transfer']=$this->label['method_transfer'][$id]['title'];
			}
			else $row[$item]['method_transfer']='...';
						
			
		}
		$res=array('error'=>0,'row'=>$row);
		
		if ( $p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}    
	
	function GetTransactionInfo($p=array()) // vis, purpose, type_transfer, method_trasnfer / список транзакций
	{
		//$id_record=number,$type_bonds        
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		
		$q="
		SELECT 
			t1.*, t2.full_account, t2.type_account , t3.full_account as full_account_to, t3.type_account as type_account_to 
		FROM 
			account_transaction t1 
			LEFT JOIN account t2 ON t1.id_account=t2.id 
			LEFT JOIN account t3 ON t1.id_account_to=t3.id 
		WHERE t1.id='{$p['id']}' ";
		
		//echo $q; exit;
		$row=$this->getRow($q);
	   // print_r($row); exit;
		if (count($row)>0){

				if ($row['type_account']==1) $row['title_account']='Демо счет';
				if ($row['type_account']==2) $row['title_account']='Биржевой счет';
				if ($row['type_account']==3) $row['title_account']='Бенефициарный счет';
				if ($row['type_account']==4) $row['title_account']='Лицевой счет';
  
		}
		$res=array('error'=>0,'row'=>$row);
		//print_r($res);
		
		if ( $p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
		
		//---в
	}    
		
}
?>