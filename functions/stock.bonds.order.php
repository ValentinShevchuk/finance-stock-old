<?
class BondsOrder extends database { 
   public $stats;
   public $bond;
   public $account;  
  // public $beneficiary;
	
	function __construct()
	{
		$this->stats=New Stats();
		$this->bond=New Bond();
		$this->account=New Account();
		$this->beneficiary=New Project(); // функции для бенефициара
		
		parent::__construct();
		
	}    
	public function GetBondOrderNumbers($param=array())     // подсчёт заявок в базе, новых, проверенных и отмененных
	{
		if ($param[0]['json']==1) $param=$param[0];
		$q="
		SELECT  
			COUNT(IF (vis =2, vis, NULL )) AS new, 
			COUNT(IF (vis =1, vis, NULL )) AS current, 
			COUNT(IF (vis =0, vis, NULL )) AS del
		FROM project_bond_order"; 
		$row=$this->getRow($q);       
	
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}  
 
	public function GetBondOrderList($p=array())
	{
		$p['json']=(int)$p['json'];
		
		if (isset($p['vis'])) 
		{ 
			$p['vis']=(int)$p['vis'];  
			$where=" AND t1.vis='{$p['vis']}'";
		}
		if (isset($p['id_user'])) { 
			$p['id_user']=(int)$p['id_user'];
			$where.=" AND t1.id_user='{$p['id_user']}'";  
		}
		
		$q="SELECT t1.*, t2.title as project_title FROM project_bond_order t1, project t2 WHERE t1.id>0 AND t1.id_project=t2.id $where";
		$row=$this->getAll($q);
		
		foreach($row as $item=>$key){
			if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
			if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Утверждено';
			if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';
		}
		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;   
		
	}  
	public function GetBondOrderInfo($param=array())
	{
		if ($param[0]['json']==1) $param=$param[0];
		if (isset($param['id'])){  
				
			$q="SELECT t1.* FROM project_bond_order t1 WHERE t1.id=?";
			$row=$this->getRow($q,array($param['id']));
			$this->stats->CreateUserEvent(array('event'=>'4','event_text'=>'Просмотр')); 
			
			$res['row']=$row;
			$res['error']=0;         
				 
		}else $res=array('error'=>1, 'error_msg'=>'');
		
		if ($param[0]['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}
	public function CreateBondOrder($param=array())
	{
	   // print_r($param);// exit;
		if ($param[0]['json']==1) $param=$param[0];
		
		$arr=$param;
		$arr['id_manager']=$_SESSION['site_user'];
		$id=$this->insertdata('project_bond_order',$arr);    
		$this->stats->CreateUserEvent(array( 'event'=>'2', 'event_text'=>'Создан', 'id_record'=>$id));   
			  
		if ($param[0]['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;   
	} 
	
	public function EditBondOrder($param=array()) // редактирование завки выпуск акций на бирже
	{  //print_r($param); exit;
		if ($param[0]['json']==1) $param=$param[0];
		$arr=$param['POST'];
		$this->update('project_bond_order',$arr," WHERE id=?", array($param['id']));
		
		$this->stats->CreateUserEvent(array( 'event'=>'3','event_text'=>'Исправлен','post'=>$param['POST'] )); // записываем в лог действие
			
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}    
	public function ApproveBondOrder($param=array()) // подтверждение создания акций на бирже
	{  
		$param['json']=(int)$param['json'];
		
		$param['id']=(int)$param['id'];
		
		$capital_in_stock=(int)$param['POST']['capital_in_stock'];

		$this->update('project_bond_order',array('vis'=>1)," WHERE id=?",array($param['id']));       
		 
		$this->stats->CreateUserEvent(array( 'event'=>'5','event_text'=>'Смена статуса')); // записываем в лог действие

		$q="
			SELECT 
				id_project, 
				bonds_title as title, 
				bonds_descr as descr, 
				short_name, 
				prefix_begin, 
				prefix_end, 
				short_name, 
				is_dividend, 
				dividend_percent as percent, 
				dividend_payday as payday, 
				value_of_stock as par,
				capital_in_stock as count
			FROM project_bond_order 
			WHERE id='{$param['id']}'";
		$row=$this->getRow($q);
		
		$row['id_manager']=$_SESSION['site_user'];        
		$row['par_now']=$row['par'];
		$result=$this->bond->CreateBondName(array('post'=>$row));  
	   
		//функция создания номеров
	
		if ($result['error']==0) $this->bond->CreateBond(array('count'=>$capital_in_stock,'id_bond_name'=>$result['new_id']));
		else $row=$result;
   
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}   
	function CancelBondOrder($p=array()) // отмена заявок выпуска акций на биржу (создание акций в системе)
	{
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		
	
		$arr=array('vis'=>0);
		$this->update('project_bond_order',$arr," WHERE id='{$p['id']}'");

		$arr=array('event'=>1,'event_text'=>'Отменен');
		$this->stats->CreateUserEvent($arr); // записываем в лог действие
	   
		
		
		if ($p['json']==1)  {            
			$result =  json_encode($res);            
			echo "jsonpCallback(".$result.")";            
		} else return  $res;            
	}      
	public function CreateBeneficiaryBondOrder($param=array())
	{
		$param['json']=(int)$param['json'];
		$arr=$param;
	   unset($arr['json']);
		$res['error']=0;
		if ($_SESSION['access']>0){
			$arr['id_manager']=$_SESSION['site_user'];
		}  
		$arr['id_open_position']=(int)$arr['id_open_position'];

		if ($arr['id_open_position']>0){
			$q="SELECT bonds_count FROM bond_position_open WHERE id='{$arr['id_open_position']}'";
			$row=$this->getOne($q);

			if ($row-$arr['to_market_in_amount']>=0){
				// если передана открытая позиция - фиксируем замороженное кол-во акций
				$q="UPDATE bond_position_open SET frozen={$arr['to_market_in_amount']}, bonds_count=bonds_count-{$arr['to_market_in_amount']}  WHERE id='{$arr['id_open_position']}'";
				$this->query($q);
				//$this->update('bond_position_open', $array, " WHERE "); 

			}else {
				$res=array('error'=>1, 'error_msg'=>'Ошибка создания заявки. Недостаточно акций');
			}
		}
		
		if ($res['error']==0) {
			if (isset($arr['date_start'])) $arr['date_start']=date('Y-m-d',strtotime($arr['date_start']));
			// print_r($arr); echo "==="; exit;
			$id=$this->insertdata('project_beneficiary_bond_order',$arr); 
			if ($id>0)  { 
				
				$this->stats->CreateUserEvent(array( 'event'=>'2', 'event_text'=>'Создан', 'id_record'=>$id, 'p'=>'bond_order&beneficiary'));            
				$res=array('error'=>0);
				
			}
			else $res=array('error'=>1,'error_msg'=>'Ошибка создания заявки');
		}
		if ($param[0]['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;           
	}    
	public function GetBeneficiaryBondOrderList($p=array())
	{
		$p['json']=(int)$p['json'];
		
		$p['is_registration']=(int)$p['is_registration'];
		
		if (isset($p['vis'])) { $p['vis']=(int)$p['vis']; $where=" AND t1.vis={$p['vis']}"; }
		
		if (isset($p['id_user'])) { 
			
			$p['id_user']=(int)$p['id_user'];
			
			$where.=" AND t4.id='{$p['id_user']}'";  
			
		}
		$q="
			SELECT 
				t1.*, t2.title, t4.surname, t4.name 
			FROM 
				project_beneficiary_bond_order t1, 
				project t2, 
				project_beneficiary t3,
				client_profile t4 
			WHERE  
				t4.id=t3.id_client AND t3.id=t1.id_beneficiary AND t3.vis=1 AND 
				t2.id=t1.id_project AND t1.id>0 AND 
				t1.is_registration='{$p['is_registration']}'  $where";
		//echo $q; exit;
		$row=$this->getAll($q);
		
		if (count($row)>0) {
			
			foreach($row as $item=>$key){
				if ($row[$item]['vis']=='3')  $row[$item]['vis_text']='Проведено';
				if ($row[$item]['vis']=='2')  $row[$item]['vis_text']='Новый';
				
				if ($row[$item]['vis']=='1')  $row[$item]['vis_text']='Утверждено';
				
				if ($row[$item]['vis']=='0')  $row[$item]['vis_text']='Удалено';
				
			}
			
			$res['row']=$row;
			
			$res['error']=0;
			
		}else{
			
			$res=array('error'=>1, 'error_msg'=>'Записей нет');
			
		}
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;        
	 }
	public function GetBeneficiaryBondOrderInfo($param=array())
	{
		if ($param[0]['json']==1) $param=$param[0];
		if (isset($param['id'])){              //   t3.name as beneficiary_name,   t4.par_now as value_of_stock,
			$q="
		   SELECT 
				t1.*, 
				t2.title as project_title, 
				
				t3.capital_in_percent,
				t3.capital_in_stock,
			   
				t4.title as bond_title,
				IF (t3.type=1,(SELECT CONCAT(surname,' ',name) FROM client_profile t5 WHERE t5.id=t3.id_client),(SELECT title FROM project t6 WHERE t6.id=t3.id_company ) )as beneficiary_name
				,t5.*, t1.vis as vis
			FROM 
				project_beneficiary_bond_order t1 LEFT JOIN bond_position_open t5 ON t5.id=t1.id_open_position, 
				project t2, 
				project_beneficiary t3, 
				bond_name t4
				
			WHERE t2.id=t1.id_project AND t3.id=t1.id_beneficiary AND t4.id=t1.id_bond_name AND t1.id=?";
			$row=$this->getRow($q,array($param['id']));
			//echo $q; exit;
			if (PEAR::isError($row)) {
					//die($row->getMessage()); exit;
			//die($row->getMessage());
					$res=array('error'=>1,'error_msg'=>'Ошибка создания заявки');
			} else {
				$this->stats->CreateUserEvent(array('event'=>'4','event_text'=>'Просмотр', 'p'=>'bond_order&beneficiary')); 
				$res['error']=0;                      
				$res['row']=$row;    
			}
		}else $res=array('error'=>1, 'error_msg'=>'');
		
		if ($param[0]['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;   
	}    

	public function EditBeneficiaryBondOrder($param=array()) // ред. заявки от беенфициара
	{  //print_r($param); exit;
		$p['json']=(int)$p['json'];
		
		// убираем поле так как не нужно и не должно изменяться
		unset($param['POST']['is_registration']); 
	
		$arr=$param['POST'];
		if (isset($arr['date_start'])) $arr['date_start']=date('Y-m-d',strtotime($arr['date_start']));
		$this->update('project_beneficiary_bond_order',$arr," WHERE id=?", array($param['id']));
		
		$this->stats->CreateUserEvent(array( 'event'=>'3','event_text'=>'Исправлен','p'=>'bond_order&beneficiary','post'=>$param['POST'] )); // записываем в лог действие
			
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}
	public function ApproveBeneficiaryBondOrder($param=array())
	{  
		// это еще не выпуск акций - акции выпускаются по срокам!!! Функция в cron
		$log=array(); 
		$param['json']=(int)$param['json'];
		
		$param['id']=(int)$param['id'];
		
		$param['is_registration']=(int)$param['is_registration'];    
		 
		$log[]=array('step'=>1, 'message'=>'Пришли данные', 'data'=>json_encode($param, JSON_UNESCAPED_UNICODE));    
		
		$log[]=array('step'=>2, 'message'=>'Обновили: project_beneficiary_bond_order '); 
		
		$this->update('project_beneficiary_bond_order',array('vis'=>1)," WHERE id='{$param['id']}'");  
		
		// Получаем инфу номинальной цены и кол-ве оформляемых товаров
		/*
			- если нет цены на покупку акции - тогда берется цена номинала при регистрации. Случай только при начального распределения
		*/
		$q="
			SELECT 
				t1.*, t2.to_market_in_amount as order_count, t3.id as id_account, 
				(SELECT COUNT(id) FROM bond t6 WHERE t6.id_project_beneficiary= t2.id_beneficiary) as beneficiary_count,
				t2.id_beneficiary as id_beneficiary,
				t5.id as type_account4, t2.value_of_stock
			FROM 
				bond_name t1, 
				project_beneficiary_bond_order t2,
				account t3
					LEFT JOIN account t5 ON t5.type_account=3
				 
			WHERE 
				t2.id='{$param['id']}' AND t2.id_bond_name=t1.id AND
				t3.type_account=4
			LIMIT 1";   
		  //  echo "$q"; exit;
		$bond_info=$this->getRow($q);
		
		$log[]=array('step'=>3, 'message'=>'Выгрузили: bond_name ', 'data'=>json_encode($bond_info, JSON_UNESCAPED_UNICODE));      
		  

	   
		if ($param['is_registration']==1) {
			// сброс заморозки
			$q="UPDATE bond_position_open t1, project_beneficiary_bond_order t2 SET  t1.frozen=0  WHERE t1.id=t2.id_open_position AND t2.id='{$param['id']}'";
			$this->query($q);

			// увеличиваем кол-во акций у бенефициара
			$this->beneficiary->EditProjectBeneficiary(array('id'=>$bond_info['id_beneficiary'], 'POST'=>array('capital_in_stock'=>$bond_info['beneficiary_count']+$bond_info['order_count'])));
			// если цена изменилась то цена продажи идет с процентами, если это начало распределений то цена продажи будет стоять 0, значит продавать будем по номиналу
			if ($bond_info['par_now_sell']>0) $bond_info['par']=$bond_info['par_now_sell'];
		
			// сумма к списыванию было - $bond_info['par'];
			$total=$bond_info['order_count']*$bond_info['value_of_stock'];
			
			$log[]=array('step'=>4, 'message'=>"Сумма к списыванию:  $total ");
			
			// списываем со счет у клиента деньги кол-во акций по номинальной цене Внимание при условии что есть 1 в is_registration. Списываем с лицевого счета
			$array=array(
				(value)=>$total,
				(id_account)=>$bond_info['id_account'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Оформление акций в собственность'
			);
			
			$this->account->DecrementAccountUser($array);
			
			$log[]=array('step'=>5, 'message'=>"Списали сумму ",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));


			//  зачисляем на бенефициарный счет
			$array=array(
				(value)=>$total,
				(id_account)=>$bond_info['type_account4'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Оформление акций в собственность',
				(type_account)=>4
			);
			
			$this->account->IncrementAccountUser($array);            
			$log[]=array('step'=>5, 'message'=>"Заислена сумма ",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));

			
 /*  */             
			// зачисляем деньги на биржу
			$array=array(
				(value)=>$total,
				(id_account)=>1,
				(type_events)=>3,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Оформление акций в собственность бенефициаром (продажа акций)'
			);
			$this->account->IncrementAccountUser($array);
			
			$log[]=array('step'=>6, 'message'=>"Зачисляем сумму на счет биржи ",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));   
	 
		}
		 if ($param['is_registration']==0){
			if ($bond_info['par_now_buy']>0) $bond_info['par']=$bond_info['par_now_buy'];
		  
			$this->beneficiary->EditProjectBeneficiary(array('id'=>$bond_info['id_beneficiary'], 'POST'=>array('capital_in_stock'=>$bond_info['beneficiary_count']-$bond_info['order_count'])));
			
			// сумма к списыванию
			$total=$bond_info['order_count']*$bond_info['par'];
			
			$log[]=array('step'=>4, 'message'=>"Сумма к заичлсению:  $total ");
			
			 $array=array(
				(value)=>$total,
				(id_account)=>$bond_info['id_account'],
				(type_events)=>3,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Продажа собственных акций бирже'
			);
			
			$this->account->IncrementAccountUser($array);
			
			$log[]=array('step'=>5, 'message'=>"Списали списали ",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));
			
			
			// списываем деньги с биржы
			$array=array(
				(value)=>$total,
				(id_account)=>1,
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Покупка акций клиента'
			);
			$this->account->DecrementAccountUser($array);
			
			$log[]=array('step'=>6, 'message'=>"Зачисляем сумму на счет биржи ",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));        


/*
			// списыввкм с бенефициарного счета
			$array=array(
				(value)=>$total,
				(id_account)=>$bond_info['type_account4'],
				(type_events)=>1,
				(id_manager)=>$_SESSION['site_user'],
				(number_transaction)=>0,
				(trans_descr)=>'Оформление акций в собственность',
				(type_account)=>4
			);
			
			$this->account->IncrementAccountUser($array);            
			$log[]=array('step'=>5, 'message'=>"Списываем сумму",  'data'=>json_encode($array, JSON_UNESCAPED_UNICODE));
 */                       
		}
		//echo "<pre>"; print_r($log); exit;
		$this->stats->CreateUserEvent(array( 'event'=>'5','event_text'=>'Смена статуса','p'=>'bond_order&beneficiary')); // записываем в лог действие

		//   if ($result['error']==0) SetBeneficiaryBondToMarket// $this->bond->SetBeneficiaryBond(array('bond_name'=>, 'id_beneficiary'=>, 'count_bond'=>)); 
		// else $row=$result;
		//$this->bond->CreateBond(array('count'=>$capital_in_stock,'id_bond_name'=>$result['new_id']));

		if ($param['json']==1)  {
			
			$result =  json_encode($row);
			
			echo "jsonpCallback(".$result.")";
			
		} else return  $row;    
	}    
	
	function CancelBeneficiaryBondOrder($p=array())
	{
		$p['json']=(int)$p['json'];

		$p['id']=(int)$p['id'];
		$arr=$p['POST'];
		$arr['vis']=0;

		$this->update('project_beneficiary_bond_order',$arr," WHERE id='{$p['id']}'");

		$q="UPDATE bond_position_open t1, project_beneficiary_bond_order t2 SET  t1.bonds_count=t1.bonds_count+t1.frozen,t1.frozen=0  WHERE t1.id=t2.id_open_position AND t2.id='{$p['id']}'";
		$this->query($q);


		$arr=array('event'=>1,'event_text'=>'Отменен');
		$this->stats->CreateUserEvent($arr); // записываем в лог действие
	   
		
		
		if ($p['json']==1)  {            
			$result =  json_encode($res);            
			echo "jsonpCallback(".$result.")";            
		} else return  $res;            
	}
}
?>