<?php
require_once("../include/GeoIP.php");

class CountryCity extends database {
	public $geoip;
	public $client_ip;
	public $location;
	
	public function __construct($custom_ip=''){
		parent::__construct();		
		//city identification	城市鉴别通过ip
		//  '83.149.8.159'
		if ($_SERVER['REMOTE_ADDR']=='127.0.0.1') {
			$ip = !empty($custom_ip) ? $custom_ip : '113.111.63.245'; 
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		$this->client_ip = trim($ip);
		$geoip = Net_GeoIP::getInstance("../include/GeoLiteCity.dat");
		
		if(!empty($this->client_ip)){
			try {
				 $location = $geoip->lookupLocation($this->client_ip);
				 $this->location['countryCode'] = $location->countryCode;
				 $this->location['countryCode3'] = $location->countryCode3;
				 $this->location['countryName'] = $location->countryName;				 
				 $this->location['region_id'] = $location->region;				 
				 $this->location['city'] = $location->city;
				 $this->location['region'] = $this->getRegionName($location->city);
				 $this->location['postalCode'] = $location->postalCode;
				 $this->location['latitude'] = $location->latitude;
				 $this->location['longitude'] = $location->longitude;
				 $this->location['areaCode'] = $location->areaCode;
				 $this->location['dmaCode'] = $location->dmaCode;
			}
			catch (Exception $e) {
				throw new Exception("Invalid IP address!!");
			}
		}else{
			throw new Exception("IP address can't empty!!");
		}
	}
	
	public function getCountryData($where=''){
		$q='SELECT * FROM `geo_country` '.$where.'ORDER BY name_en';
		return $this->db->getAll($q);
	}
	
	public function getRegionData($where=''){
		$q='SELECT * FROM `geo_region` '.$where.'ORDER BY name_en';
		//echo $q;
		return $this->db->getAll($q);
	}

	public function getCityData($where=''){
		$q='SELECT * FROM `geo_city` '.$where.'ORDER BY name_en';
		return $this->db->getAll($q);
	}
		
	public function getLoaction(){
		return $this->location;
	}
	
	public function getRegionName($city_name){
		$q="select region_id from geo_city where name_en='{$city_name}'";
		$region_id=$this->db->getOne($q);
		unset($q);
		$q="select name_en from geo_region where id='{$region_id}'";
		return $this->db->getOne($q);
	}
	
}
?>