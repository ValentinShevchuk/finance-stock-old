<?
class Project extends database {
	public $account;
	public $stats;
	public $bond;

	function __construct()
	{
		$this->account=New Account();
		$this->stats=New Stats();
		$this->bond=New Bond();

		parent::__construct();

	}

	public function GetProjectNumbers($p=array())     // подсчёт заявок в базе, новых, проверенных и отмененных
	{
		 $p['json']=(int)$p['json'];

		$q="
		SELECT
			COUNT(IF (vis =2, vis, NULL )) AS new,
			COUNT(IF (vis =1, vis, NULL )) AS current,
			COUNT(IF (vis =0, vis, NULL )) AS del
		FROM project";
		$row=$this->getRow($q);

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function CreateProjectBeneficiary($p=array()) // Создать бенифициара к компании
	{
	   // print_r($p); exit;
		$p['json']=(int)$p['json'];

		$arr=$p;
		unset($arr['json']);
		$arr['id_manager']=$_SESSION['site_user'];
		$id=$this->insertdata('project_beneficiary',$arr);

		$arr=array(
				'event'=>'2',
				'event_text'=>'Создан',
				'p'=>$_GET['p']."&beneficiary"
			);
			$this->stats->CreateUserEvent($arr); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function CreateProjectOrder($p=array())     // создание проекта для утверждения приходит POST
	{
		$p['json']=(int)$p['json'];
		$arr=$p['POST'];
		$arr['id_manager']=$_SESSION['site_user'];

		//print_r($arr); exit;

		$id=$this->insertdata('project',$arr);
		$this->stats->CreateUserEvent(array( 'event'=>'2', 'event_text'=>'Создан', 'id_record'=>$id)); // записываем в лог действие
		$res=array('error'=>0);
		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $res;


	}

	public function EditProjectOrder($p=array())     // редактирование проектов
	{   
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$arr=$p['POST'];
		//print_r($arr); exit;
		$this->update('project',$arr," WHERE id='{$p['id']}'");

		$arr=array(
			'id_user'=>$_SESSION['site_user'],
			'event'=>'3',
			'event_text'=>'Исправлен',
			'url'=>"{$_SERVER['PHP_SELF']}{$_SERVER['REQUEST_URI']}",
			'p'=>$_GET['p'],
			'id_record'=>$_GET['id'] ,
			'post'=>json_encode($p['POST'], JSON_UNESCAPED_UNICODE)
		);
		$this->stats->CreateUserEvent($arr); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function CancelProjectOrder($p=array())     // редактирование проектов
	{   //print_r($p); exit;
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$arr=array('vis'=>0);
		$this->update('project',$arr," WHERE id='{$p['id']}'");

			$arr=array('event'=>'1','event_text'=>'Отменен');
			$this->stats->CreateUserEvent($arr); // записываем в лог действие


		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}
	public function ApproveProjectOrder($p=array())     // редактирование проектов
	{
		//print_r($p); exit;
		// после того как компанию подвтерждают, создается кол-во акций, указанных в заявке, с пустым полем бенефициара
		// далее когда бенефициар добавляется и указывает кол-во акций, акции заполняются в случайном порядке

		//При удалении компании, акции блокируются в поле vis=0

		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$post=isset($p['post'])?$p['post']:'';
		$arr=array('vis'=>1);
		$this->update('project',$arr," WHERE id='{$p['id']}'");

		$this->stats->CreateUserEvent(array('event'=>'5','event_text'=>'Смена статуса')); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}
	public function EditProjectBeneficiary($p=array())     // редактирвоание бенефициара
	{  // print_r($p); exit;
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$arr=$p['POST'];
		$this->update('project_beneficiary',$arr," WHERE id='{$p['id']}'");

		$this->stats->CreateUserEvent( array( 'event'=>'3', 'event_text'=>'Исправление','p'=>'project&beneficiary','post'=>$p['POST'])); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}
	public function ApproveProjectBeneficiary($p=array())     // утверждение бенефициара: id
	{  // print_r($p); exit;
		/*
			Входящие данные
			id - ид записи заявки бенефициара
			
		*/
		$p['json']=(int)$p['json'];
		$arr=array("vis"=>1);
		$p['id']=(int)$p['id'];
		$this->update('project_beneficiary',$arr," WHERE id='{$p['id']}'");
		
		// записываем в лог действие
		$arr=array(   
				'event'=>'5',
				'event_text'=>'Смена статуса',
				'p'=>$_GET['p']."&beneficiary",
		);
		$this->stats->CreateUserEvent($arr); 

		//Получаем инфу: название бонда, бенефициар, кол-во акций
		$q="
			SELECT t1.capital_in_stock as bond_count, t1.id as id_beneficiary, t2.id as bond_name, t1.id_client as id_client 
			FROM project_beneficiary t1, bond_name t2 
			WHERE t1.id_project=t2.id_project AND t1.id='{$p['id']}'";
		$row=$this->getRow($q);
		
		$this->account->CreateAccount(array('id_user'=>$row['id_client'],'bond_name'=>$row['bond_name'],'type_account'=>3)); // бенефициарный счет    
		$this->account->CreateAccount(array('id_user'=>$row['id_client'],'bond_name'=>$row['bond_name'],'type_account'=>4)); // лицевой счет
		
		/*   
					id_beneficiary === id Client_profile

			
			
				// заполняем случайные бонды
				$res['error']=$this->bond->SetBeneficiaryBond(array('bond_name'=>$row['bond_name'], 'id_beneficiary'=>$row['id_beneficiary'], 'bond_count'=>$row['bond_count']));

				// создаем аккаунт бенефициара
				// записываем сколько бондов имеется
			
		*/

		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	public function CancelProjectBeneficiary($p=array())     // редактирвоание бенефициара
	{  // print_r($p); exit;
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$arr=array("vis"=>0);
		$this->update('project_beneficiary',$arr," WHERE id='{$p['id']}'");
			   $arr=array(
				'event'=>'1',
				'event_text'=>'Отменен',
				'p'=>$_GET['p']."&beneficiary",
			);
			$this->stats->CreateUserEvent($arr); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function GetProjectBeneficiaryList($p=array()) // список бенефициаров пренадлежащих проекту
	{
		// переменные id_project - id проекта, vis - состояние
		$p['json']=(int)$p['json'];

		if (isset($p['id_project'])) { $p['id_project']=(int)$p['id_project']; $where=" AND t1.id_project='{$p['id_project']}'";}
		if (isset($p['vis']))  {$p['vis']=(int)$p['vis']; $where.=" AND t1.vis='{$p['vis']}'"; } else {  $where.=" AND t1.vis>0"; }


		$q="
			SELECT t1.id, t1.id_project,  t1.date_create, t1.capital_in_stock, 
				(SELECT CONCAT(surname,'  ',name) FROM client_profile WHERE id=t1.id_client) as name,  
				(SELECT CONCAT(surname,'  ',name) FROM user WHERE id=t1.id_manager) as fio_manager, 
				t1.id_manager, t1.vis, t2.title, t3.count
			FROM 
				project_beneficiary t1, 
				project t2,
				bond_name t3 
			WHERE 
				t1.id_project=t2.id AND
				t3.id_project=t2.id
				$where";
   // echo $q; exit;
		$row=$this->getAll($q);
		if (count($row)>0){
			foreach ($row as $item=>$key)
			{
				if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
				if ($row[$item]['vis']=='1')  $row[$item]['vis_text']='Утверждено';
				if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';

			}
			$res['row']=$row;
			$res['error']=0;
		}
		else {
			$res['error']=1;
			$res['error_msg']='Нет записей о бенефициарах';}

		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	function GetProjectBeneficiaryId($p=array()) // получить ИД бенефициара 
	{
		/*
			Входные данные:
			id_user по умолчанию Session
		*/
		$p['json']=(int)$p['json'];
		$p['id_project']=(int)$p['id_project'];
		if (isset($p['id_user'])) $p['id_user']=(int)$p['id_user']; else $p['id_user']=$_SESSION['site_user'];
	   
		$q="SELECT id FROM project_beneficiary WHERE id_client='{$p['id_user']}' AND id_project='{$p['id_project']}' AND vis=1";
		$row=(int)$this->getOne($q);
		if ($row>0) $res=array('error'=>0, 'id_beneficiary'=>$row);
		else  $res=array('error'=>1, 'error_msg'=>'Бенефициар не найден');
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	public function GetProjectList($p=array())     // список проектов vis, is_beneficary, is_stock
	{
		/*
			Входящие данные:
			
			vis (int), -- видимость объектов, по умолчанию 1
			is_beneficary (int), -- является ли проект бенефициаром, по умолчанию- нет
			is_stock (bool) -- является ли у проекта акции, по умолчанию нет
			
			На выходе:
			
			row - списисок компании
		  
		*/
		$p['json']=(int)$p['json'];
		//print_r($p); exit;
		if (isset($p['vis'])) { 
			$p['vis']=(int)$p['vis']; 
			$WHERE=" AND t1.vis='{$p['vis']}'"; 
		} else
			$WHERE=" AND t1.vis>0";

		if (isset($p['is_beneficiary'])) { 
			$p['is_beneficiary']=(int)$p['is_beneficiary']; 
			$WHERE.=" AND '{$p['is_beneficiary']}'=t1.is_beneficiary"; 
		}
	   //$get_bond_price=(int) $p['get_bond_price'];
	   
	  if ($p['is_stock']) $FROM.=" INNER JOIN project_bond_order t2 ON t2.id_project=t1.id AND t2.vis=1";            
		else  $FROM.="";
		$q="
			SELECT 
				t1.*, t3.count as count_all, t3.count_in_market,
				(SELECT COUNT(id) FROM project_beneficiary t2  WHERE t1.id=t2.id_project AND t2.vis=1) as beneficiary_count, t4.*
			FROM 
				user t4,
				project t1 LEFT JOIN bond_name t3 ON t3.id_project=t1.id
				$FROM
			WHERE 
				t4.id=t1.id_manager 
				$WHERE";
		//echo $q; exit;
		$row=$this->getAll($q);
		
		//echo $q; 
		foreach ($row as $item=>$key)
		{
			if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
			if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Утверждено';
			if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';
			if ($get_bond_price==1){
				$row_res=$this->bond->GetPriceForChart(array('limit'=>20, 'id_bond_name'=>$row[$item]['id']));
				if ($row_res['error']==0) $row[$item]['bond_price']= $row_res['price']; else $row[$item]['bond_price']=$row_res['error_msg'];
			}
		}
		
		//print_r($row); exit;
		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}
	public function GetProjectNewsList($p=array()) // список новостей id_projectv ид проекта, vis - сортировка новостей по полю vis
	{
		$p['json']=(int)$p['json'];
		if (isset($p['vis'])) { $p['vis']=(int)$p['vis']; $where=" AND t1.vis={$p['vis']}"; } else { $where=" AND t1.vis>0";}//
		if (isset($p['id_project'])) {  $p['id_project']=(int)$p['id_project']; $where.=" AND  '{$p['id_project']}'=t1.id_project";  }

		$q="SELECT t1.*, t2.title as project_title FROM project_news t1 LEFT JOIN  project t2 ON t1.id_project=t2.id WHERE t1.id>0 $where";
		//echo $q; exit;
		$row=$this->getAll($q);
		if (count($row)>0){
			foreach ($row as $item=>$key)
			{
				if ($row[$item]['project_title']=='') $row[$item]['project_title']='Проект не указан!';
				if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
				if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Утверждено';
				if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';

			}
			$res=array('error'=>0, 'row'=>$row);
		}else{
			$res=array('error'=>1);
		}


		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}

	public function CreateProjectNews($p=array())     // создание ноновсти к проекту
	{
		//$p['json']=(int)$p['json'];
		$arr=$p;
		$arr['id_manager']=$_SESSION['site_user'];
		$id=$this->insertdata('project_news',$arr);

		$this->stats->CreateUserEvent(array( 'event'=>'2', 'event_text'=>'Создан', 'id_record'=>$id)); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}
	public function GetProjectNewsInfo($p=array())     // инфа одного проекта
	{
		$p['json']=(int)$p['json'];
		
		if (isset($p['id'])) {
			$p['id']=(int)$p['id'];
			$p['id_project']=(int)$p['id_project'];

			if ($p['id']>0) $q="SELECT * FROM project_news WHERE id='{$p['id']}'";
			else $q="SELECT * FROM project_news WHERE id_project='{$p['id_project']}' AND vis=1 ORDER by id DESC LIMIT 1";
			//echo $q; exit;
			$row=$this->getRow($q);

			if (count($row)>0){
			$this->stats->CreateUserEvent( array( 'event'=>'4', 'event_text'=>'Просмотр')); // записываем в лог действие        if ($row['vis']=='2') $row['vis_text']='Новый';
			if ($row['vis']=='1') $row['vis_text']='Утверждено';
			if ($row['vis']=='0') $row['vis_text']='Удалено';
			$res['row']=$row;
			$res['error']=0;
			}
			else { $res=array('error'=>1, 'error_msg'=>'Информация о проекте не найдена');}
		}else {
			$res=array('error'=>1,'error_msg'=>'Не передан ID проекта');
		}
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	public function  EditProjectNews($p=array())     // редактирование проектов
	{   //print_r($p); exit;
		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$arr=$p['POST'];
		
		$this->update('project_news',$arr," WHERE id='{$p['id']}'");

			   $arr=array(
				'id_user'=>$_SESSION['site_user'],
				'event'=>'3',
				'event_text'=>'Исправлен',
				'url'=>"{$_SERVER['PHP_SELF']}{$_SERVER['REQUEST_URI']}",
				'p'=>$_GET['p']."&news",
				'id_record'=>$_GET['id'] ,
				'post'=>json_encode($p['POST'], JSON_UNESCAPED_UNICODE)
			);
			$this->stats->CreateUserEvent($arr); // записываем в лог действие
		$res=array('error'=>0);
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	
	public function ApproveProjectNews($p=array())     // редактирование проектов
	{
	//print_r($p); exit;
	// после того как компанию подвтерждают, создается кол-во акций, указанных в заявке, с пустым полем бенефициара
	// далее когда бенефициар добавляется и указывает кол-во акций, акции заполняются в случайном порядке

	//При удалении компании, акции блокируются в поле vis=0

		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$post=isset($p['post'])?$p['post']:'';
		$arr=array('vis'=>1);
		$this->update('project_news',$arr," WHERE id='{$p['id']}'");

		$this->stats->CreateUserEvent(array('event'=>'5','event_text'=>'Смена статуса')); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function CancelProjectNews($p=array())     // редактирование проектов
	{
	//print_r($p); exit;
	// после того как компанию подвтерждают, создается кол-во акций, указанных в заявке, с пустым полем бенефициара
	// далее когда бенефициар добавляется и указывает кол-во акций, акции заполняются в случайном порядке

	//При удалении компании, акции блокируются в поле vis=0

		$p['json']=(int)$p['json'];
		$p['id']=(int)$p['id'];
		$post=isset($p['post'])?$p['post']:'';
		$arr=array('vis'=>0);
		$this->update('project_news',$arr," WHERE id='{$p['id']}'");

		$this->stats->CreateUserEvent(array('event'=>'1','event_text'=>'Отмена')); // записываем в лог действие

		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;
	}

	public function GetProjectInfo($p=array())     // инфа одного проекта
	{
		$p['json']=(int)$p['json'];

		if (isset($p['id'])) { 
			$p['id']=(int)$p['id'];
			$q="SELECT * FROM project WHERE id='{$p['id']}'";
			//echo $q; exit;
			$row=$this->getRow($q);
			if (count($row)>0){
			$this->stats->CreateUserEvent( array( 'event'=>'4', 'event_text'=>'Просмотр')); // записываем в лог действие        if ($row['vis']=='2') $row['vis_text']='Новый';
			if ($row['vis']=='1') $row['vis_text']='Утверждено';
			if ($row['vis']=='0') $row['vis_text']='Удалено';
			$res['row']=$row;
			$res['error']=0;
			}
			else { $res=array('error'=>1, 'error_msg'=>'Информация о проекте не найдена');}
		}else {
			$res=array('error'=>1,'error_msg'=>'Не передан ID проекта');
		}
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
	
	public function GetBeneficiaryInfo($p=array())
	// инфа одного бенефициара
	{
		$p['json']=(int)$p['json'];
		if (isset($p['id']) || isset($p['id_client'])){
			$p['id']=(int)$p['id'];
			$p['id_client']=(int)$p['id_client'];
			if ($p['id']>0) $where=" AND t1.id='{$p['id']}'";
			if ($p['id_client']>0) $where=" AND t1.id_client='{$p['id_client']}'";
			$q=" SELECT t1.*, COUNT(t3.id) as beneficiary_bond, t2.par_now as value_of_stock, t2.count as bond_all
				FROM 
					project_beneficiary t1 
						LEFT JOIN bond t3 ON t3.id_project_beneficiary=t1.id
						LEFT JOIN bond_name t2 ON  t2.id_project=t1.id_project 
				WHERE 
				t1.vis>0 $where";
					//echo $q;
			$row=$this->getRow($q);

			if (PEAR::isError($row)) {
				
			//die($row->getMessage());
				$res=array('error'=>1, 'error_msg'=>'Информация о бенефициаре не найдена');
			} 
			else { 
				if (count($row)>0){
					$this->stats->CreateUserEvent(array('event'=>'4','event_text'=>'Просмотр','p'=>$_GET['p']."&beneficiary"));
					if ($row['vis']=='2') $row['vis_text']='Новый';
					if ($row['vis']=='1') $row['vis_text']='Утверждено';
					if ($row['vis']=='0') $row['vis_text']='Удалено';

					$row['percent']=($row['capital_in_stock']/$row['bond_all'])*100;
					$res['row']=$row;
					$res['error']=0;
				}
				else { $res=array('error'=>1, 'error_msg'=>'Информация о бенефициаре не найдена');}
			}

	   }
		else {
			$res=array('error'=>1,'error_msg'=>'Не передан ID бенефициара');
		}


		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}
}
?>
