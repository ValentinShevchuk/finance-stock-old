<?php
class Bonds extends dbBonds {

    public $timeStart=0;
    public $date_create=0;
    public $type_bonds=1;

    function SetWhereTime($type_bonds) {

      $h=date("H",time())+0;

      $this->timeStart=strtotime(date("Y-m-d 10:00:00"));

      $this->date_create=" AND date_create>'".$this->timeStart."'";

      $q="SELECT  date_create FROM amount WHERE type_bonds='$type_bonds' {$this->date_create} LIMIT 1";
      $timeStart=$this->db->getOne($q);

      if ($timeStart>0) {

      } else {
       $q="SELECT date_create FROM amount WHERE type_bonds='$type_bonds' ORDER by id DESC LIMIT 1";

       $this->timeStart=$this->db->getOne($q)-43200;
       $this->date_create=" AND date_create>'".$this->timeStart."'";

     }



     $this->type_bonds=$type_bonds;


    }
       
    public function GetBondsInfo($param=array()) 
    // получение инфы по всем или конкрентной акции если передать параметра type_bonds
    {
      //  print_r($param); 
        $where='';
     //   if (isset($param['type_bonds'])) $where=" t1.id={$param['type_bonds']} AND ";
        if (isset($param['0']['type_bonds'])) $where=" t1.id={$param['0']['type_bonds']} AND ";
        $q="
            SELECT t1.*, 
            (SELECT price FROM amount t2 WHERE t2.type_bonds=t1.id ORDER by t2.id DESC LIMIT 1) as price ,
            (SELECT price_buy FROM amount t3 WHERE t3.type_bonds=t1.id ORDER by t3.id DESC LIMIT 1) as price_buy,
            (SELECT  price_sell FROM amount t4 WHERE t4.type_bonds=t1.id ORDER by t4.id DESC LIMIT 1) as price_sell
            FROM bonds_name t1 WHERE $where t1.is_bond=1";
    // echo $q;      exit;
        if (isset($param['0']['type_bonds']))
        { // есле передан ID то не зачем грузить двойной массив, достаточно Row
            $row=$this->getRow($q);  

        } 
        else 
        $row=$this->getAll($q);
        
        $res= array('row'=>$row);
        
        if ($param[0]['json']==1)  {
            $result =  json_encode($res );
            echo "jsonpCallback(".$result.")";
        } else return  $res;
    

    }
    
    public function Price($type_bonds=1,$is_demo=0) // старая функция
     {

       $this->type_bonds=$type_bonds;

       $this->SetWhereTime($type_bonds);

       $q = "SELECT price FROM amount WHERE type_bonds='$type_bonds' {$this->date_create} LIMIT 1";
       $PriceBegin=$this->db->getOne($q);
       //$PriceBegin=$q;

       $q = "SELECT MAX(price) FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC";  //echo $q;
       $priceMax=$this->db->getOne($q);

       $q = "SELECT MIN(price)FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC";
       $priceMin=$this->db->getOne($q);


      $q = "SELECT price, price_buy, price_sell FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC LIMIT 1";
//echo $q;
      $row=$this->db->getRow($q);

      $PriceLast=$row['price'];
     // $PriceLast=$q;
      $difference=$PriceLast-$PriceBegin;
      $differencePercent=round(($difference*100)/$PriceBegin,4);
      $difference=round($difference,4);

      if ($difference>=0) $rost=1; else $rost=0;

      $array1=array(
        'PriceBegin'=>$PriceBegin,
        'PriceLast'=>$PriceLast,
        'rost'=>$rost,
        'difference'=>$difference,
        'differencePercent'=>$differencePercent,
        'price'=>$row['price'],
        'price_buy'=>$row['price_buy'],
        'price_sell'=>$row['price_sell'],
        'PriceMax'=>$priceMax,
        'PriceMin'=>$priceMin,
        'DateTimeNow'=>date("d/m/Y, H:i")
        );
                                        // AND date_create='".strtotime(date("Y-m-d"))."'
      $q = "SELECT * FROM statistics_day WHERE type_bonds='$type_bonds' AND is_demo=$is_demo  ORDER BY id DESC LIMIT 1";
//echo $q;
      $row=$this->db->getRow($q);
      $array1['count_open_position']=$row['count_open_position'];
      $array1['cop_num']=$row['cop_num'];
      $array1['cop_percent']=$row['cop_percent'];
      $array1['count_close_position']=$row['count_close_position'];
      $array1['ccp_num']=$row['ccp_num'];
      $array1['ccp_percent']=$row['ccp_percent'];
      $array1['count_deal']=$row['count_deal'];
      $array1['cd_percent']=$row['cd_percent'];
      $array1['cd_num']=$row['cd_num'];
      $array1['tourover_all_deal']=$row['tourover_all_deal'];
      $array1['tourover_last_position']=$row['tourover_last_position'];
      $array1['price_last_position']=$row['price_last_position'];
      $array1['year_min_price']=$row['year_min_price'];
      $array1['year_max_price']=$row['year_max_price'];
      $array1['year_min_date']=date("Y-m-d",$row['year_min_date']);
      $array1['year_max_date']=date("Y-m-d",$row['year_max_date']);


    return  $array1;
    }
    
public function PriceAll($param=array()) //список акций и котировки последние 1000 значений, возможно уменшить до 100. Переделать запрос, чтобы грузил, последние 100 с минутами, 100 с 5 минутами
{    
    
    $type_bonds=isset($param[0]['type_bonds'])?$param[0]['type_bonds']:1;
    $is_demo=isset($param[0]['is_demo'])?$param[0]['is_demo']:0;
    
    $q="SELECT * FROM amount WHERE type_bonds='{$type_bonds}'  ORDER by id DESC LIMIT 1000";
    //echo $q;
    $row=$this->getall($q);
    $last_id=0;
    foreach ($row as $item=>$key)
    {
        if ($last_id==0)$last_id=$row[$item]['id'];
        $row[$item]['date_create']=date("Y-m-d H:i:00",$row[$item]['date_create']);
    }    
    $res=array('row'=>$row, 'last_id'=>$last_id);
    if ($param[0]['json']==1)  {
        //array('row'=>$row)
        $result =  json_encode( $res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;
    
}

public function Price_($param=array() )// фукнция по получения цены по JSON по новому формату и все статистики по по Акции
{
    
    $type_bonds=isset($param[0]['type_bonds'])?$param[0]['type_bonds']:1;
    $is_demo=isset($param[0]['is_demo'])?$param[0]['is_demo']:0;
        
       $this->type_bonds=$type_bonds;

       $this->SetWhereTime($type_bonds);

       $q = "SELECT price FROM amount WHERE type_bonds='$type_bonds' {$this->date_create} LIMIT 1";
       $PriceBegin=$this->db->getOne($q);
       //$PriceBegin=$q;

       $q = "SELECT MAX(price) FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC";  //echo $q;
       $priceMax=$this->db->getOne($q);

       $q = "SELECT MIN(price)FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC";
       $priceMin=$this->db->getOne($q);


      $q = "SELECT price, price_buy, price_sell, date_create FROM amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC LIMIT 1";
//echo $q;
      $row=$this->db->getRow($q);

      $PriceLast=$row['price'];
     // $PriceLast=$q;
      $difference=$PriceLast-$PriceBegin;
      $differencePercent=round(($difference*100)/$PriceBegin,4);
      $difference=round($difference,4);

      if ($difference>=0) $rost=1; else $rost=0;

      $array1=array(
        'PriceBegin'=>$PriceBegin,
        'PriceLast'=>$PriceLast,
        'rost'=>$rost,
        'difference'=>$difference,
        'differencePercent'=>$differencePercent,
        'price'=>$row['price'],
        'price_buy'=>$row['price_buy'],
        'price_sell'=>$row['price_sell'],
        'PriceMax'=>$priceMax,
        'PriceMin'=>$priceMin,
        'DateTimeNow'=>date("Y-m-d H:i:00"),
        'date_create'=>$row['date_create']
        );
                                        // AND date_create='".strtotime(date("Y-m-d"))."'
      $q = "SELECT * FROM statistics_day WHERE type_bonds='$type_bonds' AND is_demo=$is_demo  ORDER BY id DESC LIMIT 1";
//echo $q;
      $row=$this->db->getRow($q);
      $array1['count_open_position']=$row['count_open_position'];
      $array1['cop_num']=$row['cop_num'];
      $array1['cop_percent']=$row['cop_percent'];
      $array1['count_close_position']=$row['count_close_position'];
      $array1['ccp_num']=$row['ccp_num'];
      $array1['ccp_percent']=$row['ccp_percent'];
      $array1['count_deal']=$row['count_deal'];
      $array1['cd_percent']=$row['cd_percent'];
      $array1['cd_num']=$row['cd_num'];
      $array1['tourover_all_deal']=$row['tourover_all_deal'];
      $array1['tourover_last_position']=$row['tourover_last_position'];
      $array1['price_last_position']=$row['price_last_position'];
      $array1['year_min_price']=$row['year_min_price'];
      $array1['year_max_price']=$row['year_max_price'];
      $array1['year_min_date']=date("Y-m-d",$row['year_min_date']);
      $array1['year_max_date']=date("Y-m-d",$row['year_max_date']);


    if ($param[0]['json']==1)  {
        $result =  json_encode( $array1 );
        echo "jsonpCallback(".$result.")";
    } else return  $array1;
    }

public function OrderSell($param=array()) // заявка на покупку (ПРОДАЖА клиенту), проверка команды, наличие средств и т.д.
{
    //print_r($param);
    $type_bonds=$param[0]['type_bonds']; // ID акции 
    $price=$this->GetBondsInfo(array('0'=>array('type_bonds'=>$type_bonds)));
    //print_r($price);
    $price_real=$price['row']['price'];
    $price_sell=$price['row']['price_sell']; // цена для продажи
    $type_account=$_SESSION['site_user_account_active']; // активный счет ID record
    $count=$param[0]['count'];
    $total=$price_sell*$count;
    $id_account=$this->AccountsGetInfo($_SESSION['site_user'],$type_account); // информация по счету
    
   // echo  " OrderSell $count, $type_bonds,$price_sell, $total, {$_SESSION['site_user']}, {$id_account['id']},{$id_account['type_accounts']},$price_real ";
    
  

    if ($id_account['value']>$total){ 
        
        $result=$this->Buy($count, $type_bonds,$price_sell, $total, $_SESSION['site_user'], $id_account['id'],$id_account['type_accounts'],$price_real);
        if ($result['error']==0){
            $res['error']=0;
            $res['msg']='Акции успешно преобретены';
        }
        else {
             $res['error']=1;
             $res['error_msg']=$result['error_msg'];
        }
    }
    else { 
        $res['error']=1; 
        $res['error_msg']='Недостаточно средств';
    }
    
    if ($param[0]['json']==1)  {
        //array('row'=>$row)
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $row;    
}    

    public function Buy($count, $type_bonds, $price, $total, $id_user,$account, $type_accounts,$real_price)
    // покупка клиентом (Продажа клиенту)
    {
       $q = "SELECT COUNT(id) id FROM bonds WHERE id_user=0 AND id_bonds_name=$type_bonds";//echo $q;exit;
       $counts=$this->db->GetOne($q);
       //echo $counts; exit;
       if ($counts>0)

       {  $is_demo=0;

       if ($type_accounts==4) $is_demo=1;


         $type_bonds=(int)$type_bonds;
         $q="SELECT title FROM bonds_name WHERE id={$type_bonds}";
         $title_bonds = $this->db->getOne($q);


         $this->DecrementAccountUser($total,$account,1,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Покупка (&laquo;'.$title_bonds.'&raquo;)');
          // списываем сумму бондов со счета
          // Добавляем в историю, что списали.


          $real_price_total=$real_price*$count;
          // создаем открытую позицию с кол-вом
          $number_transaction = $open_postion=$this->CreateOpenPosition($count,$type_bonds, $price,$total,$id_user,$is_demo,$real_price,$real_price_total);

          if ($is_demo==0) $this->IncrementAccountUser($total,1,3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Продажа бондов (&laquo;'.$title_bonds.'&raquo;) клиенту',$account,$number_transaction='0');// уменьшаем деньги у биржи

          // Занимаем случайные бонды в базе

          $q="UPDATE bonds SET id_position={$open_postion}, id_user=$id_user WHERE id_user=0 AND id_bonds_name='$type_bonds' ORDER by id LIMIT $count";

          $this->db->query($q);


          // Добавляем в бонды метку, к какой позиции они относятся

          return array('error'=>0,'msg'=>'Акции успешно преобретены');
       }
       else
       {

        return array('error'=>1,'error_msg'=>'Нет доступных акций');
       }
    }
    
    public function OrderBuy($param=array()) // Покупка у клиента, проверка команды, наличие средств и т.д.
{
    //print_r($param);
    $type_bonds=$param[0]['type_bonds']; // ID акции 
    $price=$this->GetBondsInfo(array('0'=>array('type_bonds'=>$type_bonds)));
    $open_position=$param[0]['open'];
    $price_real=$price['row']['price'];
    $price_buy=$price['row']['price_buy']; // цена для продажи
    $type_account=$_SESSION['site_user_account_active']; // активный счет ID record
    $count=$param[0]['count'];
    $total=$price_buy*$count;
    $id_account=$this->AccountsGetInfo($_SESSION['site_user'],$type_account); // информация по счету
    
    
    //echo  "$count, $type_bonds,$price_buy, $total, {$_SESSION['site_user']}, {$_SESSION['site_user_account_active']},$open_position,{$id_account['type_accounts']},{$price_real}";
    
  

    if ($id_account['value']>$total){ 
        
       $this->sell($count, $type_bonds,$price_buy, $total, $_SESSION['site_user'], $_SESSION['site_user_account_active'],$open_position,$id_account['type_accounts'],$price_real);
        $res['error']=0;
        $res['msg']='Акции успешно проданы';
    }
    else { $res['error']=1; $res['error_msg']='Недостаточно средств';}
    
    if ($param[0]['json']==1)  {
        //array('row'=>$row)
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $row;    
}    
    public function Sell($count, $type_bonds, $price, $total, $id_user,$account,$open, $type_accounts,$real_price)
    {
       //$account_info=$this->AccountsGetValue($id_user,$account);
       $open_position_info=$this->OpenPositionGetInfo($open,$id_user);



          if ($open_position_info[bonds_count]>=$count) {


           $is_demo=0;
           if ($type_accounts==4) $is_demo=1;
           $type_bonds=(int)$type_bonds;
           $q="SELECT title FROM bonds_name WHERE id={$type_bonds}";
           $title_bonds = $this->db->getOne($q);


           $this->IncrementAccountUser($total,$account,3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Продажа (&laquo;'.$title_bonds.'&raquo;)');  // пополняем счет

           $this->DecrementBondsOpenPosition($open, $count);   // уменьшаем кол-во купленных бондов

           $q="UPDATE bonds SET id_position=0, id_user=0 WHERE id_position=$open AND id_user='$id_user' AND id_bonds_name='$type_bonds' ORDER by id LIMIT $count";
           $this->db->query($q);    // в случайных бондов кол-во равных проданным, обнуляем владельца и позицию.


           $real_price_total=$real_price*$count; // на продажу по цене биржи без процентов
           $number_transaction = $this->CreateClosePosition($count,$type_bonds,$open_position_info['price'],$open_position_info['price_real'], $price,$total,$id_user, $open,$is_demo,$real_price,$real_price_total); // создаем закрытую позицию

           /*-----------Расчет действий с балансом биржи---------------*/
           /*if ($is_demo==0){
             $profit = $total - ($open_position_info['price']*$count);
             if ($profit > 0){
                $this->IncrementAccountUser(abs($profit),1,3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Убыток при закрытии сделки на биржевом рынке',$account,$number_transaction='0');
             }else{
                $this->DecrementAccountUser(abs($profit),1,1,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Прибыль при закрытии сделки на биржевом рынке',$account,$number_transaction='0');
             }
           }*/

           if ($is_demo==0) $this->DecrementAccountUser($total,1,1,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Покупка бондов (&laquo;'.$title_bonds.'&raquo;) у клиента',$account,$number_transaction='0');// уменьшаем деньги у биржи

           $mess="Вы успешно продали: $count шт. за $price руб.";
          // Добавляем в бонды метку, к какой позиции они относятся
          } else $mess='Ошибка! Нехватает бондов!';


       return $mess;
    }

    public function OpenPositionGetInfo($open,$id_user)
    {
        
      $q="SELECT * FROM bonds_position_open WHERE id='$open' AND id_user='$id_user'";
      return $this->db->getRow($q);
    }
    public function DecrementBondsOpenPosition($open, $count)
    {
        $q="SELECT bonds_count FROM bonds_position_open WHERE id=$open";
        $bonds_count=$this->getOne($q);
        if ($bonds_count-$count>=0){
        $q="UPDATE bonds_position_open SET bonds_count=bonds_count-$count, total=(bonds_count*price) WHERE id=$open AND bonds_count>0";
        $this->db->query($q);
        return 1;
        }
        else return 0;
    }

    public function CreateClosePosition($count,$type_bonds, $price_buy,$price_buy_real, $price_sell,$price_sell_total, $id_user,$open,$is_demo,$price_real,$price_real_total)
    {


        $inData = array(
			'id_user'=>$id_user,

			'date_create'=>time(),
			'price_buy'=>$price_buy,
			'price_buy_real'=>$price_buy_real,
			'price_sell'=>$price_sell,
			'price_sell_total'=>$price_sell_total,
			'price_real'=>$price_real,
			'price_real_total'=>$price_real_total,
			'type_bonds'=>$type_bonds,
            'bonds_count'=>$count,
            'id_open'=>$open,
            'is_demo'=>$is_demo);

        return $this->insertdata('bonds_position_close',$inData);
    }

    public function CreateOpenPosition($count,$type_bonds, $price, $total,$id_user,$is_demo,$price_real,$price_real_total)
    {
        $inData = array(
			'id_user'=>$id_user,
			'date_create'=>time(),
			'price'=>$price,
			'total'=>$total,
			'type_bonds'=>$type_bonds,
            'bonds_count'=>$count,
            'price_real'=>$price_real,
            'price_real_total'=>$price_real_total,
            'is_demo'=>$is_demo);

        return $this->insertdata('bonds_position_open',$inData);
    }


    public function CreateBondsMysql($count,$id_bonds_name,$id_user=0,$demo=0)
    {

        $q='INSERT INTO bonds (full_name,id_bonds_name,date_create,id_user,id_position,price_buy, date_buy) VALUES';
        for($i=1;$i<=$count-1;$i++){
          $q.="('','$id_bonds_name','".time()."',$id_user,0,0,0),";
        }

        $q.="('','$id_bonds_name','".time()."',$id_user,0,0,0)";
        echo $q;
        $this->db->query($q);

        //$short_name_bonds=$this->db->getOne("SELECT short_name FROM bonds_name WHERE id=$id_bonds_name");

        $q="SELECT * FROM bonds WHERE full_name='' AND id_bonds_name=$id_bonds_name";
        echo $q;
        $row=$this->db->getAll($q);

        foreach ($row as $row)
        {
          $full_account=$this->CreateNumber($row[id],$id_bonds_name);
          $q="UPDATE bonds SET full_name='$full_account' WHERE id={$row[id]}";
          $this->db->query($q);
        }
    }


    public function GetOpenPositionAdd($id_user=0, $type_bonds=0, $active_account=0) // count and price
    {

       $add_where='';
      if ($active_account>0) {$is_demo=0;  if ($active_account==2) $is_demo=1; $add_where.=" AND t1.is_demo=$is_demo";  } // для получения полного списка open position без градации demo
      if ($id_user>0) { $add_where.="  AND t1.id_user='{$id_user}'";  }
      if ($type_bonds>0) { $add_where.=" AND t1.type_bonds='{$type_bonds}'";  }

      $q = "SELECT  SUM(t1.total) sum_price, SUM(t1.bonds_count) count_bond, SUM(t1.total) total  FROM bonds_position_open t1, bonds_name t2  WHERE t1.type_bonds=t2.id AND t1.vis>0 AND t1.bonds_count>0 $add_where";


      return  $this->db->getRow($q);
    }
    public function GetOpenPosition($id_user=0, $type_bonds=0, $active_account=0, $order='')
    {
      $add_where='';
      if ($active_account>0) {$is_demo=0;  if ($active_account==2) $is_demo=1; $add_where.=" AND t1.is_demo=$is_demo";  } // для получения полного списка open position без градации demo
      if ($id_user>0) { $add_where.="  AND t1.id_user='{$id_user}'";  }
      if ($type_bonds>0) { $add_where.=" AND t1.type_bonds='{$type_bonds}'";  }

      $q = "SELECT *, t1.id, t1.date_create,t1.is_demo FROM bonds_position_open t1, bonds_name t2  WHERE t1.type_bonds=t2.id AND t1.vis>0 AND t1.bonds_count>0 $add_where $order";
//echo $q;

      return  $this->db->getAll($q);
    }

    public function GetClosePosition($id_user=0, $type_bonds=0, $active_account=0)
    {
      $add_where='';
      if ($active_account>0) {$is_demo=0;  if ($active_account==2) $is_demo=1; $add_where.=" AND t1.is_demo=$is_demo";  } // для получения полного списка open position без градации demo
      if ($id_user>0) { $add_where.="  AND t1.id_user='{$id_user}'";  }
      if ($type_bonds>0) { $add_where.=" AND t1.type_bonds='{$type_bonds}'";  }

      $q = "SELECT  *, t1.id, t1.date_create  FROM bonds_position_close t1, bonds_name t2 WHERE t1.type_bonds=t2.id AND t1.vis>0 $add_where";

      return  $this->db->getAll($q);
    }


// -----------------       функции с аккаунтом клиента

public function SetAccountActive($param=array()) // установить активный аккаунт
{

  $_SESSION['active_account']=$param[0]['active_account'];
 // echo  $_SESSION['active_account']."|";
  if ($param[0]['active_account']=='1') { $acc=$_SESSION['site_user_accounts']; }
  if ($param[0]['active_account']=='2') { $acc=$_SESSION['site_user_accounts_demo'];  }
  
  $_SESSION['site_user_account_active']=$acc;
          
    if ($param[0]['json']==1)  {
        $result =  json_encode( array('res'=>$acc));
        echo "jsonpCallback(".$result.")";
    } else return $acc;
}

public function GetAccountsList($param=array())
{
    if (isset($param[0]['id_user'])) $id_user=" AND t1.id_user='{$param[0]['id_user']}'";
    $q = "SELECT * FROM accounts t1 WHERE id>0 {$id_user}";       
    $row=$this->db->getAll($q);
  
    if ($param[0]['json']==1)  {
        $result =  json_encode($row);
        echo "jsonpCallback(".$result.")";
    } else return $row;
}
    
    
    public function CreateNumber($id_record,$type_bonds)
    {

        $q="SELECT * FROM bonds_name WHERE id=$type_bonds";
        $prefix=$this->db->getRow($q);

        $zero='';
        $full_account='';



    	for ($i=0;$i<(12-strlen($id_record));$i++) $zero.='0';
        $full_account = $prefix[prefix_begin].$zero.$id_record.$prefix[prefix_end];
        //echo $full_account;
        return $full_account;

    }


    public function DecrementAccountUser($amount, $id_account,$type_events=1, $id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0) // 1 - списание на покупку сертов - 2 -списание на вывод 3 - пополнение с продажи 4 - пополнение счета
    {

       $q="UPDATE accounts SET value=value-$amount WHERE id={$id_account}";
       $this->db->query($q);

       $q="SELECT value FROM accounts WHERE id={$id_account}";
       $account_value = $this->db->getOne($q);



       $this->AccountsHistory($id_account, $id_manager, $type_events,$amount, $trans_number, $trans_type, $trans_descr,$account_value,$account_user,$number_transaction);
    }


     public function IncrementAccountUser($amount,$id_account,$type_events=3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='',$account_user=0,$number_transaction=0)
     // 1 - списание на покупку сертов - 2 -списание на вывод 3 - пополнение с продажи 4 - пополнение счета
    {

       $q="UPDATE accounts SET value=value+$amount WHERE id={$id_account}";  //echo $q;
       $this->db->query($q);

       $q="SELECT value FROM accounts WHERE id={$id_account}";
       $account_value = $this->db->getOne($q);

       $this->AccountsHistory($id_account, $id_manager, $type_events,$amount, $trans_number, $trans_type, $trans_descr,$account_value,$account_user,$number_transaction);

    }

    public function AccountsHistory($id_account, $id_manager, $type_events,$amount, $trans_number=0, $trans_type=0, $trans_descr='',$account_value,$account_user=0,$number_transaction=0)
    {
         $inData = array(

			'id_accounts'=>$id_account,
			'id_manager'=>$id_manager,
			'date_create'=>time(),
			'type_events'=>$type_events,
            'value'=>$amount,
            'account_value'=>$account_value,
			'vis'=>1,
            'trans_number'=>$trans_number,
            'trans_type'=>$trans_type,
            'trans_descr'=>$trans_descr,
            'account_user'=>$account_user,
            'number_transaction'=>$number_transaction
            );

        $this->insertdata('accounts_history',$inData);
    }

                                 //type_bonds = type_accounts
    public function AccountsNew($id_user,$type_bonds,$id_manager=0,$deposit=0,$id_contract=0)
    {
        $q = "SELECT id FROM accounts WHERE id_user='{$id_user}' AND type_accounts=$type_bonds";
		$is_account = $this->db->getOne($q);
        if ($is_account==0 || (int)$type_bonds==7 || (int)$type_bonds==8){
          $zero='';
  		$full_account='';

        $inData = array(
  			'id_user'=>$id_user,
  			'id_manager'=>$id_manager,
  			'date_create'=>time(),
  			'type_accounts'=>$type_bonds,
            'value'=>'0',
  			'vis'=>1,
            'id_contract'=>$id_contract);

          $id_record=$this->insertdata('accounts',$inData);

          $this->session['site_user_account_bond']=$id_record;


          if ($deposit>0) $this->IncrementAccountUser($deposit,$id_record,4); // добавляем стартовый капитал


          $inData = array(
  			'full_account'=>$this->CreateNumber($id_record,$type_bonds)
  			);

          $this->update('accounts',$inData,	$where = "WHERE `id`= $id_record");

          return "Счет создан";
        }
        else
        {
          $_SESSION[site_user_account_bond]=$is_account;

          return "Счет уже создан.";


        }
    }


    public function AccountsDemoValueGet($id_user)
    {
       $q = "SELECT value FROM accounts WHERE id_user='{$id_user}' AND type_accounts=1 AND is_demo=1";
	   return  $this->db->getOne($q);
    }

    public function AccountsGetValue($id_user,$id_record)
    {
       $q = "SELECT id,value FROM accounts WHERE id_user='{$id_user}' AND id={$id_record}";
	   return  $this->db->getRow($q);
    }

    public function AccountsGetInfo($id_user,$id_record)
    {
       $q = "SELECT * FROM accounts WHERE id_user='{$id_user}' AND id={$id_record} LIMIT 1";
	   return  $this->db->getRow($q);
    }
    
    public function GetAccounts($id_user,$where='1=1')
    {

       $q = "SELECT *, t1.id,t1.date_create FROM accounts t1,bonds_name t2 WHERE t1.type_accounts=t2.id AND id_user='{$id_user}' AND $where";       
	   return $this->db->getAll($q);
    }

    public function GetAccountsId($id_user,$type_bonds) // получаем id счета по типу
    {
       $q = "SELECT * FROM accounts WHERE id_user='{$id_user}' AND type_accounts='{$type_bonds}'";
	   return $this->db->getRow($q);
    }


    public function SessionAccountsType()  //запись в сессию типа аккаунта
    {
       $q = "SELECT * FROM bonds_name";
	   $row = $this->db->getAll($q);
       foreach ($row as $row){
         $_SESSION['accounts_type'][$row['id']] = $row['title'];
       }
    }

    public function GetAccountsHistory($id_accounts,$where='1=1')
    {
       $q = "SELECT t1.*,t2.full_account FROM accounts_history t1,accounts t2 WHERE t1.id_accounts='{$id_accounts}' AND t1.id_accounts=t2.id AND ".$where." ORDER BY t1.date_create DESC";
	   return $this->db->getAll($q);
    }

    public function GetAccountsHistoryStock($id_accounts,$where='1=1')   // отличается только тем что получается full_account и id_user в зависимости от account_user
    {
       $q = "SELECT t1.*,t2.full_account,t2.id_user FROM accounts_history t1,accounts t2 WHERE t1.id_accounts='{$id_accounts}' AND t1.account_user=t2.id AND ".$where." ORDER BY t1.date_create DESC";
       return $this->db->getAll($q);
    }

    public function GetAccountsHistoryFund($id_accounts,$where='1=1')   // отличается только тем что получается full_account и id_user в зависимости от account_user
    {
       $q = "SELECT t1.*,t2.full_account,t2.id_user FROM accounts_history t1,accounts t2 WHERE t1.id_accounts='{$id_accounts}' AND t1.account_user=t2.id AND ".$where." ORDER BY t1.date_create DESC";
       return $this->db->getAll($q);
    }
//---------------------------------------------
//--------------- открытые и закрытые
    public function GetOpenAndClosePosition($date_from,$date_to)
    {
       /*$q="SELECT t1.*,t2.full_account
        FROM `bonds_position_close` t1
            LEFT JOIN accounts t2 ON t1.id_user=t2.id
                WHERE t1.is_demo=0 AND t1.date_create>='".$date_from."' AND t1.date_create<='".$date_to."'
       UNION ALL
       SELECT t3.id, t3.id_user, t3.date_create, price, 0 , t3.type_bonds, t3.bonds_count, NULL , t3.is_demo,t4.full_account
        FROM `bonds_position_open` t3
            LEFT JOIN accounts t4 ON t3.id_user=t4.id
            WHERE t3.is_demo=0 AND t3.date_create>='".$date_from."' AND t3.date_create<='".$date_to."'
       ORDER BY `date_create` DESC";
       return $this->db->getAll($q); */

    }

    public function GetInfoClientAccounts($id_user,$type_accounts)    // получение баланса клиента по всем аккаунтам определенного типа
    {
       $q="SELECT * FROM accounts WHERE id_user='".(int)$id_user."' AND type_accounts='".(int)$type_accounts."'";
       return $this->db->getAll($q);

    }

    public function GetTurnoverValue($type=1)
    {
       if ($type==1) $type=3; else $type=4;
       $q="SELECT SUM(t1.value) FROM accounts_history t1, accounts t2 WHERE t1.type_events='4' AND t1.id_accounts=t2.id AND t2.type_accounts='$type' LIMIT 1";
       return $this->db->getOne($q);

      /* $q = "SELECT * FROM statistics_day WHERE type_bonds='$type_bonds' AND is_demo=0  ORDER BY id DESC LIMIT 1";
//echo $q;
      $row=$this->db->getRow($q);  */

    }

    public function GetOpenOrClosePositionFond($date_from,$date_to,$table='bonds_position_open',$where='1=1', $real=0)    // открытые или закрытые позиции по всем аккаунтам для фонда
    {   if ($real==0) $str='t1.is_demo=0 AND';
      $q = "SELECT t1.*, t2.title FROM {$table} t1, bonds_name t2  WHERE t1.type_bonds=t2.id AND $str t1.vis>0 AND t1.date_create>='".$date_from."' AND t1.date_create<='".$date_to."' AND t1.bonds_count>0 AND ".$where." ORDER by t1.date_create DESC";
      return  $this->db->getAll($q);
    }

    public function GetNamesCertificate($id_user=0, $type_bonds=0)          //получение списка сертефикатов для определенного пользователя
    {

      $q = "SELECT t1.*, t2.full_name FROM bonds_position_open t1, bonds t2  WHERE t2.id_position=t1.id AND t1.vis>0 AND t1.bonds_count>0 AND t1.is_demo=0 AND t1.id_user='".(int)$id_user."' AND t1.type_bonds='".(int)$type_bonds."'";


      return  $this->db->getAll($q);
    }
    public function GetNamesCertificateAll($type_bonds,$where)          // получение списка сертефикатов на балансе фонда
    {
      $q = "SELECT * FROM `bonds` WHERE id_bonds_name='".$type_bonds."' AND ".$where."";
      return  $this->db->getAll($q);
    }

    public function GetCountBonds($id_user,$type_bonds) // количество сертефикатов у клиента из open_position
    {             
        $q="SELECT SUM(bonds_count) FROM bonds_position_open WHERE id_user='".(int)$id_user."' AND vis>0 AND type_bonds='".(int)$type_bonds."' AND is_demo=0";
        return $this->db->getOne($q);
    }

    public function GetCountBondsAll($type_bonds,$where='1=1'){         // количество сертефикатов на балансе фонда
        $q="SELECT COUNT(*) FROM `bonds` WHERE id_bonds_name='".$type_bonds."' AND ".$where."";
        return $this->db->getOne($q);
    }

    public function ReturnBondsFund($open,$id_user,$type_bonds,$count){       // в случайных бондов кол-во равных проданным, обнуляем владельца и позицию.

        $q="UPDATE bonds SET id_position=0, id_user=0 WHERE id_position=".$open." AND id_user='".$id_user."' AND id_bonds_name='".$type_bonds."' ORDER by id LIMIT ".$count."";
        $this->db->query($q);    // в случайных бондов кол-во равных проданным, обнуляем владельца и позицию.
    }

    public function CreateBonds($open_position,$id_user,$type_bonds,$count){ // Занимаем случайные бонды в базе

          $q="UPDATE bonds SET id_position='".$open_position."', id_user='".$id_user."' WHERE id_user=0 AND id_bonds_name='".$type_bonds."' ORDER by id LIMIT ".$count."";
          $this->db->query($q);
    }

    public function GetBalanceFund($prev_day,$type='1'){ //  баланса фонда (входящий остаток)    1374215598

          $q="SELECT * FROM `accounts_history` WHERE id_accounts='".$type."' AND date_create<='".$prev_day."' ORDER by date_create DESC LIMIT 1";
          $row = $this->db->getRow($q);
          return $row['account_value'];
    }


    public function GetAmountMoney($id_type=3,$where='1=1')     // операции пополнения баланса счета, -( история пополнение счета клиента )
    {

        $q="SELECT t1.*,t2.full_account FROM `accounts_history` t1,`accounts` t2 WHERE t2.type_accounts='".$id_type."' AND t1.account_user=t2.id AND t1.`id_accounts` =2 AND t1.type_events =3 AND t1.`account_user` >2 AND ".$where." ORDER BY t1.`date_create` DESC";
        return $this->db->getAll($q);

    }

    public function test($param) // тестовая функция - првоерял соединение по json
    {
        $var=isset($param[0]['var'])?$param[0]['var']:0;
        
        $result=array('row'=>  $param[0]['test']);
        if ($param[0]['json']==1)  {
            $result =  json_encode( $result );
            echo "jsonpCallback(".$result.")";
        } else return  $result;
    
    }
}
?>