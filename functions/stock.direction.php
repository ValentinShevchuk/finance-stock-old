<? 
class Direction extends Bond{
  
    function __construct()
    {
        $this->account=New Account();
        $this->stats=New Stats();
        
        parent::__construct();
        
    }
    
public function GetDirection($param=array()) // получаем направление для бонда
{
    if ($param[0]['json']==1) $param=$param[0];
    
    $bond=isset($param['bond'])?"AND type_bonds='{$param['bond']}'":"";
    
    $q="SELECT * FROM bond_direction WHERE date_min<='".time()."' $bond AND vis>0 ORDER BY date_min DESC, id DESC LIMIT 1";
    //echo $q;
    if (isset($param['bond'])) $row=$this->getRow($q); 
    else $row=$this->getAll($q);

    if ($param['json']==1)  {
        $result =  json_encode($row);
        echo "jsonpCallback(".$result.")";
    } else return  $row;    
}

}?>