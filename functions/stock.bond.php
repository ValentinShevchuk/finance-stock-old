<? 
class Bond extends database {
/**
ВНИМАНИЕ!
Все действия продажа и покупка идут от лица биржи. Биржа покупает у клиента это событие продажи клиентом. Все функции начинаются на Buy
Если биржа продает, это действие покупки клиентом все функции начинаются с Sell    
	
	
***/    
		
	public $account;
	public $stats;
	
	function __construct()
	{
		$this->account=New Account();
		$this->stats=New Stats();
		
		parent::__construct();
		
	}
	public function GetPriceForChart($param=array()) // получение списка цен для линеных графиков
	{
		$param['json']=(int)$param['json'];   
		if (isset($param['id_bond_name'])) { $id_bond_name=$param['id_bond_name'];  $where="AND t2.id='{$id_bond_name}' ";}
		if (isset($param['id_project'])) { $id_project=$param['id_project']; $where.="AND t2.id_project='{$id_project}' "; }
				
		if (isset($param['limit'])) $limit=$param['limit']; else $limit=10;
		 
		$q="SELECT t1.price FROM bond_amount t1, bond_name t2 WHERE t2.id=t1.type_bonds $where ORDER by t1.id DESC LIMIT $limit";
		//echo $q;
		$row=$this->getAll($q);
		if (count($row)>0){
			$price='';
			foreach ($row as $row) $price=$row['price'].','.$price;
			$res['price']=trim($price,',');
			$res['error']=0;
		} else $res=array('error'=>1,'error_msg'=>'Нет цены');
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;            
		
	}
	public function SetBeneficiaryBondToMarket($param=array()) // устаниваливаем бенефициара на пустых акциях, сниманием бенефициара
	{
		/*
			Вводные данные
			id_bond_name - id акции
			id_beneficiary - id беника
			count_bond = 
		*/
		$param['json']=(int)$param['json'];
			
		$id_bond_name=(int)$param['id_bond_name'];
		$id_beneficiary=(int)$param['id_beneficiary'];
		$count_bond=(int)$param['count_bond'];   
		
		$param['set']=(int)$param['set']; 
		
		$id_open_position=(int)$param['id_open_position'];
		$arr=array('vis'=>2, 'id_project_beneficiary'=>$id_beneficiary );
		// стоит set=1 то записываем пустые акции владельца id_beneficiary
		if ($param['set']==1){ 
			// если указана открытая позиция, то добавляем условие
			if ($id_open_position>0) { 
				$WHERE=" AND id_position='{$id_open_position}'";
				$arr['id_position']=0;
			} else { $WHERE=" AND id_position=0 "; $SET=""; }
			$this->update('bond',$arr,"WHERE id_bond_name='{$id_bond_name}' AND id_project_beneficiary=0 $WHERE LIMIT $count_bond");
		
		}
		// если set 0 - то беник сбрасывается в ноль
		if ($param['set']==0) {
			$q="UPDATE bond SET vis=1, id_project_beneficiary=0 WHERE id_bond_name='{$id_bond_name}' AND id_project_beneficiary='{$id_beneficiary}' LIMIT $count_bond ";
			echo $q;
			$this->update('bond',array('vis'=>1, 'id_project_beneficiary'=>0 ),"WHERE id_bond_name='{$id_bond_name}' AND id_project_beneficiary='{$id_beneficiary}' LIMIT $count_bond");
		}        
		//тут надо sql запрос 
		
		$res['error']=0;
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;            
		
	}

	public function SetBeneficiaryBond($param=array())
	{ // занимаем пустые акции и ставим бенефициара во владельцы
		
		if ($param[0]['json']==1) $param=$param[0];      
		if (isset($param['bond_name'])) $bond_name=$param['bond_name'];
		if (isset($param['id_beneficiary'])) $id_beneficiary=$param['id_beneficiary'];
		if (isset($param['bond_count']))  $bond_count=$param['bond_count']; else $count_bond=1;     
		 
		$this->update('bond',array('id_project_beneficiary'=>$id_beneficiary),"WHERE id_bond_name='{$bond_name}' AND id_project_beneficiary=0 LIMIT $count_bond");
		
		
		$res['error']=0;
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;            
		
	}
		
	
	public function CreateBond($param=array())
	{
		// создание номеров акции, если компания выпускает миллион акций, нужно выпустить млн записей в таблицу
		
		//$count,$id_bonds_name,$id_user=0,$demo=0
		
		if ($param[0]['json']==1) $param=$param[0];      
		//print_r($param); exit;
		$count=isset($param['count'])?$param['count']:2; // кол-во выпускаемых акций, по умолчанию 2
		$id_bond_name=isset($param['id_bond_name'])?$param['id_bond_name']:0;
		
		if ($id_bond_name>0){
		$part=100;   // делим записи на запросы, чтобы mysql пропустил, 100 среднее, если будет большая нагрузка можно уменьшить
		$limit=$part;
			for($i=1;$i<=$count;$i++){           
				
				$full_account=$this->account->CreateNumber(array('number'=>$i,'id_bond_name'=>$id_bond_name));
				if ($limit==$i || $i=='1'){ 
					if ($i>1) { $this->query($q); }
					
					$q='INSERT INTO bond (full_name,id_bond_name) VALUES'; 
					
					
					$q.="('$full_account','$id_bond_name')";
					//echo "<br> i="."$i".$q;

					$limit=+$part;
					}
				else {  
				
				$q.=",('$full_account','$id_bond_name')";

				}
		
		  if ($i==$count) $this->query($q);
				
			}
		
		
		
		//$short_name_bonds=$this->db->getOne("SELECT short_name FROM bonds_name WHERE id=$id_bonds_name");
/*
		$q="SELECT * FROM bonds WHERE full_name='' AND id_bonds_name=$id_bonds_name";
		echo $q;
		$row=$this->getAll($q);

		foreach ($row as $row)
		{
			$full_account=$this->account->CreateNumber($row[id],$id_bonds_name);
			$q="UPDATE bonds SET full_name='$full_account' WHERE id={$row[id]}";
			$this->query($q);
		}
*/    
			$row['error']=0;    
		} 
		else { $row=array('error'=>1,'error_msg'=>'Параметры переданы не полно');}
		
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
		
	}   
	/*
	 public function CreateBond($param=array())
	{
		// создание номеров акции, если компания выпускает миллион акций, нужно выпустить млн записей в таблицу
		
		//$count,$id_bonds_name,$id_user=0,$demo=0
		
		if ($param[0]['json']==1) $param=$param[0];      
		//print_r($param); exit;
		$count=isset($param['count'])?$param['count']:2; // кол-во выпускаемых акций, по умолчанию 2
		$id_bond_name=isset($param['id_bond_name'])?$param['id_bond_name']:0;
		
		if ($id_bond_name>0){
		$part=100;   // делим записи на запросы, чтобы mysql пропустил, 100 среднее, если будет большая нагрузка можно уменьшить
		$limit=$part;
			for($i=1;$i<=$count;$i++){           
				
				$full_account=$this->account->CreateNumber(array('number'=>$i,'id_bond_name'=>$id_bond_name));
				if ($limit==$i || $i=='1'){ 
					if ($i>1) $this->query($q);
					$q='INSERT INTO bonds (full_name,id_bonds_name) VALUES'; 
					
					
					$q.="('$full_account','$id_bond_name')";
					//echo "<br> i="."$i".$q;

					$limit=+$part;
					}
				else {  
				
				$q.=",('$full_account','$id_bond_name')";

				}
		
		  if ($i==$count) $this->query($q);
				
			}
		
		//$short_name_bonds=$this->db->getOne("SELECT short_name FROM bonds_name WHERE id=$id_bonds_name");
 
			$row['error']=0;    
		} 
		else { $row=array('error'=>1,'error_msg'=>'Параметры переданы не полно');}
		
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
		
	}
	
	public function CreateBond__($open_position,$id_user,$type_bonds,$count){ // Занимаем случайные бонды в базе

		  $q="UPDATE bonds SET id_position='".$open_position."', id_user='".$id_user."' WHERE id_user=0 AND id_bonds_name='".$type_bonds."' ORDER by id LIMIT ".$count."";
		  $this->db->query($q);
	}
*/    
	public function CreateBondName($param=array()) // создаем имя акции в таблице bond name
	{
		
		if ($param[0]['json']==1) $param=$param[0];
				
		$id=$this->insertdata('bond_name',$param['post']);
		
		if ($id>0)  $row=array('new_id'=>$id, 'error'=>0);  
		else $row=array('error'=>1,'error_msg'=>'Произошла непредвиденная ошибка');
		
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}   
	
	public function GetBondNameList($p=array())     // выгрузка списка акций bond_name таблицы 
	{
		$p['json']=(int)$p['json'];
		$p['id_project']=(int)$p['id_project'];
		
		if (!isset($p['vis'])) $p['vis']=1; else $p['vis']=(int)$p['vis']; 
		$where=" AND t1.vis='{$p['vis']}'";
		
		if ($p['id_project']>0) $where.=" AND t1.id_project='{$p['id_project']}'";
		
		$q="SELECT * FROM bond_name t1 WHERE t1.id>0 $where";
		//echo $q;
		$row=$this->getAll($q);
		if (count($row)>0){
			foreach ($row as $item=>$key)
			{
				if ($row[$item]['vis']=='2') $row[$item]['vis_text']='Новый';
				if ($row[$item]['vis']=='1') $row[$item]['vis_text']='Утверждено';
				if ($row[$item]['vis']=='0') $row[$item]['vis_text']='Удалено';
				$price=$this->GetPriceForChart(array('limit'=>20,'id_bond_name'=>$row[$item]['id']));
				//print_r($price);
				if ($price['error']=='0') $row[$item]['price']=$price['price']; else $row[$item]['price']=$price['error_msg'];
			}
			$res=array('error'=>0, 'row'=>$row);
  
		} else{
			$res=array('error'=>1, 'error_msg'=>'Нет записей бондов');
		}
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	
	public function NewPrice($param=array()) // создаем новую цену для акции 
	{

		if ($param[0]['json']==1) $param=$param[0];
		$bond=isset($param['bond'])?$param['bond']:1;
		$value=isset($param['value'])?$param['value']:1;
		
		$direction=$this->GetDirection(array('bond'=>$bond));
		print_r($direction);
		$direction['price_min']=isset($direction['price_min'])?$direction['price_min']: $value-100;
		$direction['price_max']=isset($direction['price_max'])?$direction['price_max']: $value+100;
		$direction['percent_buy']=isset($direction['percent_buy'])?$direction['percent_buy']: 1;
		$direction['percent_sell']=isset($direction['percent_sell'])?$direction['percent_sell']: 1;
		
		$step=isset($direction['step'])?$direction['step']: 10;
	
//---------
		$random=mt_rand((0-$step*100),($step*100))/100;
//--------
 /*    if ( ($value+$random >=$direction['price_min']) || ($value+$random <= $direction['price_max']))  {     
$random=$random/10;
 } else {

 }

  */  
		if ($random<0)  {  if ($value+$random >=$direction['price_min'])  $value=$value+$random; else $value=$value-($random); }
		if ($random>0)  {  if ($value+$random <= $direction['price_max'])  $value=$value+($random); else $value=$value-$random; }


		
		
		$value_buy=$value-round($value*($direction['percent_buy']/100),5);
		$value_sell=$value+round($value*($direction['percent_sell']/100),5);


		if (isset($direction['id'])){
			
			$q="UPDATE bond_direction SET vis=1 WHERE type_bonds='$bond' AND vis=2";
			$this->query($q);
		
			$q="UPDATE bond_direction SET vis=2 WHERE id={$direction['id']}";
			$this->query($q);
		}
		
		$array=array(
			'value'=>$value,
			'value_buy'=>$value_buy,
			'value_sell'=>$value_sell,
			'id_direction'=>$direction['id']
		);
	
		if ($param['json']==1)  {
			$result =  json_encode($array);
			echo "jsonpCallback(".$result.")";
		} else return  $array;    
	}
	
	public function GetBondInfo($param=array())
	{
		if ($param[0]['json']==1) $param=$param[0];
		if (isset($param['type_bond'])){ $type_bond=$param['type_bond']; $where=" AND t1.id={$type_bond} ";} else $type_bond=0;
	   
		$q="
				SELECT t1.*,par_now as price ,
				par_now_buy as price_buy,
				par_now_sell as price_sell
				FROM bond_name t1 WHERE vis=1 $where ";
		// echo $q;      exit;
		
		// есле передан ID то не зачем грузить двойной массив, достаточно Row
		if ($type_bond>0) $row=$this->getRow($q);   
		else  $row=$this->getAll($q);
		if (count($row)>0){
			$res['row']=$row;
			$res['error']=0;
		} else $res=array('error'=>1, 'error_msg'=>'Нет информации');
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	public function PriceAll($param=array()) //список акций и котировки последние 1000 значений, возможно уменшить до 100. Переделать запрос, чтобы грузил, последние 100 с минутами, 100 с 5 минутами
	{    
		if ($param[0]['json']==1) $param=$param[0];
		
		$type_bonds=isset($param['type_bonds'])?$param['type_bonds']:1;
		//$is_demo=isset($param['is_demo'])?$param[0]['is_demo']:0;
		
		$q="SELECT * FROM bond_amount WHERE type_bonds='{$type_bonds}'  ORDER by id DESC LIMIT 1000";
		//echo $q;
		$row=$this->getall($q);
		$last_id=0;
		foreach ($row as $item=>$key)
		{
			if ($last_id==0)$last_id=$row[$item]['id'];
			$row[$item]['date_create_new_format']=date("Y/m/d H:i:s", strtotime($row[$item]['date_create'])); // FORMAT FOR SAFARI
			$row[$item]['date_create_unix']=strtotime($row[$item]['date_create']);           
			
			$row[$item]['date_create_full_format']=date("D M d Y H:i:s GMT", strtotime($row[$item]['date_create']));
		}    
		$res=array('row'=>$row, 'last_id'=>$last_id);
		
		if ($param['json']==1)  {
			//array('row'=>$row)
			$result =  json_encode( $res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	
	}

	
	public function OrderSell($param=array()) // заявка на ПРОДАЖА клиенту, проверка команды, наличие средств и т.д.
	{
		$param['json']=(int)$param['json'];
		$temp_pay_pass=(int)$param['temp_pay_pass'];
		if (md5($temp_pay_pass)==$_SESSION['temp_pay_pass']){     
		
			if (!isset($param['type_bond']))$param['type_bond']=6; // ID акции по умолчанию 6
			$price=$this->GetBondInfo(array('type_bond'=>(int)$param['type_bond']));
		   // print_r($price); 
	
			$price_real=$price['row']['price'];
			$price_sell=$price['row']['price_sell']; // цена для продажи
			$type_account=$_SESSION['site_user_account_active']; // активный счет ID record
	
			$count=$param['count'];
			$total=$price_sell*$count;
			$res=$this->account->GetAccountInfo(array((id)=>$type_account)); // информация по счету
			
			if ($res['error']==0){ 
				$account=$res['row']['id'];   
				if ($res['row']['value']>$total){             
					$result=$this->Buy($count, $param['type_bond'],$price_sell, $total, $_SESSION['site_user'],$res['row']['id'],$res['row']['type_account'],$price_real);
					if ($result['error']==0){
						$res['error']=0;
						$res['msg']='Акции успешно преобретены';
					}
					else {
						$res['error']=1;
						$res['error_msg']=$result['error_msg'];
					}
				}
				else { 
					$res['error']=1; 
					$res['error_msg']='Недостаточно средств';
				}
	  
			}  
		} else {
			$res=array((error)=>1, (error_msg)=>'Платежный пароль неверный!');
		}
		  
		if ($param['json']==1)  {
			//array('row'=>$row)
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}    

	public function Buy($count, $type_bonds, $price, $total, $id_user,$account, $type_accounts,$real_price)
	// покупка клиентом (Продажа биржой)
	{
		if($_SESSION['site_user_account_active']==$_SESSION['site_user_account_demo'])   $q = "SELECT COUNT(id) id FROM bond WHERE demo_id_user=0  AND id_bond_name=$type_bonds";
		else $q = "SELECT COUNT(id) id FROM bond WHERE id_user=0 AND id_bond_name=$type_bonds AND vis=1";//echo $q;exit;
		$bond_free=$this->db->GetOne($q);
	   //echo $q; exit;
		if ($bond_free>=$count)
	
		{  $is_demo=0;
	
		if ($type_accounts==1) $is_demo=1;
	
	
		$type_bonds=(int)$type_bonds;
		$q="SELECT title FROM bond_name WHERE id={$type_bonds}";

		$title_bonds = $this->db->getOne($q);
	

		$this->account->DecrementAccountUser( array('type_transfer'=>4,'value'=>$total, 'id_account'=>$account,'trans_descr'=>'Покупка (&laquo;'.$title_bonds.'&raquo;)'));
		
		// списываем сумму бондов со счета
		// Добавляем в историю, что списали.
	
	
		$real_price_total=$real_price*$count;
		// создаем открытую позицию с кол-вом
		$open_postion=$this->CreateOpenPosition($count,$type_bonds, $price,$total,$id_user,$is_demo,$real_price,$real_price_total);
		$number_transaction = $open_postion;
		
		if ($is_demo==0) {
			
		   $arr=array(
			   'id_user' => 0,
			   'id_account' => 1, 
			   'type_transfer' => 1,
			   'method_transfer' => 0,
				'purpose' => 0,
				'value' => $total,
				'reference' =>"Продажа бондов (&laquo;{$title_bonds}&raquo;) клиенту"                   
			);
			$res=$this->account->CreateTransaction(array('POST'=>$arr, 'id'=>$_GET['id'], 'vis'=>1));                

			$this->account->IncrementAccountUser(array('type_transfer'=>5,'value'=>$total, 'id_account'=>1,'type_events'=>3, 'trans_descr'=>"Продажа бондов (&laquo;{$title_bonds}&raquo;) клиенту",'account_user'=>$account));// уменьшаем деньги у биржи
		}
	
		  // Занимаем случайные бонды в базе
		if($_SESSION['site_user_account_active']==$_SESSION['site_user_account_demo'])
			$q="UPDATE bond SET demo_id_position='{$open_postion}', demo_id_user={$id_user} WHERE demo_id_user=0 AND id_bond_name='$type_bonds'  ORDER by id LIMIT $count";
		else 
			$q="UPDATE bond SET id_position='{$open_postion}', id_user={$id_user} WHERE id_user=0 AND id_bond_name='$type_bonds' AND vis=1 ORDER by id LIMIT $count";
	//echo $q;
		  $this->query($q);
	
	
		  // Добавляем в бонды метку, к какой позиции они относятся
	
		  return array('error'=>0,'msg'=>'Акции успешно преобретены');
	   }
	   else
	   {
	
		return array('error'=>1,'error_msg'=>'Нет доступных акций');
	   }
	}
		
	public function OrderBuy($param=array()) // Покупка у клиента, проверка команды, наличие средств и т.д.
	{
		$param['json']=(int)$param['json'];
		$open_position=$param['open'];
		$temp_pay_pass=(int)$param['temp_pay_pass'];
		
		if (md5($temp_pay_pass)==$_SESSION['temp_pay_pass']){
			$row=$this->GetOpenPositionInfo(array('open'=> $open_position, 'id_user'=>$_SESSION['site_user']));
			//print_r($row); exit;
			$type_bond=$row['row']['type_bonds'];//$param['type_bond']; // ID акции 
			$price=$this->GetBondInfo(array('type_bond'=>$type_bond));
			
			$price_real=$price['row']['price'];
			$price_buy=$price['row']['price_buy']; // цена для продажи
			$type_account=$_SESSION['site_user_account_active']; // активный счет ID record
			$count=$param['count'];
			$total=$price_buy*$count;
			$res=$this->account->GetAccountInfo(array((id)=>$type_account)); // информация по счету
		
			if ($res['error']==0){    
			   $row=$this->sell($count, $type_bond,$price_buy, $total, $_SESSION['site_user'], $_SESSION['site_user_account_active'],$open_position, $res['row']['type_account'],$price_real);
				$res=$row;
			}    
		} else {
			$res=array((error)=>1, (error_msg)=>'Платежный пароль неверный!');
		}
		  
		if ($param['json']==1)  {
			//array('row'=>$row)
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}    
	public function Sell($count, $type_bonds, $price, $total, $id_user,$account,$open, $type_accounts,$real_price)
	{
	   //$account_info=$this->AccountsGetValue($id_user,$account);
		$row=$this->GetOpenPositionInfo(array('open'=>$open,'id_user'=>$id_user));
		$open_position_info=$row['row'];        
		
		if ($open_position_info['bonds_count']>=$count) {   
			$is_demo=0;
			if ($type_accounts==1) $is_demo=1;
			
		   // echo $is_demo;
			
			$type_bonds=(int)$type_bonds;
			$q="SELECT title FROM bond_name WHERE id={$type_bonds}";
			$title_bonds = $this->db->getOne($q);
  
			//array('value'=>$total, 'id_account'=>$account,'type_events'=>3, 'trans_descr'=>"Продажа (&laquo;'.$title_bonds.'&raquo;)",'account_user'=>$account)
			
			$row=$this->account->IncrementAccountUser( array('type_transfer'=>5, 'value'=>$total, 'id_account'=>$account,'type_events'=>3, 'trans_descr'=>"Продажа (&laquo;'.$title_bonds.'&raquo;)"));  // пополняем счет

			$this->DecrementOpenPosition(array('open'=>$open, 'count'=>$count));   // уменьшаем кол-во купленных бондов
	   
			if($_SESSION['site_user_account_active']==$_SESSION['site_user_account_demo']){   
				// в случайных бондов кол-во равных проданным, обнуляем владельца и позицию.    
				$q="UPDATE bond SET demo_id_position=0, demo_id_user=0 WHERE demo_id_position='$open' AND demo_id_user='$id_user' AND id_bond_name='$type_bonds' ORDER by id LIMIT $count";       
			} 
			else  
			{ 
				// в случайных бондов кол-во равных проданным, обнуляем владельца и позицию.
				$q="UPDATE bond SET id_position=0, id_user=0 WHERE id_position=$open AND id_user='$id_user' AND id_bond_name='$type_bonds' ORDER by id LIMIT $count"; 
			}
		   //echo $q;

 //exit;    
			$this->query($q);    
	
	
			$real_price_total=$real_price*$count; // на продажу по цене биржи без процентов
			$number_transaction = $this->CreateClosePosition($count,$type_bonds,$open_position_info['price'],$open_position_info['price_real'], $price,$total,$id_user, $open,$is_demo,$real_price,$real_price_total); // создаем закрытую позицию
	//print_r( $number_transaction);
		   /*-----------Расчет действий с балансом биржи---------------*/
		   /*if ($is_demo==0){
			 $profit = $total - ($open_position_info['price']*$count);
			 if ($profit > 0){
				$this->IncrementAccountUser(abs($profit),1,3,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Убыток при закрытии сделки на биржевом рынке',$account,$number_transaction='0');
			 }else{
				$this->DecrementAccountUser(abs($profit),1,1,$id_manager=0, $trans_number=0, $trans_type=0, $trans_descr='Прибыль при закрытии сделки на биржевом рынке',$account,$number_transaction='0');
			 }
		   }*/
	//array('value'=>$total, 'id_account'=>1,'trans_descr'=>'Покупка бондов (&laquo;'.$title_bonds.'&raquo;) у клиента','account_user'=>$account_user)
		   if ($is_demo==0) {
				$arr=array(
				   'id_user' => 0,
				   'id_account' => 1, 
				   'type_transfer' => 2,
				   'method_transfer' => 0,
					'purpose' => 0,
					'value' => $total,
					'reference' => "Покупка бондов (&laquo;{$title_bonds}&raquo;) у клиента"                   
				);
				$res=$this->account->CreateTransaction(array('POST'=>$arr, 'id'=>$_GET['id'], 'vis'=>1));                
				
				$this->account->DecrementAccountUser(array('type_transfer'=>4, 'value'=>$total, 'id_account'=>1,'trans_descr'=>'Покупка бондов (&laquo;'.$title_bonds.'&raquo;) у клиента','account_user'=>$account_user));
			}// уменьшаем деньги у биржи
	
 
			return array('error'=>0,'msg'=>"Вы успешно продали: $count шт. за $price руб.");
		  // Добавляем в бонды метку, к какой позиции они относятся
		  } 
		else   return array('error'=>1,'error_msg'=>'Ошибка! Нехватает бондов!');
	
	

	}    
	public function Price($param=array() )// фукнция по получения цены по JSON по новому формату и все статистики по по Акции функция GetPriceLastList
	{
		
		$type_bonds=isset($param['type_bonds'])?$param['type_bonds']:1;
		$is_demo=isset($param['is_demo'])?$param['is_demo']:0;
			
		   $this->type_bonds=$type_bonds;
	
		   $this->SetWhereTime($type_bonds);
	
		   $q = "SELECT price FROM bond_amount WHERE type_bonds='$type_bonds' AND date_create>='".date("Y-m-d 10:00")."' ORDER BY id LIMIT 1";
		   //echo $q;
		   $PriceBegin=$this->getOne($q);
		  //$PriceBegin=$q;
	
		   $q = "SELECT MAX(price) as price_max, MIN(price) as price_min FROM bond_amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC";  //echo $q;
		   $row=$this->getRow($q);
		   $priceMax=$row['price_min'];  $priceMin=$row['price_max'];
	
	
		  $q = "SELECT price, price_buy, price_sell, date_create FROM bond_amount WHERE type_bonds='$type_bonds'  {$this->date_create} ORDER BY id DESC LIMIT 1";
	//echo $q;
		  $row=$this->getRow($q);
	
				   $PriceLast=$row['price'];
		 // $PriceLast=$q;
		  $difference=$PriceLast-$PriceBegin;
		  $differencePercent=round(($difference*100)/$PriceBegin,4);
		  $difference=round($difference,4);
	
		  if ($difference>=0) $rost=1; else $rost=0;
	
		  $array1=array(
			'PriceBegin'=>$PriceBegin,
			'PriceLast'=>$PriceLast,
			'rost'=>$rost,
			'difference'=>$difference,
			'differencePercent'=>$differencePercent,
			'price'=>$row['price'],
			'price_buy'=>$row['price_buy'],
			'price_sell'=>$row['price_sell'],
			'PriceMax'=>$priceMax,
			'PriceMin'=>$priceMin,
			'DateTimeNow'=>date("Y-m-d H:i"),
			'date_create'=>$row['date_create'],
			'date_create_new_format'=>date("Y/m/d H:i:s", strtotime($row['date_create']))
			
			);
											// AND date_create='".strtotime(date("Y-m-d"))."'
		  $q = "SELECT * FROM bond_statistics_day WHERE type_bonds='$type_bonds' AND is_demo=$is_demo  ORDER BY id DESC LIMIT 1";
	//echo $q;
		  $row=$this->getRow($q);
		 // print_r($row);
		  if (count($row)>0) $array1+=$row;

	
	
		if ($param['json']==1)  {
			$result =  json_encode( $array1 );
			echo "jsonpCallback(".$result.")";
		} else return  $array1;
		}    
	function SetWhereTime($type_bonds) {
	
	  $h=date("H",time())+0;
	
		   $this->timeStart=strtotime(date("Y-m-d 10:00")); //$this->timeStart=date("Y-m-d 10:00:00");
	
	  $this->date_create=" AND date_create>'".$this->timeStart."'";
	
	  $q="SELECT  date_create FROM bond_amount WHERE type_bonds='$type_bonds' {$this->date_create} LIMIT 1";
	  $timeStart=$this->db->getOne($q);
	
	  if ($timeStart>0) {
	
	  } else {
	   $q="SELECT date_create FROM bond_amount WHERE type_bonds='$type_bonds' ORDER by id DESC LIMIT 1";
	
			 //$this->timeStart=$this->db->getOne($q)-43200; //
			  $this->timeStart=date("Y-m-d H:i:s",strtotime($this->db->getOne($q))-43200);
	   $this->date_create=" AND date_create>'".$this->timeStart."'";
	
	 }
	
	
	
	 $this->type_bonds=$type_bonds;
	
	
	}
	public function CreateClosePosition($count,$type_bonds, $price_buy,$price_buy_real, $price_sell,$price_sell_total, $id_user,$open,$is_demo,$price_real,$price_real_total)
	{
	
	
		$inData = array(
			'id_user'=>$id_user,
			'price_buy'=>$price_buy,
			'price_buy_real'=>$price_buy_real,
			'price_sell'=>$price_sell,
			'price_sell_total'=>$price_sell_total,
			'price_real'=>$price_real,
			'price_real_total'=>$price_real_total,
			'type_bonds'=>$type_bonds,
			'bonds_count'=>$count,
			'id_open'=>$open,
			'is_demo'=>$is_demo);
	
		return $this->insertdata('bond_position_close',$inData);
	}
	
	public function CreateOpenPosition($count,$type_bonds, $price, $total,$id_user,$is_demo,$price_real,$price_real_total)
	{
		
		
		$inData = array(
			'id_user'=>$id_user,
			'price'=>$price,
			'total'=>$total,
			'type_bonds'=>$type_bonds,
			'bonds_count'=>$count,
			'price_real'=>$price_real,
			'price_real_total'=>$price_real_total,
			'is_demo'=>$is_demo);
			
	
		return $this->insertdata('bond_position_open',$inData);
	}
	public function GetOpenPositionList($param=array()) // count and price старые параметры $id_user=0, $type_bonds=0, $active_account=0
	{
		if ($param[0]['json']==1) $param=$param[0];

		// для получения полного списка open position без градации demo 
		$is_demo=0; 
		
		if ($_SESSION['site_user_account_active']==$_SESSION['site_user_account_demo']) {$is_demo=1;}    
		if (isset($param['is_demo'])) $is_demo=$param['is_demo'];
		
		
		$where.=" AND t1.is_demo='$is_demo'";   
		if (isset($param['id_user'])) 
		{ 
			if ($param['id_user']=='session_user') $param['id_user']=$_SESSION['site_user']; 
			$where.="  AND t1.id_user='{$param['id_user']}'";  
		}
		
		if (isset($param['id_bond_name'])) {  
		$where.=" AND t1.type_bonds='{$param['id_bond_name']}'";}
		
		$q = "SELECT t1.*, t2.title, CONCAT(t3.surname,' ',t3.name,' ',t3.email) as fio  FROM bond_position_open t1, bond_name t2, client_profile t3  WHERE t3.id=t1.id_user AND t1.type_bonds=t2.id AND t1.vis>0 AND t1.bonds_count>0 $where";
		//echo $q; 
		$row=$this->getAll($q);
		$res=array('error'=>0,'row'=>$row, 'count'=>count($row));
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}    
	public function GetOpenPositionInfo($param=array()) // count and price старые параметры $id_user=0, $type_bonds=0, $active_account=0
	{
		if ($param[0]['json']==1) $param=$param[0];
		if (!isset($param['open'])) { }
		if (!isset($param['id_user'])) { }
		$q="SELECT * FROM bond_position_open WHERE id='{$param['open']}' AND id_user='{$param['id_user']}'";
		$row=$this->getRow($q);
		$res=array('error'=>0,'row'=>$row);
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}    
	
	public function DecrementOpenPosition($param=array())
	{//$open, $count
		if ($param[0]['json']==1) $param=$param[0];
		
		$q="SELECT bonds_count FROM bond_position_open WHERE id='{$param['open']}'";
	   
		$bonds_count=$this->getOne($q);
		if ($bonds_count-$param['count']>=0){
			$q="UPDATE bond_position_open SET bonds_count=bonds_count-{$param['count']}, total=(bonds_count*price) WHERE id='{$param['open']}' AND bonds_count>0";
			$this->query($q);
			$res=array('error'=>0);
		}
		else { $res=array('error'=>1, 'error_msg'=>'Нет доступных акций в позиции');}
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;  
		
	}

	public function GetClosePositionList($param=array()) // count and price старые параметры $id_user=0, $type_bonds=0, $active_account=0
	{
		if ($param[0]['json']==1) $param=$param[0];
		
		// для получения полного списка open position без градации demo 
		$is_demo=0; 
		if ($_SESSION['site_user_account_active']==$_SESSION['site_user_account_demo']) {$is_demo=1;}    
		if (isset($param['is_demo'])) $is_demo=$param['is_demo'];    
		
		$where.=" AND t1.is_demo=$is_demo";   

		if (isset($param['id_user'])) 
		{ 
			if ($param['id_user']=='session_user') $param['id_user']=$_SESSION['site_user']; 
			$where.="  AND t1.id_user='{$param['id_user']}'";  
		}

		$q = "SELECT  *, t1.id, t1.date_create, CONCAT(t3.surname,' ',t3.name,' ',t3.email) as fio   FROM bond_position_close t1, bond_name t2, client_profile t3  WHERE t3.id=t1.id_user AND  t1.type_bonds=t2.id AND t1.vis>0 $where";
		$row=$this->getAll($q);
		$res=array('error'=>0,'row'=>$row);
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}    

	function GetDirection($param=array()) // получаем направление для бонда
	{
		/*
			all - выгрузить все строки - если all передано то грузит GetAll, если нет getRow
			bond - id акции
			id - ид записи направления
		*/
	   // print_r($param);
		$param['json']=(int)$param['json'];
		
		$bond=isset($param['bond'])?" AND type_bonds='{$param['bond']}'":"";
		
		$whereId=" AND date_min<='".date('Y-m-d H:i:s',time())."'";   
		if (isset($param['id'])){ $whereId=" AND id='{$param['id']}'";}
		
		$q="SELECT * FROM bond_direction WHERE  vis>0 $bond  $whereId ORDER BY date_min DESC LIMIT 1";
		
		if (isset($param['all'])) {
			$q="SELECT * FROM bond_direction WHERE vis>0  $bond ORDER BY date_min DESC";
			$row=$this->getAll($q);
			$once=1;
			foreach($row as $item=>$key){
		
				if ($row[$item]['date_min']<=date("Y-m-d H:i:s")) {   if ($once==1)  {  $row[$item]['class_color']='text-primary';   $once=0;}
					else $row[$item]['class_color']='text-muted';}
				else { 
					$row[$item]['class_color']='text-default';
				}               
			}
		}
		else $row=$this->getRow($q); 
		//      echo $q; exit;        //print_r($row);
	
		if ($param['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}


	function CreatePointDirection($p=array())     // создание проекта для утверждения приходит POST
	{
		$p['json']=(int)$p['json'];
		if (isset($p['POST']['date_min'])) $p['POST']['date_min']=date('Y-m-d H:i:s',strtotime($p['POST']['date_min']));
		
		$arr=$p['POST'];
		$arr['id_manager']=$_SESSION['site_user'];

		$id=$this->insertdata('bond_direction',$arr);
		$this->stats->CreateUserEvent(array( 'event'=>'2', 'event_text'=>'Создан', 'id_record'=>$id)); // записываем в лог действие
		$res=array('error'=>0);
		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $res;
	}

	function EditDirection($p=array('json','POST','id'))     // редактирование проектов
	{   
		/*
			
		*/
		
		$p['json']=(int)$p['json'];
		
		$post=$p['POST'];
		if (isset($post['date_min']))    $post['date_min']=date('Y-m-d H:i:s',strtotime($post['date_min']));
   //print_r($post); exit;     
	  
		$this->update('bond_direction',$post," WHERE id=?",array($p['id'])); // записываем саму запись
	
		$this->stats->CreateUserEvent( array( 'event'=>'3', 'event_text'=>'Исправление','post'=>$post)); // записываем в лог действие
		
		if ($p['json']==1)  {
			$result =  json_encode($row);
			echo "jsonpCallback(".$result.")";
		} else return  $row;    
	}

	public function GetNumbersBond($p=array()) // count and price старые параметры $id_user=0, $type_bonds=0, $active_account=0
	{
		$p['json']=(int)$p['json'];
		if (isset($p['id_beneficiary'])) {$p['id_beneficiary']=(int)$p['id_beneficiary']; $where.=" AND id_project_beneficiary='{$p['id_beneficiary']}'";}

		$q="SELECT * FROM bond WHERE id>0 $where LIMIT 100";
		$row=$this->getAll($q);
		$res=array('error'=>0,'row'=>$row);
		
		if ($param['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;     
	}   
}?>