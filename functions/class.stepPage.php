<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Step_Page
 *
 * @author lu <549943405@qq.com>
 */
class Step_Page
{
    //put your code here
    private $total_counts = 0;
    private $total_page_counts;
    private $page_rows = array(10,20,50,100);
    
    private $current_page_rows = 10;    
    private $current_page = 1;
    private $pre_page;
    private $next_page;

    private $current_url;
    private $pre_url;
    private $next_url;
    
    private $url;
    private $testbugs;

    // Initialize the Pagination
    public function  __construct($optional=array(),$testbugs=false) {
        if(!empty($optional)){
            $this->total_counts = isset($optional['total_counts']) && !empty($optional['total_counts'])?
                        intval($optional['total_counts']) : $this->total_counts;
            $this->page_rows = isset($optional['page_rows']) && !empty($optional['page_rows']) ?
                        $optional['page_rows'] : $this->page_rows;
            $this->current_page = isset($optional['current_page']) && !empty($optional['current_page'])?
                        intval($optional['current_page']) : $this->current_page;
            $this->current_page_rows = isset($optional['current_page_rows']) && !empty($optional['current_page_rows'])?
                        intval($optional['current_page_rows']) : $this->current_page_rows;
            $this->url = isset($optional['current_page_rows']) && !empty($optional['current_page_rows'])?
                        $optional['url'] : $this->url;
        }
        $this->testbugs = $testbugs;
        $this->__initalParameters();
        if($this->testbugs){
            $this->__printPageParameters();
        }
    }
    // Initialize the Pagination Parameters
    private function __initalParameters(){

        $this->total_page_counts = ($this->total_counts > 0) ? intval(ceil($this->total_counts / $this->current_page_rows)) : 0;
        
        if($this->current_page < 1 ){
            $this->current_page = 1;
        }else if ($this->current_page > $this->total_page_counts ) {
            $this->current_page = $this->total_page_counts;
        }else{
            $this->current_page = $this->current_page;
        }        

        $this->current_url = $this->url;
        
        $this->pre_page = ($this->total_counts > 0 && $this->current_page > 1) ? $this->current_page - 1: 1 ;
        $this->pre_url = $this->url.'&page='.$this->pre_page.'&page_rows='.$this->current_page_rows;

        $this->next_page = ($this->total_counts > 0 && $this->current_page < $this->total_page_counts) ? $this->current_page + 1: $this->total_page_counts ;
        $this->next_url = $this->url.'&page='.$this->next_page.'&page_rows='.$this->current_page_rows;
    }
    
    // Generate the page html code
    public function getPageHTML(){
       $html = '';
       if($this->total_page_counts > 0){
           $html ='<span class="tx">Show rows:</span>';
           $html.='<select class="current_page_rows" curl="'.$this->current_url.'">';
           foreach($this->page_rows as $row){
               if($row == $this->current_page_rows){
                   $html.='<option value="'.$row.'" selected="selected">'.$row.'</option>';
               }else{
                   $html.='<option value="'.$row.'" >'.$row.'</option>';
               }
           }
           $html.='</select>';
           $html.='<span class="page_num">'.$this->current_page.'/'.$this->total_page_counts.'</span>';
		   
		   $style_pre = ($this->pre_page == $this->current_page) ? 'class="page_pre_noclick"': 'class="page_pre"';
		   $style_next = ($this->next_page == $this->current_page) ? 'class="page_next_noclick"': 'class="page_next"';
           $html.='<a href='.$this->pre_url.' '.$style_pre.'></a>';
           $html.='<a href='.$this->next_url.' '.$style_next.'></a>';
       }
       return $html;
    }
    
	public function getLkPage(){  //for lk page
       $html = '';
       if($this->total_page_counts > 0){
		    $i=1;
		    while ($i<=$this->total_page_counts){
			    $select = ($i==$this->current_page) ? "class='select' " : " ";
		        $html.="<a href='".$this->url."&page=".$i."' ".$select.">".$i."</a>";
			    $i++;
		    };
			$this->pre_page = ($this->total_counts > 0 && $this->current_page > 1) ? $this->current_page - 1: 1 ;
            $this->next_page = ($this->total_counts > 0 && $this->current_page < $this->total_page_counts) ? $this->current_page + 1: $this->total_page_counts;
//			$html.="<a class='forward' href='".$this->url."&page=".$this->pre_page."' style='margin-left:15px'>Backward</a>";
//			$html.="<a class='forward' href='".$this->url."&page=".$this->next_page."'>Forward</a>";
        }
        return $html;
    }

    public function getDnfundPage($pageLink,$nextPage='>>',$prevPage='<<',$separator='?'){  //for lk page
       $html = '';
       if($this->total_page_counts > 0){
            $i=$this->current_page-(round($pageLink/2));
            $i= ($i <= 0) ? 1 : $i ;
            $stop = ($i==1) ? $pageLink : $this->current_page + ((round($pageLink/2)-1));
            if ($pageLink%2 !== 0){
                $i=$this->current_page-(round($pageLink/2)-1);
                $i= ($i <= 0) ? 1 : $i ;
                $stop = ($i==1) ? $pageLink : $this->current_page + ((round($pageLink/2))-1);
            }
            if ($stop >= $this->total_page_counts){
                $i = $this->total_page_counts - $pageLink;
            }
            $this->pre_page = ($this->total_counts > 0 && $this->current_page > 1) ? $this->current_page - 1: 1 ;
            $html.="<a class='forward' href='".$this->url.$separator."page=".$this->pre_page."' style='background:none;margin-top:1px;' title='Prev'>".$prevPage."</a>";
		    while ($i<=$this->total_page_counts){
		        if ($i>0){
			        $select = ($i==$this->current_page) ? "class='select' " : " ";
		            $html.="<a href='".$this->url.$separator."page=".$i."' ".$select.">".$i."</a>";
                }
			    $i++;
                if ($i>$stop) break;
		    };

            $this->next_page = ($this->total_counts > 0 && $this->current_page < $this->total_page_counts) ? $this->current_page + 1: $this->total_page_counts;
    		$html.="<a class='forward' href='".$this->url.$separator."page=".$this->next_page."' style='background:none;margin-top:1px;' title='Next'>".$nextPage."</a>";
        }
        return $html;
    }

    // for test the page
    private function __printPageParameters(){
        $parameters = array(
            'total_counts'=>$this->total_counts,
            'total_page_counts'=>$this->total_page_counts,
            'page_rows'=>$this->page_rows,
            'current_page_rows'=>$this->current_page_rows,
            'current_page'=>$this->current_page,
            'total_counts'=>$this->pre_page,
            'total_counts'=>$this->next_page,
            'total_counts'=>$this->total_counts,
            'url'=>$this->url,
            'pre_url'=>$this->pre_url,
            'next_url'=>$this->next_url,
        );
        var_dump($parameters);
    }
}