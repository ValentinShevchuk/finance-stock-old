<?php
require_once('../include/tcpdf/tcpdf.php');

class pdf extends TCPDF {
	
	private $dataset;
	
	private $path;
	
	private $filename;
	
	private $websiteAbsolutePath;
	
	private $print_html;
	
	private $html;
	
	private $m_;
	
	private $add_;
	
	private $db;
	
	private $filetitle;
	
	private $files_category;
	
	public function __construct($m_='',$add_=''){
		global $db, $_POST, $_GET, $files_category;
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->setPrintHeader(false);
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);            
		$this->setImageScale(1);   //set image scale factor
		$this->setFontSubsetting(true);    // set default font subsetting mode
		
		$this->db = $db;
		$this->post = $_POST;
		$this->get = $_GET;
		$this->websiteAbsolutePath = str_replace("/", "\\", $_SERVER['DOCUMENT_ROOT']);
		$this->files_category = isset($files_category) ? $files_category : '';
		if(empty($this->files_category)){
			echo "Please initialize the Files Path!!";exit;
		}
		$this->m_= trim($m_);
		$this->add_ = trim($add_);
		$this->setFileTitle();
		//echo $m_;
		$this->generatePrintTemplatePNG();
		$this->generatePrintPDF();
	}
		
	public function openBrowserPDF(){
		$this->commonActionPDF($this->html,'I');
	}
	
	public function savePDF(){
		$this->commonActionPDF($this->html,'F');
	}
	
	public function downloadPDF(){
		$this->commonActionPDF($this->html,'D');
	}
	
	public function saveAndopenPDF(){
		$this->commonActionPDF($this->html,'FI');
	}
	
	private function commonActionPDF($html='',$control='I'){
		$this->AddPage();
		$this->writeHTMLCell($w=0, $h=0, $x=15, $y=10, $html,
					$border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
		$this->Output($this->path, $control);
	}
	
	private function setFileTitle(){
//		if ($this->m_=='client') $this->filetitle = 'Client '.$this->post['surname'].' '.$this->post['names'].' '.$this->post['id_user'];
		if ($this->m_=='client') $this->filetitle = 'Client_'.$this->get['c'];
		if ($this->m_=='transaction') $this->filetitle = 'Transction_'.$this->add_.'_'.$this->get['c'];
		if ($this->m_=='accounts') $this->filetitle = 'Accounts '.$_POST['id_user'];
	}
	
	public function getPngHtml(){
		return $this->print_html;
	}
	private function getStorageDir($id=0){
		$catalog = $id / 100;
		$catalog = (int)$catalog;		
		$catalog_path = "../".$this->files_category."/".$catalog;
		if (!is_dir($catalog_path)){
			mkdir($catalog_path,0777);
		}		
		return $this->files_category."/".$catalog;
	}

	private function updateWeekFiles($filezie=0){
		$filesize = intval($filezie);
		$this->db->query("UPDATE week_files SET size='$filesize' WHERE id='$id_week_files'");
	}
	
	private function insertWeekFiles($params=array()){
		$sql = "INSERT INTO week_files VALUES( null,".
					"'".$params['title']."',".
					"'".$params['descr']."',".
					$params['vis'].",".
					"'".$params['ext']."',".
					"'".$params['size']."',".
					$params['create_date'].",".
					$params['create_admin'].",".
					$params['create_child'].")";
		$this->db->query($sql);
		$id_week_files = $this->db->getOne("SELECT LAST_INSERT_ID()");
		return $id_week_files;
	}
	
	private function insertWeekFolderList($id_week_files=0){
		$q = "INSERT INTO week_folder_list VALUES(null,67,'{$id_week_files}',1)";
		$this->db->query($q);
	}

	private function getDataFromPdfstandard($template=''){
		$sql = 'select * from pdf_standard where template=\''.$template.'\' order by id asc';
        //echo $sql;
		$dataset = $this->db->getAll($sql);
		return $dataset;
	}
	
	private function generatePrintTemplatePNG(){
		$template = $this->m_;
		$dataset = $this->getDataFromPdfstandard($template);
		if(empty($dataset)){
			echo "Havn't any dataset to generate the PNG file ,please check the database or paramters!!";exit;
		}
		
		//generate the color palette
		$img = @imagecreatetruecolor(678,960) or die('Failed to create the color palette');
		$white = imagecolorallocate($img,255,255,255);
		imagefilledrectangle($img,0,0,678,960,$white);
	
		$drawtype = array(
			0=>'text', 1=>'date', 2=>'checkbox', 3=>'image',
			4=>'recbg', 5=>'line', 6=>'dtext', 7=>'lineArray',
		);
		
		foreach($dataset as $item){
			switch($item['type'])
			{
				case $drawtype[0]:      //text
				case $drawtype[6]:      //dynamic text
					$r = hexdec($item['color'][0].$item['color'][1]);
					$g = hexdec($item['color'][2].$item['color'][3]);
					$b = hexdec($item['color'][4].$item['color'][5]);
	
					$fontcolor = imagecolorallocate($img,$r,$g,$b);
					$fontfamily = "Fonts/tahoma.ttf";
					$fontsize = $item['fontsize']-2;
					
					if($item['type']==$drawtype[0]) {		//text run this code
						$fontstring = isset($_POST[$item['desc']]) ? $_POST[$item['desc']] : $item['desc'];
					}
					if($item['type']==$drawtype[6]){		//dynamic text run this code
						$fontstring = str_replace('******', time(), $item['desc']);
					}
					@imagettftext($img,$fontsize,0,$item['xposition'],$item['yposition'],$fontcolor,$fontfamily,$fontstring);
					break;								
				case $drawtype[1]:      //date
					//var_dump($item);
					$dst_x = $item['xposition'];
					$dst_y = $item['yposition'];
					$src_x = 0;
					$src_y = 0;
					$src_width = $item['width'];
					$src_height = $item['height'];
					$pct = 100; //透明度
					$img_first = imagecreatefrompng($item['imageurl']) or die('Failed to open the image file!!');
					imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
					
					if(isset($_POST[$item['desc']])){
						$time_val = date('dmY',$_POST[$item['desc']]);
						$fontcolor = imagecolorallocate($img,0,0,0);
						$fontfamily = "Fonts/tahoma.ttf";
						$fontsize = 9;
						
						for($i=0;$i<strlen($time_val);$i++){
							$fontstring = $time_val[$i];
							if($i == 0){
								$f_x =  $dst_x + 4;
							}else if($i == 1){
								$f_x =  $dst_x + 20;
							}else if($i == 2){
								$f_x =  $dst_x + 54;
							}else if($i == 3){
								$f_x =  $dst_x + 70;
							}else if($i == 4){
								$f_x =  $dst_x + 104;
							}else if($i == 5){
								$f_x =  $dst_x + 120;
							}else if($i == 6){
								$f_x =  $dst_x + 136;
							}else{
								$f_x =  $dst_x + 150;
							}
							$f_y = $dst_y + 13;
							@imagettftext($img,$fontsize,0,$f_x,$f_y,$fontcolor,$fontfamily,$fontstring);
						}
					}
					break;
								
				case $drawtype[2]:      //checkbox
					//var_dump($item);
					$dst_x = $item['xposition'];
					$dst_y = $item['yposition'];
					$src_x = 0;
					$src_y = 0;
					$src_width = $item['width'];
					$src_height = $item['height'];
					$pct = 100; //透明度
					$img_url = '';
					
					if(!empty($item['desc'])){
						if($item['desc']=='lang'){
						    $img_url = 'images/language_checkbox_'.$_POST['lang'].'.png';
						}else{
						    $post_arr = explode('##',$item['desc']);
						    $post_key = $post_arr[0];
						    $post_value = $post_arr[1];
						    
						    if( intval($_POST[$post_key]) === intval($post_value) ){
						    	$img_url = 'images/checkbox_selected.png';
						    }else{
						    	$img_url = $item['imageurl'];
						    }
						}
					}else{
						$img_url = $item['imageurl'];
					}
					$img_first = imagecreatefrompng($img_url) or die('Failed to open the image file!!');
					imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
					
					break;
				case $drawtype[3]:      //image
					//var_dump($item);
					$dst_x = $item['xposition'];
					$dst_y = $item['yposition'];
					$src_x = 0;
					$src_y = 0;
					$src_width = $item['width'];
					$src_height = $item['height'];
					$pct = 100; //透明度
					$img_first = imagecreatefrompng($item['imageurl']) or die('Failed to open the image file!!');
					imagecopymerge($img, $img_first, $dst_x, $dst_y, $src_x, $src_y, $src_width, $src_height, $pct);
					break;
	
				case $drawtype[4]:      //recbg
				case $drawtype[5]:      //line
				case $drawtype[7]:		//lineArray
					$r = hexdec($item['color'][0].$item['color'][1]);
					$g = hexdec($item['color'][2].$item['color'][3]);
					$b = hexdec($item['color'][4].$item['color'][5]);
	
					$bgcolor = imagecolorallocate($img,$r,$g,$b);
					$x1 = $item['xposition'];
					$y1 = $item['yposition'];
					$x2 = $x1+$item['width'];
					$y2 = $y1+$item['height'];
					imagefilledrectangle($img,$x1,$y1,$x2,$y2,$bgcolor);
										
					if($item['type'] == $drawtype[5]){		//line run this code
						$fontcolor = imagecolorallocate($img,0,0,0);
						$fontfamily = "Fonts/tahoma.ttf";
						$fontsize = 9;
						$fontstring = $_POST[$item['desc']];
		
						@imagettftext($img,$fontsize,0,$x1+5,$y1-2,$fontcolor,$fontfamily,$fontstring);
					}
					if($item['type'] == $drawtype[7]){		//lineArray run this code
						$fontcolor = imagecolorallocate($img,0,0,0);
						$fontfamily = "Fonts/tahoma.ttf";
						$fontsize = 9;
						
						$str_arr = isset($_POST[$item['desc']]) ? $_POST[$item['desc']] : array();
						$fontstring = '';
						
						foreach($str_arr as $row){
							$fontstring .= $row.'   ';
						}
						
						@imagettftext($img,$fontsize,0,$x1+5,$y1-2,$fontcolor,$fontfamily,$fontstring);
					}
					break;					
				default:
					//something to done
					break;
			}
		}
		
		$inData = array(
			'id'=>null,
			'title'=>$this->filetitle.'.png',
			'descr'=>'The '.$this->m_.' print png file',
			'vis'=>2,
			'ext'=>'png',
			'size'=>'',
			'create_date'=>time(),
			'create_admin'=>1,
			'create_child'=>1,
		);
		
        /* insert png file into database */		
//		$id_WeekFiles = $this->insertWeekFiles($inData);
//		$storageDir = $this->getStorageDir($id_WeekFiles);
//		$this->insertWeekFolderList($id_WeekFiles);
			
		if (!is_dir("../files_print/")){
            mkdir("../files_print/",0777);
        }
		$rImgDir = '../files_print/';
		$rImgName = $this->filetitle;
		$rImgPath = $rImgDir.$rImgName.".png";
		if(file_exists(rImgPath)){
           @unlink(rImgPath);
        }
		imagepng($img,$rImgPath);       /* create png file */
		imagedestroy($img);
		if(file_exists($rImgPath) === false){
			return "<span style='color:red;font-size:16px;'>Failed to generate the png file</span>";exit;
		}else{
			$filesize = filesize($rImgPath);
			$this->updateWeekFiles($filesize);
		}
		
//		$this->print_html = '<img src="img.php?f='.$rImgPath.'" border=0 />';     /* show png in html */
        $this->print_html = $rImgName;
		$this->html = '<img src="'.$rImgPath.'" border=0 />';
		
	}

	private function generatePrintPDF(){
		$inData = array(
			'id'=>null,
			'title'=>$this->filetitle.'.pdf',
			'descr'=>'The '.$this->m_.' print pdf file',
			'vis'=>1,
			'ext'=>'pdf',
			'size'=>'',
			'create_date'=>time(),
			'create_admin'=>1,
			'create_child'=>1,
		);
		
        /* insert pdf file into database */
//		$id_WeekFiles = $this->insertWeekFiles($inData);
//		$storageDir = $this->getStorageDir($id_WeekFiles);
//		$this->insertWeekFolderList($id_WeekFiles);
		
		if (!is_dir("../files_print/")){
            mkdir("../files_print/",0777);
        }
		$rPdfDir = '../files_print/';
		$rPdfName = $this->filetitle;
		$rPdfPath = $rPdfDir.$rPdfName.".pdf";		
		if(file_exists($rPdfPath)){
           @unlink($rPdfPath);
        }
		$this->path = $rPdfPath;
		$this->savePDF();        /* create pdf file */
		
		if(file_exists($this->path) === false){
			return "<span style='color:red;font-size:16px;'>Failed to generate the pdf file</span>";exit;
		}else{
			$filesize = filesize($this->path);
			$this->updateWeekFiles($filesize);
		}
	}
}

?>