<? 
class Search{
    private $account;
    private $client;
    private $db;
          
    function __construct()
    {
        $this->db=New database();
        $this->account=New Account();
        $this->client=New Client();
        $this->stats=New Stats();
        
    }
    
public function Go($p=array()) // получаем направление для бонда
{
    $p['json']=(int)$p['json'];
    $p['type']=(int)$p['type'];   // 0 OR 1 = искать в клентской базе
    
    $res=array('error'=>0);
    $row='';
    $res['row']=array();
    
    if ($p['type']==0 || $p['type']==1) {  }
    
    $q="SELECT * FROM client_profile WHERE (LOWER(surname) LIKE LOWER('%!%')) OR (LOWER(name) LIKE LOWER('%!%')) OR id=?";
    $row=$this->db->getAll($q, array($p['find'],$p['find'], (int)$p['find']));

    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="{$row[$item]['surname']} {$row[$item]['surname']}";
            $row[$item]['part']="Список клиентов";
            $row[$item]['descr']="Email: {$row[$item]['email']}. Телефон: {$row[$item]['mobile_phone']}";
            $row[$item]['link']='/?p=client&edit&id='.$row[$item]['id'];
        }
        $res['row']=array_merge($res['row'],$row);
    }   
    
    $q="SELECT * FROM account_transaction WHERE (LOWER(reference) LIKE LOWER('%!%'))  OR id=?";
    $row=$this->db->getAll($q, array($p['find'], (int)$p['find']));
  //  print_r($row); exit;
    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="ID{$row[$item]['id']} {$row[$item]['reference']}";
            $row[$item]['part']="Транзакции";
            //$row[$item]['descr']="Email: {$row[$item]['email']}. Телефон: {$row[$item]['mobile_phone']}";
            $row[$item]['link']='/?p=account&transaction&edit&id='.$row[$item]['id'];
        }
    
        $res['row']=array_merge($res['row'],$row);
    }
 
     
    $q="SELECT * FROM project WHERE (LOWER(title) LIKE LOWER('%!%'))";
    $row=$this->db->getAll($q, array($p['find']));
  //  print_r($row); exit;
    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="ID{$row[$item]['id']} {$row[$item]['title']}";
            $row[$item]['part']="Проекты";
           //$row[$item]['descr']="Email: {$row[$item]['email']}. Телефон: {$row[$item]['mobile_phone']}";
            $row[$item]['link']='/?p=project&edit&id='.$row[$item]['id'];
        }
    
        $res['row']=array_merge($res['row'],$row);
    }   

 
     
    $q="SELECT * FROM project_news WHERE (LOWER(title) LIKE LOWER('%!%')) OR  (LOWER(descr) LIKE LOWER('%!%'))";
    //echo  $q; exit;
    $row=$this->db->getAll($q, array($p['find'],$p['find']));
    // print_r($row); exit;
    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="ID{$row[$item]['id']} {$row[$item]['title']}";
            $row[$item]['part']="Новости проекта";
           //$row[$item]['descr']="Email: {$row[$item]['email']}. Телефон: {$row[$item]['mobile_phone']}";
            $row[$item]['link']='/?p=project&news&edit&id='.$row[$item]['id'];
        }
    
        $res['row']=array_merge($res['row'],$row);
    }   
    
     
    $q="SELECT * FROM bond_position_open WHERE  id=?";
    //echo  $q; exit;
    $row=$this->db->getAll($q, array((int)$p['find']));
    // print_r($row); exit;
    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="ID{$row[$item]['id']}";
            $row[$item]['part']="Открытые позиции";
            $row[$item]['descr']="Цена: {$row[$item]['price']}. Всего: {$row[$item]['total']}";
            $row[$item]['link']='/?p=position&table&open';
        }
    
        $res['row']=array_merge($res['row'],$row);
    }       

     
    $q="SELECT * FROM bond_position_close WHERE  id=?";
    //echo  $q; exit;
    $row=$this->db->getAll($q, array((int)$p['find']));
    // print_r($row); exit;
    if (count($row)>0){
        foreach ($row as $item=>$key){
            $row[$item]['title']="ID{$row[$item]['id']}";
            $row[$item]['part']="Закрытые позиции";
            $row[$item]['descr']="Цена: {$row[$item]['price']}. Всего: {$row[$item]['total']} Цена продажи:   {$row[$item]['price_sell']} Цена продажи:  {$row[$item]['price_sell_total']}";
            $row[$item]['link']='/?p=position&table&close';
        }
    
        $res['row']=array_merge($res['row'],$row);
    }       
        
   /*           
    if (count($row)>0){     }
    else $res=array('error'=>1, 'error_msg'=>'Ничего не найдено');
  

    $q="SELECT * FROM project_ WHERE (LOWER(surname) LIKE LOWER('%!%'))";
    $row=$this->db->getAll($q, array($p['find']));
    if (count($row)>0){ 
        foreach ($row as $item=>$key){
            $row[$item]['title']="{$row[$item]['surname']} {$row[$item]['surname']}";
            $row[$item]['part']="Список клиентов";
            $row[$item]['descr']="Email: {$row[$item]['email']}. Телефон: {$row[$item]['mobile_phone']}";
        }
        $res['row']=$row;
    }
    else $res=array('error'=>1, 'error_msg'=>'Ничего не найдено');
    */


    if ($p['json']==1)  {
        $result =  json_encode($res);
        echo "jsonpCallback(".$result.")";
    } else return  $res;    
}

}?>