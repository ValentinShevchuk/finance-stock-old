<?
class Stats extends database{    
    public function CreateUserEvent($param=array())
    {
        /* copypasta
                    $arr=array(
            'id_user'=>$_SESSION['site_user'],
            'event'=>'5',
            'event_text'=>'Смена статуса',
            'url'=>"{$_SERVER['PHP_SELF']}{$_SERVER['REQUEST_URI']}",
            'p'=>$_GET['p'],
            'id_record'=>$_GET['id'] ,   
            'post'=>json_encode($param['POST'], JSON_UNESCAPED_UNICODE)    
        );
        $this->stats->CreateUserEvent($arr); // записываем в лог действие
        */
        if ($param[0]['json']==1) $param=$param[0];
        if (!isset($param['id_user'])) $param['id_user']=$_SESSION['site_user'];
        if (!isset($param['url'])) $param['url']="{$_SERVER['PHP_SELF']}{$_SERVER['REQUEST_URI']}";
        if (!isset($param['p'])) $param['p']=$_GET['p'];        
        if (!isset($param['id_record'])) $param['id_record']=$_GET['id']; 
        if (isset($param['post'])) $param['post']=json_encode($param['post'], JSON_UNESCAPED_UNICODE);         
        
        $id=$this->insertdata('user_event',$param);
        $row['error']=($id>0)?1:0;
        
        if ($param['json']==1)  {
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        } else return  $row;        
    }
    public function GetUserEventList($param=array())
    {
        if ($param[0]['json']==1) $param=$param[0];
        
        
        if (isset($param['id_record'])) { $where=" AND t1.id_record=?"; $p[]=$param['id_record']; }
        if (isset($param['p'])) {$where.=" AND t1.p=?"; $p[]=$param['p'];}
        if (isset($param['limit'])) {$where.=" ORDER by t1.id DESC LIMIT ?"; $p[]=$param['limit']; }
        
        $q="SELECT t1.*, (SELECT CONCAT(surname,' ', name) FROM user t2 WHERE t2.id=t1.id_user) as id_user_fio FROM user_event t1 WHERE t1.id>0 $where";
        //echo $q; exit;
        $row=$this->getAll($q,$placeholder=$p);
        
        
        if ($param['json']==1)  {
            $result =  json_encode($row);
            echo "jsonpCallback(".$result.")";
        } else return  $row;        
    }
}
?>