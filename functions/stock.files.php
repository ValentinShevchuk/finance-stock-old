<?
class Files extends database{    
	public   $tableName=array('','project_files','client_files');
	public $month=array(
		1=>array('full'=>'Январь','min'=>'Янв'),
		2=>array('full'=>'Февраль','min'=>'Фев'),
		3=>array('full'=>'Март','min'=>'Мар'),
		4=>array('full'=>'Апрель','min'=>'Апр'),
		5=>array('full'=>'Май','min'=>'Май'),
		6=>array('full'=>'Июнь','min'=>'Июн'),
		7=>array('full'=>'Июль','min'=>'Июл'),
		8=>array('full'=>'Август','min'=>'Авг'),
		9=>array('full'=>'Сентябрь','min'=>'Сен'),
		10=>array('full'=>'Октябрь','min'=>'Окт'),
		11=>array('full'=>'Ноябрь','min'=>'Ноя'),
		12=>array('full'=>'Декабрь','min'=>'Дек')
	);
	
	function Download($param=array())
	{
		
		foreach ($_FILES as $row){
		//print_r($_FILES);
		$param['json']=(int)$param['json'];
		$type= pathinfo($row["name"][0]);
		$id_manager=(int)$_SESSION['site_user'];
		$arr=array(
			'title'=>$row['name'][0],
			'type'=>$row['type'][0],
			'size'=>$row['size'][0],
			'ext'=>$type['extension'],
			'id_manager'=>"$id_manager");
	
		$id_files=$this->insertdata('files',$arr);
		
		if(isset($_POST['id_client'])){ 
			$arr=array(
			'id_client'=>(int)$_REQUEST['id_client'],
			'id_file'=>$id_files,
			'id_manager'=>"$id_manager");
	
			$id_files=$this->insertdata('client_files',$arr);
		}
		
		$temp_file=$row["tmp_name"][0];
		}
	/*    
		$db->query("INSERT INTO files VALUES( null,'".$pole2."','".$pole3."', $file_type,'".$type['extension']."','','".time()."',$admins_)"); 
		$id=$db->getOne("SELECT LAST_INSERT_ID();");*/  
		
		$catalog=(int)($id/100);
		if (!is_dir($this->doc_root."/files/".$catalog)){
			mkdir($this->doc_root."/files/".$catalog,0777);
		}

		$files_category=0;
		if (move_uploaded_file($temp_file , $this->doc_root."files/$catalog/$id_files.{$type['extension']}"))
		{
			
		}
		echo $id_files;
			
	}
	function saveFile($p=array())
	{
		/*
			Входные данные
			id_client (int)
			id_project (int)
			date_start (datestamp)
			tableNameInt (int)
			
		*/
	
		$p['json']=(int)$p['json'];
		$p['own_id']=(int)$p['own_id'];
		$p['id_client']=(int)$p['id_client'];
		$p['dateStart']=date("Y-m-01 00:00:00",strtotime($p['dateStart']));
		$p['tableNameInt']=($p['tableNameInt']>0)?(int)$p['tableNameInt']:1;
		$p['id_file']=(int)$p['id_file'];
		$p['is_site']=(int)$p['is_site'];
		$p['is_report']=(int)$p['is_report'];

		if ($p['tableNameInt']==1){
		$arr=array(
			(own_id)=>$p['own_id'],
			(id_client)=>($_SESSION['mg']=='')?$_SESSION['site_user']:0,
			(id_manager)=>($_SESSION['mg']=='')?0:$_SESSION['site_user'],
			(date_start)=>$p['dateStart'],  
			(is_site)=>$p['is_site'], 
			(is_report)=>$p['is_report'],      
			(id_file)=>$p['id_file']   
		);}
		else if ($p['tableNameInt']==2){
			$arr=array(
				(own_id)=>$p['own_id'],
				(id_client)=>($_SESSION['mg']=='')?$_SESSION['site_user']:0,
				(id_manager)=>($_SESSION['mg']=='')?0:$_SESSION['site_user'],     
				(id_file)=>$p['id_file']   
			);		
		}
		$this->insertdata($this->tableName[$p['tableNameInt']],$arr);
		if ($p['title']!=''){
			$q="UPDATE files SET title=? WHERE id='{$p['id_file']}'";
		//	echo $q;
			$this->db->query($q,array($p['title']));
		}
		$res=array((error)=>0);
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
	function getFileList($p=array())
	{
		$p['json']=(int)$p['json'];
		$p['own_id']=(int)$p['own_id'];
		$p['tableNameInt']=($p['tableNameInt']>0)?(int)$p['tableNameInt']:1;

		if ($p['tableNameInt']==1) { 
			if (isset($p['is_site'])){ 
				$p['is_site']=(int)$p['is_site'];
				$where.=" AND t2.is_site={$p['is_site']}"; 
			}
			if (isset($p['is_report'])){ 
				$p['is_report']=(int)$p['is_report'];
				$where.=" AND t2.is_report={$p['is_report']}"; 
			}
			$orderBy="ORDER by t2.date_start DESC, t1.id DESC";

		} 
		else $orderBy="ORDER by t1.id DESC";


		
		$q="SELECT t1.*, t2.* FROM files t1, {$this->tableName[$p['tableNameInt']]} t2 WHERE t2.id_file=t1.id AND t2.own_id='{$p['own_id']}'AND t1.vis>0 AND t2.vis>0 $where GROUP by t1.id $orderBy";
	   // echo $q;
		$row=$this->getAll($q);
		if (count($row)>0) { 
			
			foreach($row as $item=>$key){
				$row[$item]['month']=$this->month[(int)date('m',strtotime($row[$item]['date_start']))]['full'];
				$row[$item]['ico']='fa fa-file-text-o';
				if ($row[$item]['ext']=='jpg' || $row[$item]['ext']=='gif' || $row[$item]['ext']=='bmp')  $row[$item]['ico']='fa fa-file-image-o';
				if ($row[$item]['ext']=='zip' || $row[$item]['ext']=='rar')  $row[$item]['ico']='fa fa-file-zip-o';
				if ($row[$item]['ext']=='pdf')  $row[$item]['ico']='fa fa-file-pdf-o';
			}
			$res=array((error)=>0, (row)=>$row);
		}
		else $res=array((error)=>1);
		
		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;      
	}
	function removeFile($p=array())
	{
		/*
			Входные данные
			id_file (int) = файл
		*/
	
		$p['json']=(int)$p['json'];
		$p['id_file']=(int)$p['id_file'];

		$this->update('project_files',array((vis)=>0), " WHERE id_file={$p['id_file']}");
		$this->update('files',array((vis)=>0), " WHERE id={$p['id_file']}");

		$res=array((error)=>0);

		if ($p['json']==1)  {
			$result =  json_encode($res);
			echo "jsonpCallback(".$result.")";
		} else return  $res;    
	}
}
?>