<?php
/**
 * class exportXLS
 */
class exportXLS {
	
	var $filename;
		
	var $dataset;
	
	var $titleset;
	
	var $titlebgcolor;
	
	var $needColtitle;

        /**
         * initialize function
         * @param <string> $filename            is the xls file's name
         * @param <array> $dataset              is the output DataSet
         * @param <boolean> $needColtitle       is to control the title display
         * @param <array> $titleset             is the title array
         * @param <string> $titlebgcolor        is the HEXcolor , as : #09EEFF
         */
	public function __construct($filename='',$dataset = array(),$needColtitle = false,$titleset=array(),$titlebgcolor = '#CECECE')
	{

		if(empty($filename)){
			$this->filename = date('dmY',time()).'_outexcel.xls';
		}else{
			$this->filename = str_replace('.xls','',$filename).'.xls';
		}
		
		$this->dataset = is_array($dataset)? $dataset : array();

		$this->needColtitle = $needColtitle;

		if($this->needColtitle === true){			
			$this->titleset = is_array($titleset) ? $titleset : array();
			$this->titlebgcolor = 'FF'.str_replace('#','',$titlebgcolor);
		}

	}
	
	/**
         * output and download the xml file
         */
	public function outputXLS($path=''){
		require_once($path.'../include/phpexcel/PHPExcel.php');
		require_once($path.'../include/phpexcel/PHPExcel/IOFactory.php');
		ob_clean();
		$obj_excel = new PHPExcel();
		$results = $this->dataset;
		//print_r($results);exit;
		//$results = iconv("gbk","utf-8",$results); //The escape character set
		if($this->needColtitle == true){
			$titleArr = $this->titleset;
			$row_num = 1;
			foreach($titleArr as $key=>$item){
				$col_value = chr(65+$key).$row_num;
				$obj_excel->getActiveSheet()->setCellValue(chr(65+$key).'1',$item, false);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$obj_excel->getActiveSheet()->getStyle($col_value)->getFill()->getStartColor()->setARGB($this->titlebgcolor);
			}
		}		
		
		if(!empty($results))
		{
			$row_num = ($this->needColtitle === true) ? 2 : 1;
			foreach($results as $index=>$row){
				$i = 0;
				foreach($row as $item){
					$col_value = chr(65+($i++)).($row_num);
					$obj_excel->getActiveSheet()->setCellValue($col_value,$item, false);
				}
				$row_num++;
			}
		}
		
		$objwriter = PHPExcel_IOFactory::createWriter($obj_excel,'Excel5');
		//$objwriter = new PHPExcel_Writer_Excel2007($obj_excel); 
		
		header('Content-Disposition: attachment; filename='.$this->filename);
		header("Content-Type:application/vnd.ms-excel");
		header("Cache-Control:max-age=0");
		$objwriter->save('php://output');
		
	}

}

?>