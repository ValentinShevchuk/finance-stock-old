<?php

	class import {
		
		private $extend_class;
		
		private $sheetData;
		
		private $table_name;
		
		private $check_field;
		
		private $fields;
		
		private $foriegn_field;
		
		public function __construct($fields,$check_field=array(),$table_name,$file,$foriegn_field=array()){
			global $db;
			$this->db = $db;
			$this->fields = $fields;
			$this->table_name = $table_name;			
			$this->check_field = $check_field;
			$this->foriegn_field = $foriegn_field;
			
			require_once('../include/phpexcel/PHPExcel.php');
			require_once('../include/phpexcel/PHPExcel/IOFactory.php');
			
			$inputFileName = $file;
			$file_info = pathinfo($file);
			$file_extension = $file_info['extension'];
			
			if(strtolower($file_extension) == 'xls'){
				$inputFileType = 'Excel5';
			}else if(strtolower($file_extension) == 'xlsx'){
				$inputFileType = 'Excel2007';
			}else if(strtolower($file_extension) == 'csv'){
				$inputFileType = 'CSV';
			}else{
				return "It\'s not permitted to upload this files!!";
			}
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
				
			$this->sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			//var_dump($this->sheetData);exit;
			$this->formatKey($this->sheetData);
			//var_dump($this->sheetData);exit;
		}

		public function saveDataToDB(){
			$unSaveData = $fields_value = $faildSaveData = array();
			$first_sql = "INSERT INTO `{$this->table_name}` ";
			
			if(!empty($this->check_field)){
				//var_dump(count($this->sheetData));exit;
				$i=0;
				foreach($this->sheetData as $row){
				    $i=$i+1;
				    if($i>1){// $i=1 is title array()
					$insert_value = array();			
					//check duplicate values ​​or nots
					if($this->checkCanSave($row) === 0){
						foreach($row as $index=>$item){
							if(strlen($index)>1){
							$fields_value[]="`{$index}`";
							$insert_value[] = "'".$this->formatValue($item)."'";
							} 
						}
						$fields_sql = '('.implode(',',$fields_value).')';
						$insert_value_sql = ' VALUES ('.implode(',',$insert_value).')';
						$insert_sql=$first_sql.$fields_sql.$insert_value_sql;
						//var_dump($insert_sql);//exit;
						$bool_int = $this->db->query($insert_sql);
						if($bool_int <= 0 || gettype($bool_int) == 'object') { $faildSaveData[] = $row; }
					}else{ //save the duplicate values to $unSavData
						$unSaveData[] = $row;
					}
					unset($insert_value);
					unset($fields_value);
					
					}
					
				}
			}else{
				foreach($this->sheetData as $row){
					$insert_value = array();				
					//check duplicate values ​​or not
					foreach($row as $index=>$item){
						$fields_value[]="`{$index}`";
						$insert_value[] = "'".$this->formatValue($item)."'"; 
					}
					$fields_sql = '('.implode(',',$fields_value).')';
					$insert_value_sql = ' VALUES ('.implode(',',$insert_value).')';
					$insert_sql=$first_sql.$fields_sql.$insert_value_sql;
					//var_dump($insert_sql);
					$bool_int = $this->db->query($insert_sql);
					if($bool_int <= 0) { $faildSaveData[] = $row; }
				}
			}
			
			if( !empty($faildSaveData) ){
				return 'There are '.count($faildSaveData).' records failed to save, please try to check about your data of upload!!';
			}else if( !empty($unSaveData) ) {
				return 'There are '.count($unSaveData).' duplicate records not save, please try to check about your data of upload!!';
			}else{
				return 'All datas are saved in database ,Congratulation';
			}
		}

		private function checkCanSave($data=array()){
			$where = $this->getCheckWhere($data);
			$check_q = "SELECT count(*) FROM `{$this->table_name}` $where LIMIT 1 ";
			//var_dump($check_q);
			$total = intval($this->db->getOne($check_q));
			//var_dump($total);
			return $total;
		}
		
		private function getCheckWhere($data){
			$where = "WHERE 1 ";
			foreach($this->check_field as $item){
				$where.=" AND `{$item}` = '{$data[$item]}'";
			}
			return $where;			
		}
				
		private function formatValue($val){
			//判断是否有$的字符  Determine whether there is $ characters
			$dollar_pattern = '/^\$([\d-|\d])+/';
			if( preg_match($dollar_pattern,$val) ){ return str_replace('$','',$val); }				
			//判断是不是时间值
			$date_pattern = '/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})(.*)/';
			$matches = array();
			if( preg_match($date_pattern,$val,$matches) ){ 
				return strtotime($matches[3].'-'.$matches[2].'-'.$matches[1]); 
			}			
			return mysql_escape_string($val);
		}
		
		private function formatKey(){
			if(!empty($this->fields) && !empty($this->sheetData)){
				
				if(!empty($this->foriegn_field)){
					foreach($this->sheetData as $key=>$item){
						$o_num = 65;
						foreach($this->fields as $new_key){
							if($this->foriegn_field['import_field_compare'] ==  $new_key){
								if(isset($this->foriegn_field['need_function'])){
									$function_name = $this->foriegn_field['need_function'];
									$this->sheetData[$key][$this->foriegn_field['import_field_set']] = $this->formatValue($this->$function_name($this->sheetData[$key][chr($o_num)]));
								}else{
									$this->sheetData[$key][$this->foriegn_field['import_field_set']] = $this->formatValue($this->useToCommon($this->sheetData[$key][chr($o_num)]));
								}
							}
							$this->sheetData[$key][$new_key] = $this->formatValue($this->sheetData[$key][chr($o_num)]);
							unset($this->sheetData[$key][chr($o_num)]);
							$o_num++;
						}
					}					
				}else{
					foreach($this->sheetData as $key=>$item){
						$o_num = 65;
						foreach($this->fields as $new_key){
							$this->sheetData[$key][$new_key] = $this->formatValue($this->sheetData[$key][chr($o_num)]);
							unset($this->sheetData[$key][chr($o_num)]);
							$o_num++;
						}
					}
				}
			}
		}
		
		private function useToCurrency($compare_val){
			/*$f_name='Currency';
			$q = "SELECT id FROM `constants` WHERE LOWER(title)=LOWER('{$f_name}')";
			$f_id = $this->db->getOne($q);
			*/
			$f_id = 18;
			$q = "SELECT value FROM constants WHERE parent='{$f_id}' AND UPPER(title)=UPPER('{$compare_val}') LIMIT 1";
			$value = $this->db->getOne($q);
			return $value;
		}		

		private function useToCommon($compare_val){
			$q = "SELECT {$this->foriegn_field['foriegn_field_get']} FROM `{$this->foriegn_field['foriegn_table']}` 
					WHERE `{$this->foriegn_field['foriegn_field_compare']}` = {$compare_val}  LIMIT 1";
			$value = $this->db->getOne($q);
			return $value;
		}
		

		
	}
	
?>


















