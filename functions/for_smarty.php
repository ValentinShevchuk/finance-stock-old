<?php
function GetHtmlOption($s,$f=1)
{
	global $_SESSION,$db,$langs; $option='';
	$arr=$s;
	//var_dump($arr);
	$s=$arr['s'];
	$a=$arr['a'];
	if (is_integer($s)) { 
		$q="SELECT * FROM  constants WHERE parent='{$s}'"; $row=$db->getall($q);
	} else {
		$q="SELECT id FROM  constants WHERE LOWER(title)=LOWER('{$s}')";
		$row=$db->getOne($q);
		$q="SELECT * FROM  constants WHERE parent='{$row}' AND vis>0 order by value";
		$row=$db->getall($q);      //   echo $q;
	}

	foreach ($row as $row){ 
		if ($a==$row[value]) {
			$check=' selected=selected';
		}else {
			$check='';
		}
		$option.="<option value='{$row[value]}' $check>{$row[title]}</option>";
	
	}
	//  if ($f==1) echo $option;
	return $option;
}

function GetHtmlOptionString($params){
	global $db;
	$s = trim($params['s']);
	$sid = intval($params['sid']);	
	$q="SELECT id FROM  constants WHERE LOWER(title)=LOWER('{$s}')";
	$fid=$db->getOne($q);
	$q="SELECT title FROM  constants WHERE parent='{$fid}' AND `value`='{$sid}' limit 1";
	$row=$db->getOne($q);      //   echo $q;
	return $row;
}

function GetAccountName($params){
	global $db;
	$s = trim($params['s']);
	$q="SELECT full_account FROM accounts_client WHERE id={$s} limit 1";
	$row=$db->getOne($q);      //   echo $q;
	return $row;
}

function FormatSpecialDate($params){
	$date_str = $params['date'];
	$index = $params['index'];	
	$tmp_arr = explode(' - ',$date_str);
	
	return $tmp_arr[$index];
}

function GetFullAcounts($params){
	global $db;
	$id = $params['id'];
	$q = "SELECT full_account FROM `accounts_client` WHERE `id` = '{$id}' LIMIT 1 ";
	$full_account=$db->getOne($q);
	return $full_account;
}

function GetCountryCityName($params){
	global $db;
	$s = trim($params['s']);
	$a = trim($params['a']);
	if(!empty( $s)){ 
	    if($a=='country'){
		    $q="SELECT name_en,code FROM geo_{$a} WHERE id={$s} limit 1";
	        $row=$db->getRow($q);      //   echo $q;
			$row = $row[name_en]." - ".$row[code];
		}else{
		    $q="SELECT name_en FROM geo_{$a} WHERE id={$s} limit 1";		
	        $row=$db->getOne($q);      //   echo $q;
		}
	}
	return $row;
}

function GetDecorationMoney($number){
    $number=implode($number);
    $pos = strripos($number,'-');
    if ($pos === false)
        $pref='';
    else{
        $pref='-';
        $number=substr($number,1);
    }
    $str = explode('.',$number);
    $number='';
    $mod=strlen($str[0])%3;
    for($i=0;$i<=strlen($str[0])-1;$i++){
        if ($i%3==$mod && $i!==0){$number.="&nbsp;";}
        $number.=$str[0][$i];
    }
    if ($str[1]=='')$str[1]='00';
    if ($number=='')$number='0';
    $number.='.'.$str[1];
	return $pref.$number;
}

function GetManager($params){
	global $db;
	$s = trim($params['s']);
	$q="SELECT fio FROM week_user WHERE id={$s} limit 1";
	$row=$db->getOne($q);      //   echo $q;
	return $row;
}

?>