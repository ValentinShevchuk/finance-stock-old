<?
header("Content-Type: text/html; charset=UTF-8");
SESSION_START();

if (isset($_GET['local'])) $path="/Applications/MAMP/htdocs/"; else $path="/var/www/stock/";
//$path='';
if (isset($_GET['local'])) $path2=$path."stock/"; else $path2=$path."master/"; 

require($path."bonds.database.php");      
require($path2."functions/class.phpmailer.php");
require($path2."functions/enter.php");
require($path2."functions/stock.account.php");
require($path2."functions/stock.stats.php");
require($path2."functions/stock.bond.php");
//require($path2."functions/stock.bond.direct.php");

$db=new database(); // работа с базой акций
$bond=New Bond(); // работа с функциями акций
$sms = new Enter();


function percent($x,$y,$n)
{

    if ($y==0) { if ($x>0) $cop_percent=100; else $cop_percent=0;} else $cop_percent=round(($n/$y)*100,4);

    return $cop_percent;
}

$day_get=isset($_GET['day'])?$_GET['day']:date("d");
$DATE_NOW=date("Y-m-$day_get  23:59:59");//"2016-10-{$date_i}"; // 
$DATE_NOW_START=date("Y-m-$day_get 00:00:00");//"2016-10-{$date_i} 00:00:00";//
$DATE_NOW_END=$DATE_NOW;//"2016-10-06 23:59:59";//date("Y-m-d 23:59:59")
$q="SELECT id FROM bond_name WHERE vis=1";
$row=$bond->getAll($q);

$date="date_create>='{$DATE_NOW_START}' AND date_create<='{$DATE_NOW_END}'";

foreach ($row as $row){
    $type_bonds=$row['id'];
    for($is_demo=0;$is_demo<=1; $is_demo++) {

        $q="SELECT * FROM bond_statistics_day WHERE type_bonds=$type_bonds AND is_demo=$is_demo AND date_create='{$DATE_NOW}' - INTERVAL 1 DAY ORDER by id DESC LIMIT 1";
        $stat = $bond->getRow($q);
        print_r($stat);
        /*---------------Загружаем открытые позиции--------------------*/

        $q="SELECT COUNT(id) count, SUM(price_real_total) price_real_total, SUM(total) total, date_create, price
        FROM bond_position_open
        WHERE $date AND type_bonds=$type_bonds AND is_demo=$is_demo AND vis=1 ORDER BY date_create DESC";
       // echo $q."<br>"; exit;
       $res=$bond->getRow($q);
       // print_r($res);
        $count_open_position=(int)$res['count']; // кол-во открытых
        $open_total=(int)$res['total']; // цена всех открытых
        $open_date_create=(int)$res['date_create']; // дата последней открытой позиции
        $open_price=(int)$res['price']; // дата последней открытой позиции

        /*------------------------------------------------------------*/

        $cop_num=$count_open_position-$stat['count_open_position'];
        $cop_percent=percent($count_open_position,$stat['count_open_position'],$cop_num);

       // echo $count_open_position." ".$cop_num." ".$cop_pecent;

       /*---------------Загружаем закрытые позиции--------------------*/

        $q="SELECT COUNT(id) count, SUM(price_real_total) price_real_total, SUM(price_sell_total) total, date_create, price_sell
        FROM bond_position_close
        WHERE $date AND type_bonds=$type_bonds AND is_demo=$is_demo AND vis=1 ORDER BY date_create DESC";
        $res = $bond->getRow($q);

        $count_close_position=(int)$res['count']; // кол-во открытых
        $close_total=(int)$res['total']; // цена всех открытых
        $close_date_create=(int)$res['date_create']; // дата последней открытой позиции
        $close_price=(int)$res['price_sell']; // дата последней открытой позиции

        $ccp_num=$count_close_position-$stat['count_close_position'];
        $ccp_percent=percent($count_close_position,$stat['count_close_position'],$ccp_num);
        // echo "<br>".$q;
        // print_r($count_open_position);//
        //echo $count_close_position." ".$ccp_num." ".$ccp_percent."|";

        /*------------------Считаем кол-во сделок-----------------------*/

        $count_deal=$count_close_position+$count_open_position;
        $cd_num=$count_deal-$stat['count_deal'];
        $cd_percent=percent($count_deal,$stat['count_deal'],$cd_num);

        //echo $count_deal." ".$cd_num." ".$cd_percent."|";
       
        /*-----------------Оборот средств------------------------------*/

        $tourover_all_deal= $close_total+$open_total;

        /*-----------------Дата последней сделки------------------------------*/

        if ($open_date_create>$close_date_create)
        {
            $tourover_last_position=$open_total;
            $price_last_position=$open_price;
        }
        else
        {
            $tourover_last_position=$close_total;
            $price_last_position=$close_price;
        }

        $price_min=0; $price_max=0; $price_begin=0; $price_last_deal=0; $price_num=0; $price_percent=0;
        /*---------------Максимальная дата за год--------------------------------*/
        if ($is_demo==0){

        $q="SELECT price , date_create FROM `bond_amount` WHERE price=(SELECT MAX(price) price FROM `bond_amount` WHERE  type_bonds=$type_bonds LIMIT 1) AND type_bonds=$type_bonds LIMIT 1";

        $res = $bond->getRow($q);

        $year_max=$res[price];
        $year_max_date=$res[date_create];

        /*---------------Минимальная дата за год---------------------------------*/

        $q="SELECT price , date_create FROM `bond_amount` WHERE price=(SELECT MIN(price) price FROM `bond_amount` WHERE  type_bonds=$type_bonds LIMIT 1) AND type_bonds=$type_bonds LIMIT 1";
        $res =  $bond->getRow($q);
        $year_min=$res[price];
        $year_min_date=$res[date_create];

    }
        
            $q = "
            SELECT MAX(price) as price_max, MIN(price) as price_min 
            FROM bond_amount 
            WHERE 
                type_bonds='$type_bonds'  AND (
                $date )
            ORDER BY id DESC";  echo $q;
            $row=$db->getRow($q);
            $price_min=$row['price_min'];
            $price_max=$row['price_max'];         



        $date_create=$DATE_NOW;//strtotime(date("Y-m-d 00:00:00"));

       $q="
 INSERT bond_statistics_day
SET
  
    date_create='$date_create',
    is_demo='$is_demo',
    type_bonds='$type_bonds',
    count_open_position='$count_open_position',
    cop_num='$cop_num',
    cop_percent='$cop_percent',
    count_close_position='$count_close_position',
    ccp_num='$ccp_num',
    ccp_percent='$ccp_percent',
    count_deal='$count_deal',
    cd_num='$cd_num',
    cd_percent='$cd_percent',
    tourover_all_deal='$tourover_all_deal',
    tourover_last_position='$tourover_last_position',
    price_last_position='$price_last_position',
    price_min='$price_min',
    price_max='$price_max',
    price_begin='$price_begin',
    price_num='$price_num',
    price_percent='$price_percent',
    year_min_price='$year_min',
    year_max_price='$year_max',
    year_min_date='$year_min_date',
    year_max_date='$year_max_date'

ON DUPLICATE KEY
UPDATE

    count_open_position='$count_open_position',
    cop_num='$cop_num',
    cop_percent='$cop_percent',
    count_close_position='$count_close_position',
    ccp_num='$ccp_num',
    ccp_percent='$ccp_percent',
    count_deal='$count_deal',
    cd_num='$cd_num',
    cd_percent='$cd_percent',
    tourover_all_deal='$tourover_all_deal',
    tourover_last_position='$tourover_last_position',
    price_last_position='$price_last_position',
    price_min='$price_min',
    price_max='$price_max',
    price_begin='$price_begin',
    price_num='$price_num',
    price_percent='$price_percent',
    year_min_price='$year_min',
    year_max_price='$year_max',
    year_min_date='$year_min_date',
    year_max_date='$year_max_date'
 ";
 //echo $q;
   $bond->query($q);


    }

}


?>