<?

require_once($_SESSION['SMARTY_DIR'] . 'Smarty.class.php');
$GLOBALS['smarty'] = new Smarty();

$GLOBALS['smarty']->template_dir = $_SESSION['GAME_DIR'].'s_templates/';
$GLOBALS['smarty']->compile_dir = $_SESSION['GAME_DIR'].'s_templates_c/';
$GLOBALS['smarty']->config_dir = $_SESSION['GAME_DIR'].'s_configs/';
$GLOBALS['smarty']->cache_dir = $_SESSION['GAME_DIR'].'s_cache/';
?>