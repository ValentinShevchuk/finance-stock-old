<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinymce.create('tinymce.plugins.ExamplePlugin', {
    createControl: function(n, cm) {
        switch (n) {
            case 'insertImage':
                var mlb = cm.createListBox('insertImage', {
                     title : 'Фото из архива',
                     onselect : function(v) {
                         tinyMCE.activeEditor.selection.setContent('<a rel="prettyPhoto" href="http://www.1-week.ru/cms/photo/'+v+'_big.jpg"><img src="http://www.1-week.ru/cms/photo/'+v+'_middle.jpg" align="left"><\/a>');
                     }
                });

                // Add some values to the list box
				/*<? $q1=	"SELECT * FROM week_photo_file, week_time WHERE (
								'".$_SESSION['admins']."'=week_time.id_user AND 
								week_photo_file.id_photo_file=week_time.id_service AND 
								
								(week_time.service='photo_file' AND 
								week_time.events='create') AND vis=1 ) ORDER BY id_photo_file";
									$r1 = mysql_query($q1, $con);
									$row1 = mysql_fetch_assoc($r1);
									$num1 = mysql_num_rows($r1);
									if ($num1>0) 
										do { ?> 
										 mlb.add('<img src="http://1-week.ru/cms/photo/id_<?=$row1['id_photo_file']?>_small.jpg">', 'id_<?=$row1['id_photo_file']?>');
										
										
										<? } while ($row1 = mysql_fetch_assoc($r1));?>
										
               
                

                // Return the new listbox instance*/
                return mlb;
				
			 case 'insertLink':
                var mlb = cm.createListBox('insertLink', {
                     title : 'Страницы',
                     onselect : function(v) {
                         tinyMCE.activeEditor.selection.setContent(v);
                     }
                });

                // Add some values to the list box
				<?  $q=	"SELECT * FROM week_page, week_time WHERE
							
							'".$_SESSION['admins']."'=week_time.id_user AND
							 
							week_page.id_page=week_time.id_service AND 
							week_time.events='create' AND
							 week_time.service='page' AND week_page.vis=2
							
							ORDER BY week_page.position";
							
						$r = mysql_query($q, $con);
						$row = mysql_fetch_assoc($r);
						$num = mysql_num_rows($r);
						if ($num>0) 
							do { ?>  
					 mlb.add('<?=$row['title']?>','<a href="index.php?page=<?=$row['id_page']?>"><?=$row['title']?></a>');
																				
							
							<? } while ($row = mysql_fetch_assoc($r));?>
										
               
                

                // Return the new listbox instance
                return mlb;
				
		

            
        }

        return null;
    }
});

// Register plugin with a short name
tinymce.PluginManager.add('example', tinymce.plugins.ExamplePlugin);
tinyMCE.init({
	// General options
	mode : "textareas",
	theme : "advanced",
	editor_selector : "editorOn",
plugins : "paste, example",

theme_advanced_buttons1 : "insertImage,insertLink,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontsizeselect", 

theme_advanced_buttons2 : ",search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,inserttime,preview,|,forecolor,backcolor", 

theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen", 
	
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,

	// Example content CSS (should be your site CSS)
	content_css : "css/example.css",

	// Drop lists for link/image/media/template dialogs
	template_external_list_url : "js/template_list.js",
	external_link_list_url : "js/link_list.js",
	external_image_list_url : "js/image_list.js",
	media_external_list_url : "js/media_list.js",

	// Replace values for the template plugin
	template_replace_values : {
		username : "Some User",
		staffid : "991234"
	}
});</script>