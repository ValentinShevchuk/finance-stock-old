<script>

var error_code_send = 'Пожалуйста, введите код, отправленный на Ваш мобильный телефон';
var error_wait_2 = 'Если вы не получили СМС-код в течение 2-ух минут, нажмите \"Получить код\" еще раз.';
var error_send_check = 'СМС-код отправлен на Ваш мобильный телефон';
var error_incorrect = 'Вы ввели неверный СМС-код.';
var error_old_code = 'Срок действия СМС-кода истек.';

function _AJAX(_object, _action, _param){        
	var res;
		$.ajax({
		url: '/ajax.php',
		async:false,
		type: 'POST',
		dataType: 'jsonp',
		jsonp: 'callback',
		jsonpCallback: "jsonpCallback",
		//data: dataSend,
					data: { object: _object, action: _action, param: _param},
		
		success: function(data)
		{  
		  //  alert(data['res']);
			//return data;
			res=data;
		},
	});  
	return res;  
}

$(function(){
	

 function alert_c(arr) // alert custom
{

	if  (arr['title']==undefined)  arr['title']='Информация';
	if  (arr['type']==undefined) arr['type']='info';
	
	new PNotify({
	title: arr['title'],
	text: arr['text'],
	type: arr['type'] // all contextuals available(info,system,warning,etc)
});
}/*
 функция генерации и проверки смс кода */
$(".select_type_registration").on('click',function(){   
	$("[name='type_registration']").val($(this).data('id'));
});
function ajax_check_sms(type){
	
   
	
	  var phone_number = $("input[name=mobile_phone]").val();
	  phone_number = phone_number.replace(/\s*/g,'');         //убираем пробелы из номера
	  phone_number = phone_number.replace("(",'');         // убираем скобку
	  phone_number = phone_number.replace(")",'');         // убираем
	  phone_number = phone_number.replace("-",'');         // убираем
	  
	  if (phone_number.length>=10){ 
		$(".finish").html('<i class="fa fa-spin fa-spinner"></i>');
		$.ajax({
	   		type:'post',
				url:'ajax_generate_smscode.php',
				data:'type='+type+'&mobile_phone='+phone_number+'&input_sms='+$("input[name=smscode]").val(),
				success:function(data){
					if(type==0){
						if (data == 0){
							$(".sms_t").text(error_wait_2); // wait 2 minute for next code
						}else if (data == 1){
							$(".sms_t").text(error_send_check); // generate Sms Code successfully
						}
					}
					if(type==1){
						if (data == 0){
							$(".sms_t").text(error_incorrect); // error code
						}else if(data == 2){
							$(".sms_t").text(error_old_code);   // old code > 5 minute
						}else if(data == 1){
							$(".the_form_reg").submit();             // отправка формы регистрации если код верный
						}
					}
				
				 $(".finish").html('Отправить форму');
				}
		});
		} else { 
		
			alert_c({ 'text':'Введите номер','title':'Ошибка','type':'danger'});
			
		}
}
/* end функция генерации и проверки смс кода */

$("#myModal").on('shown.bs.modal', function (e) {
	//alert('окно запущено');
	var email=$("[name='email']").val();
	var mobile_phone=$("input[name=mobile_phone]").val();
	if (mobile_phone.length>=10){ 
		var res=_AJAX('Client', 'CheckData',{ json:1, email: email, mobile_phone: mobile_phone  });
		if (res['error']>0) { 
			$("#myModal").modal('hide');  
			alert_c({ 'text':res['error_msg'],'title':'Ошибка','type':'danger'});  
		}else{
			$(".request_sms").click();
		}
	}
	else
	{
		  $("#myModal").modal('hide');  
		alert_c({ 'text':'Введите номер.<br>Он необходим для получения SMS кода','title':'Ошибка','type':'danger'}); 
	}
});

$(".request_sms").click(function(){
 //   console.log('1');
 $(this).html('<i class="fa fa-spin fa-spinner"></i>');
	var mobile_phone=$("input[name=mobile_phone]").val();
	if (mobile_phone.length>=10){ 
		
		var res=_AJAX('Enter', 'generateSmsCode',{  'json':'1', 'mobile_phone': mobile_phone });
		if (res['error']>0){ alert_c({ 'text':res['error_msg'],'title':'Ошибка','type':'danger'}); $(this).html('Получить SMS-код'); }
		else { alert_c({ 'text':'Код успешно отправлен','title':'Информация','type':'info'});    $(this).html('Получить SMS-код'); }
		
	  
	} else {
		alert_c({ 'text':'Введите номер','title':'Ошибка','type':'danger'});  $(this).html('Получить SMS-код');

	}
   // ajax_check_sms(0);
   //alert({ 'text':'Пошел процесс'});
	//console.log(123);
//   new PNotify({  title: 'test',text:'as'});
});

$(".finish").click(function(){

	var mobile_phone=$("input[name=mobile_phone]").val();
	var sms_code=$("input[name=smscode]").val();
	var res=_AJAX('Enter', 'verificationSmsCode',{  'json':'1', 'mobile_phone': mobile_phone, 'sms_code':sms_code });        
	//$("input[name=smscode]").val();
	if(sms_code=='' || sms_code.length<4) {
		alert_c({ 'text':'Пожалуйста, введите код, отправленный на Ваш мобильный телефон','title':'Ошибка','type':'danger'}); 
	}else{  
		if (res['error']>0){ $(".finish").html('Отправить форму'); alert_c({ 'text':res['error_msg'],'title':'Ошибка','type':'danger'});}
		else {
			//alert_c({ 'text':'отправлено'});
			 $(".the_form_reg").submit();
			} 
	}
return false;
});

$( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy"});

function ajax(var_object,var_action,param,callback){      
	$.ajax({
		url: '/ajax.php',
		type: 'POST',
		dataType: 'jsonp',
		jsonp: 'callback',
		jsonpCallback: "jsonpCallback",    
		data: { object: var_object, action: var_action, param: param},
		
		success: function(data)
		{
	
		   callback(data);
		},
	});     

	
}


// Sparklines Demo
var demoSparklines = function() {

var sparkLine = $('.inlinesparkline');
// Init Sparklines
if (sparkLine.length) {

	var sparklineInit = function() {
	$('.inlinesparkline').each(function(i, e) {
		var This = $(this);
		var Color = "rgb(34,96,120)";
		var Height = '35';
		var Width = '95%';
		This.children().remove();
		// default color is "primary"
		// Color[0] = default shade
		// Color[1] = light shade
		// Color[2] = dark shade
		//alert('hi')
		// User assigned color and height, else default
		var userColor = This.data('spark-color');
		var userHeight = This.data('spark-height');
		if (userColor) {
				Color = "rgb(34,96,120)"
		}
		if (userHeight) {
				Height = userHeight;
		}
		$(e).sparkline('html', {
				type: 'line',
				width: Width,
				height: Height,
				enableTagOptions: true,
				fillColor:"", 

		});
	});
	}


				sparklineInit();
		}

}// End Sparklines Demo


demoSparklines();


/*$(".click-send-order").click(function(){ 

	var o_name=$('[name="name"]').val();
	var o_email=$('[name="email"]').val();  
	var o_quest=$('[name="quest"]').val(); 
	var button=$(this);
	if (o_email!='' || o_quest!=''){        
		$(button).html('<span class="fa fa-spin fa-spinner"></span>');
		var param= { 
			'json':'1','o_name':o_name,'o_email':o_email,'o_quest':o_quest};       
				   
		ajax('Client','Subscribe',param,function (data) {
			if (data['res']=='ok') 
			{ 
				$(".submit-status").html("Благодарим Вас за обращение в нашу компанию! В ближайшее время наш специалист свяжется с Вами!");  $(".submit-status").show();
				$(button).html('Отправлено');
			}
		});
	
	} else alert('Введите Email или Телефон');
	return false;
});	

<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
	<a href="img/project_saybel_1.jpg" rel="prettyPhoto" >
		<span class='fa fa-file-image-o fa-4x'></span><br><span style='font-size:14px'>Главная страница</span>
	</a>
</div>
<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'>
	<a href="img/project_saybel.pptx" target="_blank"> <span class='fa fa-file-powerpoint-o fa-4x' ></span><br><span style='font-size:14px'>Презентация МФО</span></a>
</div>

*/
{if $GET.p=='projects' && (isset($GET.id) && $GET.id>0)}
setInterval(function () {
	$.ajax({
		url: '/ajax.php?GetBondNameList',
		async:false,
		type: 'POST',
		dataType: 'jsonp',
		jsonp: 'callback',
		jsonpCallback: "jsonpCallback",
		data: { object: 'Bond', action: 'GetBondNameList', param: { 'json':'1', 'id_project': {$GET.id}  }},
		
		success: function(data)
		{  
			if (data['error']==0) 
				data['row'].forEach(function(row){	
					var TEMP_DIFF=0;
					var arr=[];
					var Color = "green"
					var Height = '35';
					var Width = '95%';
					$(".inlinesparkline").sparkline(row.price.split(','), {
						type: 'line',
						width: Width,
						height: Height,
						fillColor: "",       
						// enableTagOptions: true,
						//tooltipFormat: $.spformat('{literal}{{value}}{/literal}', 'tooltip')
					});
					$(".stock_value").html(row.par_now);
				});
		}
	})


	$.ajax({
		url: '/ajax.php',
		async:false,
		type: 'POST',
		dataType: 'jsonp',
		jsonp: 'callback',
		jsonpCallback: "jsonpCallback",
		data: { object: 'Bond', action: 'Price', param: {  'type_bonds': {$bond.id},   'is_demo':0 ,   'json':'1'  }},
		
		success: function(data)
		{  
			//  chart.dataProvider.push({  date: data[],visits: });
			//data.Reverse();
			//console.log(newDate);
			$(".pricing-frequency").html(data.differencePercent.toFixed(2)+'%');  
			if (data.differencePercent<0){
				$(".pricing-symbol").removeClass("fa-arrow-up text-success");
				$(".pricing-symbol").addClass("fa-arrow-down text-danger");
			}
			else {
					$(".pricing-symbol").addClass("fa-arrow-up text-success");
				$(".pricing-symbol").removeClass("fa-arrow-down text-danger");
			
			}                                    
		},
	});    
}, 10000); 

{/if}


function getFileList()
{
	var params={};

	params.json=1;
	params.own_id='{$GET.id}';
	params.tableNameInt=1;
	params.is_site=1;


	console.log('GET '+params.own_id);
	
	var result=_AJAX('Files', 'getFileList',params);
	$(".file-list").html('');
	if (result['error']==0)  {
		result['row'].forEach(function(row){
			if (row.ext=='jpg' || row.ext=='bmp' || row.ext=='png' || row.ext=='jpeg') 
			$(".file-list").append("<div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'><a href='/img.php?f="+row.id_file+"' rel='prettyPhoto' ><span class='fa fa-file-image-o fa-4x'></span><br><span style='font-size:14px'>"+row.title+"</span>	</a></div>");
			else
			$(".file-list").append(" <div class='col-xs-6 col-sm-4 col-md-4 col-lg-4'><a href='/file.php?f="+row.id_file+"' target='_blank'> <span class='fa fa-file-o fa-4x' ></span><br><span style='font-size:14px'>"+row['title']+"</span></a></div>");
		});

		$("a[rel^='prettyPhoto']").prettyPhoto({
			theme: 'picseel_lightbox',
			deeplinking: false,
			social_tools: false,
			show_title: false
		});

	} else $(".file-list").append("1");

}
{if $GET.p=='projects'} getFileList();{/if}  

$('.filter-list li a:eq(0)').click();


});
</script>