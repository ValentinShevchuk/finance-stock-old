var from_country_code="ru",
		months_langs = "个月";


Date.prototype.Format = function (fmt) { //格式化时间， Ex：new Date().Format('yyyy-MM-dd hh:mm:ss')
    var o = {
        "M+": this.getMonth() + 1, 
        "d+": this.getDate(),
        "h+": this.getHours(), 
        "m+": this.getMinutes(),
        "s+": this.getSeconds(), 
        "q+": Math.floor((this.getMonth() + 3) / 3), 
        "S": this.getMilliseconds() 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

Date.prototype.isLeapYear = function()   {   //判断是否是闰年
    return ((0==this.getFullYear()%4)&&((this.getFullYear()%100!=0)||(this.getFullYear()%400==0)));   
}
Date.prototype.isLastDay = function(){  //判断是否是该月的最后一天
        if(this.getMonth()==new Date(this.getFullYear(),this.getMonth(),this.getDate()+1).getMonth())    return false;
        else    return true;
}
Number.prototype.toFixed = function(s) {    //取最后两位小数，相当于php 的 round
    changenum=(parseInt(this * Math.pow( 10, s ) + 0.5)/ Math.pow( 10, s )).toString();
    index=changenum.indexOf(".");
    if(index<0 && s>0){
        changenum=changenum+"."; 
        for(i=0;i<s;i++){ 
            changenum=changenum+"0"; 
        } 
    }else { 
        index=changenum.length-index; 
        for(i=0;i<(s-index)+1;i++){ 
            changenum=changenum+"0"; 
        } 
    } 
    return changenum; 
}

$(document).ready(function() {
    /*calculator Start*/  
    var tarif=0,
        interest_rate = 0.06,
        deposit=3000,
        months=3,
        replenishment=1000,
        receive_money=505.00,
        curr_symbol = "$",
        curr_hkd = "1";     //用于控制水柱上升比例快慢

    var deposit_X = 0,
        deposit_min = 3000,
        deposit_max = 3000000,
        deposit_n1 = 3000,
        deposit_n2 = 200000,
        deposit_n3 = 500000,
        deposit_n4 = 1000000,
        deposit_n5 = 3000000,
        dep_vis = 0;    //根据vis判断是使用何种间隔

    var months_X = 3;

    var rep_X = 0,
        rep_min = 1000,
        rep_max = 250000,
        rep_n1 = 0,
        rep_n2 = 1000,
        rep_n3 = 20000,
        rep_n4 = 50000,
        rep_n5 = 100000,
        rep_n6 = 250000,
        rep_vis = 0;    //根据vis判断是使用何种间隔

    var interest_select = 2;

    var payment_period = 3; 
    liquid_change();

    //輸入位移X，最小值，最大值，距离。輸出當前值
    function silder_value(X,min,max,distance) {
        if(distance==null)distance=289;
        return min + X * (max-min)/distance;
    }

    //输入当前值，最小值，最大值，距离。輸出位移X
    function slider_X(value,min,max,distance){
        return Math.ceil((value-min)*distance/(max-min));
    }

    //deposit專用，輸入位移X，及5個分段值，輸出當前值
    function deposit_value(X,n1,n2,n3,n4,n5,vis) {
        var value=3000;
        var distance1 = 72;
        var distance2 = 147;
        var distance3 = 219;
        var distance4 = 289;
        if(X == 0){
            value = n1;
        }
        else if(X <= distance1){

            value = silder_value(X,n1,n2,distance1);
            //console.log(value);
            if(vis == 1 ){
                value = value - value%500;
            }else if(vis == 2){
                value = value - value%10000;
            }else if(vis == 4){
                value = value - value%50000;
            }else{
                value = value - value%5000;
            }

        }else if(X <= distance2){

            value = silder_value(X-distance1,n2,n3,distance2-distance1);
            //console.log(value);
            if(vis == 2){
                value = value - value%10000;
            }else if(vis == 4){
                value = value - value%50000;
            }else{
                value = value - value%5000;
            }

        }else if(X <= distance3){

            value = silder_value(X-distance2,n3,n4,distance3-distance2);
            //console.log(value);
            if(vis == 0 || vis == 5){
                value = value - value%10000;
            }else if(vis == 1 || vis == 3){
                value = value - value%5000;
            }else if(vis == 2){
                value = value - value%50000;
            }else{
                value = value - value%100000;
            }

        }else{
            value = silder_value(X-distance3,n4,n5,distance4-distance3);
            //console.log(value);
            if(vis == 1 || vis ==3){
                value = value - value%50000;
            }else if(vis == 4){
                value = value -value%500000;
            }else{
                value = value - value%10000;
            }
        }
        return value;
    }
    //Deposit month，輸出當前值
    function month_value(X,n1,n2,n3,n4) {
        var value=3;
        var distance1 = 77;
        var distance2 = 183;
        var distance3 = 289;
        if(X <= distance1){
            value = silder_value(X,n1,n2,distance1);
        }else if(X <= distance2){
            value = silder_value(X-distance1,n2,n3,distance2-distance1);
        }else{
            value = silder_value(X-distance2,n3,n4,distance3-distance2);
        }
        return value;
    }
    
    //Monthly replenishment，輸出當前值 //min_33
    function rep_value(X,n1,n2,n3,n4,n5,n6,vis) {
        var value=0,
            distance1 = 28,
            distance2 = 98,
            distance3 = 162,
            distance4 = 227,
            distance5 = 289;

        if(X == 0){
            value = n1;
        }else if(X <= distance1){
            value = n2;
        }else if(X <= distance2){
            value = silder_value(X-distance1,n2,n3,distance2-distance1);
            //console.log(value);
           if(vis == 2){
                value = value - value%500;
            }else if(vis == 1 || vis == 3){
                value = value - value%100;
            }else{
                value = value - value%200;
            }
        }else if(X <= distance3){
            value = silder_value(X-distance2,n3,n4,distance3-distance2);
            //console.log(value);
            if(vis == 2){
                value = value - value%1000;
            }else if(vis == 4){
                value = value - value%1000;
            }else{
                value = value - value%500;
            }
        }else if(X <= distance4){
            value = silder_value(X-distance3,n4,n5,distance4-distance3);
            //console.log(value);
            if(vis == 0 || vis ==5){
                value = value - value%1000;
            }else if(vis == 2 || vis ==4){
                value = value - value%5000;
            }else {
                value = value - value%500;
            }
        }else if(X <= distance5){
            value = silder_value(X-distance4,n5,n6,distance5-distance4);
            //console.log(value);
            if(vis ==1){
                value = value - value%500;
            }else if(vis == 3 || vis == 5){
                value = value - value%1000;
            }else{
                value = value - value%5000;
            }
        }
        return value;
        
    }

    function rep_change(){
        if($( ".slider3 div img" ).hasClass('no_rep')){
            $('.slider3 .the_sd').css('width', '26px');
        }else{
            var rmt = r_odd(rep_max/(months-1)),    //rep_max_temp
                distance1 = 28,
                distance2 = 98,
                distance3 = 162,
                distance4 = 227,
                distance5 = 289,
                rx = 315;
            
            if (rmt>=rep_n5){
                rx = slider_X(rmt,rep_n5,rep_n6,distance5-distance4)+distance4+26;
                
            }else if(rmt>=rep_n4 && rmt<rep_n5){
                rx = slider_X(rmt,rep_n4,rep_n5,distance4-distance3)+distance3+26;
            }else if(rmt>=rep_n3 && rmt<rep_n4){
                rx = slider_X(rmt,rep_n3,rep_n4,distance3-distance2)+distance2+26;
            }else if(rmt>=rep_n2 && rmt<rep_n3){
                rx = slider_X(rmt,rep_n2,rep_n3,distance2-distance1)+distance1+26;
            }
            //console.log('rmt:'+rmt+'; rx:'+rx);
            $('.slider3 .the_sd').css('width', rx+'px');
        }
            $('.slider3 div img').css('left', '0px');
            $('.slider3 div .cover').css('width', '0px');
            replenishment = 0;
            $(".slider3 .slider_count").html('0');
        
    }

    //________________________________________________________________________________
    //-----slider1-----------------------------------------------------------------
    $( ".slider1 div img" ).draggable({
        containment: "parent",
        axis: "x",
        drag:function(e){
             deposit_X = $(this).offset().left-$(this).parent().offset().left;
             deposit_slider();
        }
    });

    //獨立的deposit滑動條顯示運算
    function deposit_slider() {
        
         deposit = deposit_value(deposit_X,deposit_n1,deposit_n2,deposit_n3,deposit_n4,deposit_n5,dep_vis);
         deposit = Math.round(deposit);

         $("input[name=amount]").val(deposit);
         $(".slider1 .slider_count").html(money_format(deposit));
         $(".slider1 div .cover").css("width",deposit_X+15);
         liquid_change();
    }

    //________________________________________________________________________________
    //-----slider2-----------------------------------------------------------------
    $( ".slider2 div img" ).draggable({
        containment: "parent",
        axis: "x",
        drag:function(e){
             months_X = $(this).offset().left-$(this).parent().offset().left;
             month_slider();
             
        }
    });

     function month_slider(){

        months = month_value(months_X,3,6,12,36);
        months = Math.floor(months);

        var r = '';
        if(months == 3){
            $(".months_rates_span_1").css({'font-size':'18px','left':'20px'});
            $(".months_rates_span_2").css({'font-size':'11px','left':'130px'});
            $(".months_rates_span_3").css({'font-size':'11px','left':'230px'});
            r = $(".months_rates_span_1:not('.h')").text();
            $(".interest_rate_div").html(r);
        }else if(months <= 5){
            $(".months_rates_span_1").css({'font-size':'18px','left':'20px'});
            $(".months_rates_span_2").css({'font-size':'11px','left':'130px'});
            $(".months_rates_span_3").css({'font-size':'11px','left':'230px'});
            r = $(".months_rates_span_1:not('.h')").text();
            $(".interest_rate_div").html(r);
        }else if(months <= 11){
            $(".months_rates_span_2").css({'font-size':'18px','left':'120px'});
            $(".months_rates_span_1").css({'font-size':'11px','left':'30px'});
            $(".months_rates_span_3").css({'font-size':'11px','left':'230px'});
            r = $(".months_rates_span_2:not('.h')").text();
            $(".interest_rate_div").html(r);
        }else{
            $(".months_rates_span_3").css({'font-size':'18px','left':'220px'});
            $(".months_rates_span_2").css({'font-size':'11px','left':'130px'});
            $(".months_rates_span_1").css({'font-size':'11px','left':'30px'});
            r = $(".months_rates_span_3:not('.h')").text();
            $(".interest_rate_div").html(r);
        }

        interest_rate = Math.floor(parseFloat($(".interest_rate_div").html().split('%')[0])*100)/10000;     //解决浮点数精度问题
        
        rep_change();

        $("input[name=term]").val(months);
        $(".month_v").html(months);
        $(".slider2 .slider_count").html(months);
        $(".slider2 div .cover").css("width",months_X+15);
        liquid_change();
     }


    //________________________________________________________________________________
    //-----slider3-----------------------------------------------------------------
    $( ".slider3 div img" ).draggable({
    containment: "parent",
    axis: "x",
    drag:function(e){
         rep_X = $(this).offset().left-$(this).parent().offset().left;
         rep_slider();
     }
    });
    //獨立的replenishment滑動條顯示運算
    function rep_slider() {
        
         replenishment = rep_value(rep_X,rep_n1,rep_n2,rep_n3,rep_n4,rep_n5,rep_n6,rep_vis);
         replenishment = Math.round(replenishment);
         $(".slider3 .slider_count").html(money_format(replenishment));
         $(".slider3 div .cover").css("width",rep_X+15);
         liquid_change();
    }

    //________________________________________________________________________________

    
    var k=10.5,
        h = 154,
        u=!0;
    function liquid_change()     //根据进度条比例控制右边水柱高度
    {   
        receive_money =receive();
        var interst = receive_money-((months-1)*replenishment+deposit),
            rep = (months-1)*replenishment,
            b, c, d, e, f = deposit,
            g = months,
            i = k / 100,
            j = 1.5 / 100,
            l = 0,
            m = f * j,
            n = 0,
            o = 0,
            p = 0,
            q = 0;
        if(rep_X>0) l=replenishment;
        p += f + m;
        o = f;
        q = p;
        for (var x = 1; g >= x; x++) {
            if (g - 2 >= x) {
                d = g - 3 >= x ? l + l * j : l;
                c = l
            } else {
                c = 0;
                d = 0
            }
            e = p * i / 12;
            q += q * i / 12;
            u && (p += e);
            n += e;
            p += d;
            q += d;
            m += d - c;
            o += c
        }

        $("#liquid_1").css('height', f * h / q);  //amount
        $("#liquid_2").css("height", n * h / q);    //interests
        $("#liquid_4").css('height', o * h / q);  //amount

        $(".liqu_1 .v").html(form_symbol(deposit,1));
        $(".liqu_2 .v").html(form_symbol(receive_money,1));
        $(".receive_money").html(form_symbol(receive_money,1));
        $("#td_saving_amount").html(money_format(deposit));
        $("#td_saving_interest").html(form_symbol(interst,0));
        $("#td_saving_rep").html(money_format(rep));
    }

    function receive()  //计算金额，假设续存从第二天开始。Ex: 2014-11-20点击计算器，即2014-11-21开始续存
    {
        var d_a = 0,    //有多少天可以计算利息
            r_a = 0,    //每天的利息是多少
            de = 0,     //每次派息时的总续存是多少
            x = 0,      //每次派息后账户中的钱，如果是存至Savings account，即为最后所获得的钱
            m = 0,      //最后可获得的钱   存至Savings account，与x相等；存至Current accnout，等于x加上在current account 的利息
            result_month = 0,
            a = deposit,
            b = months,
            c = replenishment,
            d = 365,    //全年天数
            e = b,      //循环次数-1
            t = new Date(),     //creade_date 开始日
            end_t = new Date(t.getFullYear(),t.getMonth()+b,t.getDate()),   //end_date 结束日
            interest = Array(),     //每次派息所获的利息
            result_int = Array(),
            f = interest_rate;   //利率

        if (payment_period==3) e = Math.ceil(b/3)*3;
        

        for (var i = 1; i <= e+1; i++) {
            if(new Date(t.getFullYear(),t.getMonth()+i,t.getDate()).isLeapYear()) d = 366; //闰年全年天数366天

            if(payment_period==3){  //按季度派息

                result_int = real_date_diff(new Date(t.getFullYear(),t.getMonth()+1,01,00,00,00).Format('yyyy-MM-dd hh:mm:ss'),new Date(t.getFullYear(),t.getMonth()+i,01,00,00,00).Format('yyyy-MM-dd hh:mm:ss'));     //距离开始日result_int[0]第几年，result_int[1]第几个月
                //console.log(result_int);
                result_month = result_int[0]*12 + result_int[1];
                if (result_month % 3 == 0 && result_month!=0){
                    if (result_month==3){   //第一个三个月后
                      if (new Date(t.getFullYear(),t.getMonth()+i,01)-end_t>0){ //如果本月内完结，完结日减去开始日
                        d_a = Math.floor((end_t.getTime()-t.getTime())/(24*60*60*1000)+1);   //开始之日，开始计算利息
                      }else{
                        d_a = Math.floor((new Date(t.getFullYear(),t.getMonth()+i,01,00,00,00).getTime()-new Date(t.getFullYear(),t.getMonth(),t.getDate(),23,59,59).getTime())/(24*60*60*1000));   //开始之日不计算利息，开始日离第一次派息日（即三个月后1号）有多少天 Ex:2014-11-20 至 2015-03-01 d_a = 90
                      }
                    de = 0; //如果选择期间为三个月，续存不算利息
                    }else if(new Date(t.getFullYear(),t.getMonth()+i,01)-end_t>0){  //最后一次派息
                        d_a = Math.floor((end_t.getTime()-new Date(t.getFullYear(),t.getMonth()+i-3,01,00,00,00).getTime())/(24*60*60*1000)+1);     //结束日在该月占的天数，基本上等于开始日期：Ex:2014-11-20 开始，d_a = 20
                        de = c*(i-4);   //续存乘以i-3-1个月
                    }else{
                        d_a = Math.floor((new Date(t.getFullYear(),t.getMonth()+i,01).getTime()-new Date(t.getFullYear(),t.getMonth()+i-3,01).getTime())/(24*60*60*1000));  //整一个季度有多少天
                        de = c*(i-4);   //续存乘以i-3-1个月
                    }

                    r_a = (a+de)*(f/d);     //每天获得的本金利息
                    o = a+de;
                    if(interest_select==1){
                        interest[i] = new Number(d_a*r_a).toFixed(2);
                        x = a + parseFloat(interest[i])+c*(b-1);
                        a = a + parseFloat(interest[i]);    //复利
                    }else if(interest_select==2){
                        interest[i] = new Number(d_a*r_a).toFixed(2);
                        x = a+c*(b-1);
                        m = parseFloat(m)+parseFloat(interest[i]);
                    }
                    //console.log('i: '+i+'; replenishment:'+de+'; day_ago:'+d_a+'; date:'+new Date(t.getFullYear(),t.getMonth()+i,01,00,00,00).Format('yyyy-MM-dd')+'; interest['+i+']:'+interest[i]+'; sum:'+o);
                }
            }else{      //payment_period=5 按月派息
                if (t.isLastDay() && i==1) continue;
   
                if (i==1){  //第一个月
                  if (new Date(t.getFullYear(),t.getMonth()+i,01).getTime()-end_t.getTime()>0){  //如果本月内完结，完结日减去开始日
                    d_a = Math.floor((end_t.getTime()-t.getTime())/(24*60*60*1000)+1);   //开始之日，开始计算利息
                  }else{
                    d_a = Math.floor((new Date(t.getFullYear(),t.getMonth()+i,01,00,00,00).getTime()-new Date(t.getFullYear(),t.getMonth(),t.getDate(),23,59,59).getTime())/(24*60*60*1000));   //开始之日不计算利息，开始日离该月结束还有多少天 Ex：2014-11-20 从21号至30号，d_a = 10
                  }
                  de = 0;
                }else if(new Date(t.getFullYear(),t.getMonth()+i,01).getTime()-end_t.getTime()>0){  //最后一次派息
                  d_a = Math.floor((end_t.getTime()-new Date(t.getFullYear(),t.getMonth()+i-1,01,00,00,00).getTime())/(24*60*60*1000)+1);   //结束日在该月占的天数，基本上等于开始日期：Ex:2014-11-20 开始，d_a = 20
                  de = c*(i-2); //续存乘以i-1-1个月，与倒数第一次派息总续存无变化。
                }else{
                  d_a = new Date(t.getFullYear(),t.getMonth()+i,0).getDate();   //该月整月的天数
                  de = c*(i-1); //续存乘以i-1个月
                }
                
                r_a = (a+de)*(f/d);     //每天获得的本金利息
                o = a+de;
                if(interest_select==1){
                    interest[i] = new Number(d_a*r_a).toFixed(2);
                    x = a + parseFloat(interest[i])+c*(b-1);
                    a = a + parseFloat(interest[i]);    //复利
                }else if(interest_select==2){
                    interest[i] = new Number(d_a*r_a).toFixed(2);
                    x = a+c*(b-1);
                    m = parseFloat(m)+parseFloat(interest[i]);
                }
                //console.log('i: '+i+'; replenishment:'+de+'; day_ago:'+d_a+'; date:'+new Date(t.getFullYear(),t.getMonth()+i,01,00,00,00).Format('yyyy-MM-dd')+'; interest['+i+']:'+interest[i]+'; sum:'+o);
            }
        };

        m = m+x;
        return m;
    }

    function real_date_diff(date1, date2){  
        var diff = Array(),
            o,p,q,
            d1,d2;
        o = date1.split(' ');
        p = o[0].split('-');
        q = o[1].split(':');
        d1 = Array(parseInt(p[0]), parseInt(p[1]), parseInt(p[2]), parseInt(q[0]), parseInt(q[1]), parseInt(q[2]));
        o = date2.split(' ');
        p = o[0].split('-');
        q = o[1].split(':');
        d2 = Array(parseInt(p[0]), parseInt(p[1]), parseInt(p[2]), parseInt(q[0]), parseInt(q[1]), parseInt(q[2]));
        for(i=0; i<d2.length; i++) {
            if(d2[i]>d1[i]) break;
            if(d2[i]<d1[i]) {
                t = d1;
                d1 = d2;
                d2 = t;
                break;
            }
        }
        md1 = Array(31, d1[0]%4||(!(d1[0]%100)&&d1[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        md2 = Array(31, d2[0]%4||(!(d2[0]%100)&&d2[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        min_v = Array(null, 1, 1, 0, 0, 0);
        max_v = Array(null, 12, d2[1]==1?md2[11]:md2[d2[1]-2], 23, 59, 59);
        for(i=5; i>=0; i--) {
            if(d2[i]<min_v[i]) {
                d2[i-1]--;
                d2[i]=max_v[i];
            }
            diff[i] = d2[i]-d1[i];
            if(diff[i]<0) {
                d2[i-1]--;
                i==2 ? diff[i] += md1[d1[1]-1] : diff[i] += max_v[i]-min_v[i]+1;
            }
        }
        return diff;
    }


    //格式化貨幣符號，并保留两位小数，輸入數字,vi为区分是否返回符号
    function form_symbol(money,vi) {
        var money_inform = '';
        var last_num='';
        money_inform = money_inform + money%1000;//3位数以内的部分
        money_inform = Math.round(money_inform * 100) / 100;
        
        money_inform = money_inform + '';//转为字符串
        
        if(money>=1000){
            var text_loop = Math.round(money_inform);
            text_loop = text_loop+'';
            while(text_loop.length<3){
                text_loop = "0"+text_loop;
                money_inform = "0"+money_inform;
            }
        }
        money = (money-money%1000)/1000;
        while( money>0 ){
            last_num = money%1000;
            last_num = last_num+"";
            if(money>=1000){
                while(last_num.length<3){
                    last_num = "0"+last_num;
                }
            }
            money_inform =  last_num + "," + money_inform;
            money = (money-money%1000)/1000;
        }
        if(vi == 1){
            return curr_symbol+money_inform;
        }else if(vi == 0){
            return money_inform;
        }
    }

    //格式化无符号金额，病添加千
    function money_format(money){


        if(money==0){
            return 0.00
        }else{
            var money_inform = '';
            var last_num='';
            var syl = '';

            if(money>=10000){
                money_inform = money/10000;
                syl = '万';
            }else{
                money_inform = money;
            }
            return money_inform + syl;
        }
    }

    function r_odd(m){     //remove oddment 去掉零头 Ex:66666->66000
        m = m.toString().split('.')[0];
        x = m.length-2;
        m_t = parseFloat(m.substring(0,m.length-x))*Math.pow(10,x);
        return m_t;
    }

    function change_currency(){ //根据选择的product 显示货币
        $(".investment .right-part .calculator .change_curr ul").each(function() {
            $(this).children('li:visible').hide();
            var ch = $(this).find('li');
    		var min_i = ch.length - 1;
            for (var i = ch.length - 1; i >= 0; i--) {
                if($(ch[i]).attr('ver') == $(".investment .left-part .list-title-selected").attr('var') && $(ch[i]).attr('data-insurance') == $(".investment .left-part .list-title-selected").attr('vur')){
                    $(ch[i]).show();
                    //$(ch[i]).click();   //模拟点击了货币，即调用$(".change_curr ul li").click()
    				if(i<min_i) min_i = i;
    			}
            };
    		$(ch[min_i]).click();   //选择货币，即调用$(".change_curr ul li").click();
        
        });
    }


    function change_curr(obj_li){

        var product = obj_li.attr('vor'),
            currency  = obj_li.attr('var'),
            $deposit = $( ".slider1 div img" ),
            $month = $( ".slider2 div img" );

        deposit_min = parseFloat(obj_li.attr('vdi'));
        deposit_max = parseFloat(obj_li.attr('vda'));
        rep_min = parseFloat(obj_li.attr('vri'));
        rep_max = parseFloat(obj_li.attr('vra'));

        rep_min = rep_min/(36-1);
        if(rep_min<=50){
            rep_min = 50;
        }else if(rep_min<=100){
            rep_min = 100;
        }else if(rep_min<=150){
            rep_min = 150;
        }else{
            rep_min = 200;
        }

        if(rep_max%3==0){
            rep_t = rep_max/(3-1);
        }else{
            rep_t = r_odd(rep_max/(3-1));
        }


        if(product == '1'){

            if(currency == '5'){

                curr_hkd = "0";       //Great Wall,Loyalty  -> CNY
                dep_vis = 0; rep_vis = 0;
                deposit_n1 = deposit_min;            deposit_n2 = 200000;            deposit_n3 = 500000;            deposit_n4 = 1000000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;          rep_n2 = rep_min;            rep_n3 = 20000;            rep_n4 = 50000;            rep_n5 = 100000;            rep_n6 = rep_t;

            }else if(currency == '2'){

                curr_hkd = "1";        //Great Wall,Loyalty -> USD,EUR
                dep_vis = 1; rep_vis = 1;
                deposit_n1 = deposit_min;            deposit_n2 = 10000;            deposit_n3 = 100000;            deposit_n4 = 200000;            deposit_n5 = deposit_max;
                rep_n1 = 0;         rep_n2 = rep_min;      rep_n3 = 5000;            rep_n4 = 10000;            rep_n5 = 20000;            rep_n6 = rep_t;

            }

        }else if(product == '2'){
            if(currency == '5'){

                curr_hkd = "0";       //Great Wall,Loyalty  -> CNY
                dep_vis = 0; rep_vis = 0;
                deposit_n1 = deposit_min;            deposit_n2 = 200000;            deposit_n3 = 500000;            deposit_n4 = 1000000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;          rep_n2 = rep_min;            rep_n3 = 0;            rep_n4 = 0;            rep_n5 = 0;            rep_n6 = rep_t;

            }else if(currency == '2'){

                curr_hkd = "1";        //Great Wall,Loyalty -> USD,EUR
                dep_vis = 1; rep_vis = 1;
                deposit_n1 = deposit_min;            deposit_n2 = 10000;            deposit_n3 = 100000;            deposit_n4 = 200000;            deposit_n5 = deposit_max;
                rep_n1 = 0;         rep_n2 = rep_min;      rep_n3 = 0;            rep_n4 = 0;            rep_n5 = 0;            rep_n6 = rep_t;

            }
        }else if(product == '3'){

            if(currency == '5'){

                curr_hkd = "2";     //Care -> CNY
                dep_vis = 2; rep_vis = 2;
                deposit_n1 = deposit_min;            deposit_n2 = 500000;            deposit_n3 = 1000000;            deposit_n4 = 3000000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;     rep_n2 = rep_min;            rep_n3 = 50000;            rep_n4 = 100000;            rep_n5 = 200000;            rep_n6 = rep_t;

            }else if(currency == '2'){

                curr_hkd = "3";     //Care -> USD,EUR
                dep_vis = 3; rep_vis = 3;
                deposit_n1 = deposit_min;            deposit_n2 = 100000;            deposit_n3 = 200000;            deposit_n4 = 500000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;     rep_n2 = rep_min;            rep_n3 = 10000;            rep_n4 = 20000;            rep_n5 = 40000;            rep_n6 = rep_t;
                
            }

        }else if(product == '4'){

            if(currency == '5'){

                curr_hkd = "4";     //Wealth -> CNY
                dep_vis = 4; rep_vis = 4;
                deposit_n1 = deposit_min;            deposit_n2 = 2000000;            deposit_n3 = 5000000;            deposit_n4 = 10000000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;     rep_n2 = rep_min;            rep_n3 = 50000;            rep_n4 = 100000;            rep_n5 = 300000;            rep_n6 = rep_t;

            }else if(currency == '2'){
                
                curr_hkd = "5";     //Wealth -> USD,EUR
                dep_vis = 5; rep_vis = 5;
                deposit_n1 = deposit_min;            deposit_n2 = 200000;            deposit_n3 = 500000;            deposit_n4 = 1000000;            deposit_n5 = deposit_max;       
                rep_n1 = 0;     rep_n2 = rep_min;            rep_n3 = 10000;            rep_n4 = 20000;            rep_n5 = 40000;            rep_n6 = rep_t;

            }

        }

        $('.slider_note1').html(money_format(deposit_n1));
        $('.slider_note2').html(money_format(deposit_n2));
        $('.slider_note3').html(money_format(deposit_n3));
        $('.slider_note4').html(money_format(deposit_n4));
        $('.slider_note5').html(money_format(deposit_n5));

        $('.rep_note1').html(money_format(rep_n1));
        if(product=='2'){       //product2不允许添加续存
            $( ".slider3 div img" ).addClass('no_rep');     //rep_change() 中检测是否不允许续存
            $('.rep_note2').html('');
            $('.rep_note3').html('');
            $('.rep_note4').html('');
            $('.rep_note5').html('');
            $('.rep_note6').html('');
        }else{
            $( ".slider3 div img" ).removeClass('no_rep');
            $('.rep_note2').html(money_format(rep_n2));
            $('.rep_note3').html(money_format(rep_n3));
            $('.rep_note4').html(money_format(rep_n4));
            $('.rep_note5').html(money_format(rep_n5));
            $('.rep_note6').html(money_format(rep_n6));
        }
        
        //初始设置
        if(currency == '5'){
            $deposit.css('left', '184px');
            $month.css('left', '198px');
        }else if(currency == '2'){
            $deposit.css('left', '146px');
            $month.css('left', '198px');
        }
        deposit_X = $deposit.offset().left-$deposit.parent().offset().left;
        months_X = $month.offset().left-$month.parent().offset().left;

        deposit_slider();
        rep_slider();
        month_slider();
    }


	$(".change_country ul li").click(function(event) {//选国家
        $(".change_country ul .current_li").removeClass('current_li');
        $(this).addClass('current_li');

        var country = $(this).attr('var');
        $("input[name=savings_country]").val(country);
		$(".investment .left-part .list-title").siblings().hide();
		$(".investment .left-part .list-title[country="+country+"]").show();
		
		$(".investment .left-part .list-title:visible").eq(0).click();//选项目
        
    });

    $(".change_curr ul li").click(function(event) {
        $(".change_curr ul .current_li").removeClass('current_li');
        $(this).addClass('current_li');

        payment_period = $(this).attr('vur');
        var product = $(this).attr('ver');
        var currency  = $(this).attr('var');
        var insurance = $(this).attr('data-insurance');
        var country  = $("input[name=savings_country]").val();
        $("input[name=currency]").val(currency);
        $('.notes .months_rates_spans').addClass('h').hide();
        $('.notes .months_rates_span-'+product+'-'+currency+'-'+insurance).removeClass('h').show();
        
        $(".interest_rate_div").html($(".months_rates_span_1:not('.h')").text());
        curr_symbol = $(this).attr('syl');
        $("span.green_span:not('.months_span,.interest_span')").html(curr_symbol);
        
        interest_select = $(this).attr('vpr');
        $(".interest_select").removeClass('selected');
        $(".interest_select_"+interest_select).addClass('selected');

        change_curr($(this));   //调用转换货币函数
        liquid_change();
    });

     $(".investment .left-part .list-title").click(function(){      //选择项目
        $(".investment .left-part .list-title-selected").removeClass("list-title-selected");
        $(this).addClass("list-title-selected");
        $("input[name=id_product]").val($(this).attr('var'));
        $("input[name=insurance]").val($(this).attr('vur'));

        $(".months_rates_span_3").css('display','inline-block');
        $(".months_rates_p_1").css('left','85px');
        $(".months_rates_p_2").css('display','inline-block');
        $(".months_rates_p_2").css('left','191px');

        $(".saving_months_span_1").html('3');
        $(".saving_months_span_2").html('6').css('margin-left','70px');
        $(".saving_months_span_3").css('display','inline-block');
        $(".saving_months_span_4").css('display','inline-block');

        $(".slider2 div img").removeClass('wealth_slider_img');

        change_currency();
        
    });
	$(".change_country ul li[var="+from_country_code+"]").click();
	
    // For connect to the registration
    $("#submit_btn").click(function() {

        var currency = $("input[name=currency]").val();
        var amount = $("input[name=amount]").val();
        var term = $("input[name=term]").val();
        var id_product = $("input[name=id_product]").val();
		var country  = $("input[name=savings_country]").val();
        var insurance = $("input[name=insurance]").val();

        params = "object=MinSavings&action=GetSavingRate&currency="+currency+"&amount="+amount+"&term="+term+"&id_product="+id_product+"&vis=json"+"&country="+country+"&insurance="+insurance;
        $.ajax({
            url: '../ajax.php',
            type: 'POST',
            dataType: 'json',
            data: params,
            success:function(data){
                $("input[name=rate]").val(data.interest_rate);
                $("input[name=interest_payment_period]").val(data.calculate_interests);
                $("input[name=interest_payment]").val(data.interest_payment);
                $("input[name=interest_payment_type]").val(data.interest_payment);
                $("input[name=id_savings_product]").val(data.id);
                $("input[name=apply_savings]").val('Apply');
                //alert($("input[name=rate]").val()+'--------'+$("input[name=interest_payment_period]").val()+'--------'+$("input[name=interest_payment]").val()+'--------'+$("input[name=interest_payment_type]").val()+'--------'+$("input[name=id_savings_product]").val()+'--------'+$("input[name=apply_savings]").val());
                
                $('#the_form').submit();
            }
        });
        

        
        
        
    });
    /*calculator end*/  
});
function ajax()
    {
        $.ajax({
            type: "POST",
            dataType: 'script',
            timeout: 7000,
            url: '../ajax.php',
            async: false,
            data:params
        });
    }