//top picture panel auto change. 2012-6-8 Jack
$(function(){
     $(".top_panel .marker li img").attr("src","/images/deposit/marker.png");
     $(".top_panel .marker .1 img").attr("src","/images/deposit/marker_light.png");

    $(".top_panel .marker li").mouseover(function(){
        clearInterval(auto_pic);
        $(".top_panel .pic li").addClass("h");
        $(".top_panel .pic ."+$(this).attr("ver")).removeClass("h");
        $(".top_panel .marker li img").attr("src","/images/deposit/marker.png");
        $(this).find("img").attr("src","/images/deposit/marker_light.png");
    });
    $(".top_panel .marker li").mouseout(function(){
        action();
    });
    $(".top_panel .pic li img").mouseover(function(){
        clearInterval(auto_pic);
    });
    $(".top_panel .pic li img").mouseout(function(){
        action();
    });

    if($(".top_panel .pic li").length==1){
        $(".top_panel .marker li img").addClass("h");
    }
    var auto_pic;
    var pic_num = 1;
    action();

    function action() {
        auto_pic=setInterval(function(){
             pic_num = pic_num+1;
             if( pic_num>$(".top_panel .pic li").length )pic_num=1;
             
             $(".top_panel .pic li").addClass("h");
             $(".top_panel .pic ."+pic_num).removeClass("h");
             $(".top_panel .marker li img").attr("src","/images/deposit/marker.png");
             $(".top_panel .marker ."+pic_num+" img").attr("src","/images/deposit/marker_light.png");
        },5000);
    }

    $(".topmenu").click(function(){
        clearInterval(auto_pic);
    });

    $("#sub-menu a").click(function(){
        clearInterval(auto_pic);
    });
    
/*drop_down list selector*/
    var i = 1;  
    $(".arrow").click(function(){
        var v = $(this).next().attr("var");
        var w = parseInt($(this).css("width"))+5+"px";  
        var ul_w = parseInt($(this).css("width"))-12+"px";
        
        if($(this).next().css("display") == "none"){
            if(typeof v == "undefined"){
                $(this).next().css({"width":w,"left":$(this).position().left}).attr("var",i);// automatically adjust option's width and position
                $(this).next().children("ul").css("width",ul_w).attr("var",i);              
                i++;
            }
            $(".option").hide();
            $(this).next().slideDown();
        }else{
            $(".option").slideUp();
        }
    });

    $(".v").click(function(){
        var type;
        var v = parseInt($(this).attr("var"));      
        if(isNaN(v)){
            $(this).parent().children(".select").removeClass("select");
            $(this).addClass("select");
            $(this).parents(".option").prev().val($(this).text());
            $(this).parents(".option").prev().prev().val($(this).attr('var1'));
        }
        $(".option").hide();
    }); 
    
/*slider*/
    $( ".slider_btn" ).draggable({
        containment: "parent",
        axis: "y",
        drag:function(){
            var v = parseInt($(this).parents(".option").attr("var"));
            var move = $(this).offset().top-$(this).parent().offset().top;
            var total_height = parseInt(($(".option[var="+v+"] ul").css("height")));//drop_down list total height
            var option_height = parseInt(($(".option[var="+v+"]").css("height")));//option height
            var slider_multiple = -((total_height-option_height)/221).toFixed(2);//slide multiple
            $(".option[var="+v+"] ul").css("margin-top",slider_multiple*move);
        },
         stop:function(){
            var v = parseInt($(this).parents(".option").attr("var"));
            var move = $(this).offset().top-$(this).parent().offset().top;
            var total_height = parseInt(($(".option[var="+v+"] ul").css("height")));//drop_down list total height
            var option_height = parseInt(($(".option[var="+v+"]").css("height")));//option height
            var slider_multiple = -((total_height-option_height)/221).toFixed(2);//slide multiple
            $(".option[var="+v+"] ul").css("margin-top",slider_multiple*move);
        },
    }).mousedown(function(){
        $(this).css("background","url('/images/slider/option2.png')");
    });
    $("*").mouseup(function(){
        $(".slider_btn").css("background","url('/images/slider/option1.png')");
    });
        
});


$(function(){
    //process panel. 2012-6-11 jack
    $(".process a").mouseover(function(){
        $(".process a").css("color","#000");
        $(this).css("color","#0090c0");
    });
    $(".process a").click(function(){
    if($("#h_register_tip").css("display")==="none")
    {
            $("#h_register").click();
    }
    });

    //feature box. 2012-6-11 jack
    $(".feature_box ul li").mouseover(function(){
        $(".feature_box ul li").removeClass("light");
        $(this).addClass("light");
    });

    //reason box. 2012-6-11 jack
    $(".reason ul.title li").click(function(){
        //$(".reason .cnt .cc .ttl").hide();
        $(".reason .cnt .cc").not($(".new_reason .cnt .c"+$(this).attr("ver"))).slideUp();
        //$(".reason .cnt .c"+$(this).attr("ver")+" .ttl").slideDown();
        $(".reason .cnt .c"+$(this).attr("ver")).slideDown();
        
        $(".reason ul.title li").removeClass("light");
        $(this).addClass("light");
    });
    
    //cards 2012-6-14 jack
    $(".reason .quantity2 ul.intro li").mouseover(function(){
        $(".reason .quantity2 ul.intro li").removeClass("light");
        $(this).addClass("light");
    }); 
    
    BottomApplyAndLoginButton();
    
    $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
    $("#selectloan").click(function(){
        $("html,body").animate({scrollTop: $("#types").offset().top}, 500);
    });
});



function BottomApplyAndLoginButton(){
    if( $('#_login_name').val()){
        $('#PageBottomApplyButton').show();
        $('#btm_register').hide();
        $('#btm_enter').hide();
    }else{
        $('#PageBottomApplyButton').hide();
        $('#btm_register').show();
        $('#btm_enter').show();
    }
}
