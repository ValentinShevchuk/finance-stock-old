

$(function(){

    $(".saver-products .list-option").click(function(){
        $(".saver-products .list-option-selected").removeClass("list-option-selected");
        $(this).addClass("list-option-selected");
        $('.saver-products .tit_1').click();

        $('.saver-products .period-list .period-list-products').hide();
        var product = $(this).attr('var');
        $('.saver-products .period-list .period-list-product-'+product).show();

    });
    
    /*saver-porducts end*/

    $(".insurance .list-option").click(function(){
        $(".insurance .list-option-selected").removeClass("list-option-selected");
        $(this).addClass("list-option-selected");
        $('.insurance .tit_2').click();

        $('.insurance .period-list .period-list-products').hide();
        var product = $(this).attr('var');
        $('.insurance .period-list .period-list-product-'+product).show();

    });
    /*insurance end*/

    $(".banking .left-part .list-title").click(function(){
        var v = ($(this).attr("var"));
        $(".banking .left-part .list-title-selected").removeClass("list-title-selected");
        $(this).addClass("list-title-selected");
        $(".banking .right-part .banking-content").hide();
        $(".banking .right-part .banking-content[var="+v+"]").fadeIn('slow');
    });
    $(".banking .left-part .list-title").eq(1).click();
    
    /*banking end*/

    $('.how_open .ho_content .hc_list a').mouseenter(function() {
        $(this).siblings().removeClass('hovered');
        $(this).addClass('hovered');
    });
    /*how_open end*/

    $('.tls').on('click',function() {
        var num = $(this).attr('class').split(' ')[0].split('_')[1];
        $(this).parent().siblings().addClass('h');
        $(this).parent().siblings('.cont_'+num).removeClass('h');
        $(this).siblings().children().removeClass('selected');
        $(this).children().addClass('selected');
    });
    
    /*title change end*/

    $(".everbright").click(function(){
		$.ajax({
            type:'POST',
            url:'ajax.php',
            dataType:'json',
            data:'object=MinClientProfile&action=RecordDownFile&file_name='+$(this).attr("data-name")+'&file_size='+$(this).attr("data-size"),
        })
	})
});