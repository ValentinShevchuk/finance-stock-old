<?php
require('config.php');
header('Content-Type: text/html; charset=UTF-8');

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {

	if (isset($_REQUEST['object'])) {
		
		$object =  new $_REQUEST['object'];
		$action = $_REQUEST['action'];
	
	
		if (isset($_REQUEST['param'])) $object->$action($_REQUEST['param']);
		else  $object->$action();
	
	
	}
}
?>
