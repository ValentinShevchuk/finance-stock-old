<?php
define('server','../files/');
function AddWatermark( $src, $dest, $x=0, $y=0, $quality=100, $toscreen=0,$wd=1,$wh=0) {
 global $server,$row;

  if (!file_exists($src)) $src="images/no_image.png";

  $srcSize = getimagesize($src);

  if ($wd==0 AND $wh==0){
  $dstWidth=$srcSize[0];
  $dstHeight=$srcSize[1];
  $srcWidth=$srcSize[0];
  $srcHeight=$srcSize[1]; }

  else{

  $srcWidth=$srcSize[0];
  $srcHeight=$srcSize[1];
  //if ($wh==1) $perc=1; else $perc=(($wh*100)/$srcHeight)/100;
  if ($wd==1) $perc=1; else $perc=(($wd*100)/$srcWidth)/100;

  $dstWidth  =$srcSize[0]*$perc;
  $dstHeight = $srcSize[1]*$perc;

  }


$watermark="img/w.png";
   $watermarkSize = getimagesize($watermark);


  if (($srcSize === false)||($watermarkSize === false)) return false;


  $srcFormat = strtolower(substr($srcSize['mime'], strpos($srcSize['mime'], '/')+1));
  $srcIcFunc = "imagecreatefrom" . $srcFormat;
  if (!function_exists($srcIcFunc)) die('Critical error: GD lib not found!');





$idest = imagecreatetruecolor( $dstWidth, $dstHeight);
$background=imagecolorallocate($idest, 255, 255, 255);
imagefill($idest , 0, 0, $background);
$isrc = $srcIcFunc($src);
imageAlphaBlending($idest, false);
imageSaveAlpha($idest, true);
imagecopyresampled($idest, $isrc, 0,0,0, 0, $srcWidth*$perc,$srcHeight*$perc, $srcWidth,$srcHeight);

 if ($toscreen) {
      ob_clean();
      header('Content-Type: image/png');
      imagePng($idest);
      imagedestroy($isrc);

      imagedestroy($idest);
  } else {
      imagejpeg($idest, $dest, $quality);
      imagedestroy($isrc);

      imagedestroy($idest);
  }
  return true;
}

if (empty($_GET['f'])){
    header ("HTTP/1.0 404 Not Found");
    header('Location: /404');
    exit;
}
include('config.php');
$file_info = $db->getRow("SELECT * FROM files WHERE id=?",array($_GET['f']));
$filename_header =  $file_info['id'].".".$file_info['ext'];


if ($file_info['id']>0){
        $catalog=floor($_GET['f']/100);
    AddWatermark(server."$catalog/".$filename_header,$server."/photo/_".$val[1],0,0,100,1,isset($_GET['i'])?$_GET['i']:1,0);
    
    }else{
     header ("HTTP/1.0 404 Not Found");
     header('Location: /404');
     exit;
}
?>